# CaseArduino 

![](./PNG/CASEarduinoMorado.png)


## Summary/Descripción

This project is a particular case of the CASEgamma library of parameterizable cases for an Arduino UNO, the project uses the CASEgamma and Utilities libraries to run.

Éste proyecto es un caso particular de la librería de cases parametrizables CASEgamma para un Arduino UNO,el proyecto usa las librerías CASEgamma y Utilidades para poder funcionar.


## Armado/Assembly

![](./PNG/ArduinoUnoPins.png)

To Assemble the CASE with the card it is necessary to place the card in the larger piece, then place the lower part and with screw fasteners hold both pieces with the Arduino UNO board inside.
Below is a reference image to support the pins used in the Arduino UNO board

Para Ensamblar el CASE con la tarjeta es necesario colocar la tarjeta en la pieza mayor, posteriormente colocar la parte inferior y con ajuda de tornillos sujetar ambas piezas con la tarjeta arduino UNO dentro
Se muestra a continuación una imagen de referencia como apoyo a los pines utilizados en la tarjeta Arduino UNO



