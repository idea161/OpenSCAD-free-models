include<./../../../Bibliotecas/Utilidades/Utilities.scad>;



/*
####Explicación de variables#####
lonX                                    ->  longitud de cubo a esculpir en X
lonY                                    ->  longitud de cubo a esculpir en Y
lonZ                                    ->  longitud de cubo a esculpir en Z
espesor                             ->  espesor de cubo a esculpir
redondeadorMinkowski    -> radio de la esfera que se ocupa para la funcion Minkowki
//por borrar aqui
alturaSolapa                    ->altura extension de case para hacer que embone
*/




module CASEgamma(lonX=90,lonY=90,medZ=180,espesor=1.5,diametroPoste=5,tornillo=2.5,parte=0,minkow=3,soporteA=0,soporteB=0,soporteC=0,soporteD=0){

lonZ=medZ*2;

if(parte==0){
    
    
    
  
         translate([0,0,lonZ/4])
    intersection(){
       
        difference(){
            resize([lonX,lonY,espesor])
          cylinder(d=lonX*1,h=espesor*2,$fn=16);
            resize([lonX-2*espesor,lonY-2*espesor,espesor*2])
        cylinder(d=(lonX*1)-2*espesor,center=true,h=espesor*4,$fn=16);
            }    
            
       soporte(lonX=lonX,lonY=lonY,lonZ=lonZ,soporteA=soporteA,soporteB=soporteB,soporteC=soporteC,soporteD=soporteD);
            
}
        
        
    
difference(){
    if(minkow>0){
     recortadorMink(lonX,lonY,lonZ,minkow);
    }
    else{
        cube([lonX,lonY,lonZ],center=true);
    }
        cube([lonX-(2*espesor),lonY-(2*espesor),lonZ-(2*espesor)],center=true);
        
    translate([0,0,-lonZ])
        cube([lonX*2,lonY*2,lonZ*2],center=true);
        
}


if(diametroPoste>0){
    
    //REVISAR ALTURA DE SOPORTES CAMBIAR MINKOW/2 POR ESPESOR
    //Poste (1,1)
    translate([-diametroPoste+lonX/2,-diametroPoste+lonY/2,0])
    poste(diametroPoste,tornillo,lonZ);
    
    //Soporte(1,1,A)
    translate([lonX/2-diametroPoste,lonY/2-espesor,lonZ/4-(minkow/4)])
    cube([diametroPoste/2,diametroPoste/2,lonZ/2-(minkow/2)],center=true);
    
    //Soporte(1,1,B)
    //nota
    translate([lonX/2-espesor,lonY/2-diametroPoste,lonZ/4-(minkow/4)])
    cube([diametroPoste/2,diametroPoste/2,lonZ/2-(minkow/2)],center=true);
    
    //Poste (-1,1)
    translate([-diametroPoste+lonX/2,+diametroPoste-lonY/2,0])
    poste(diametroPoste,tornillo,lonZ);
    
    //Soporte(-1,1,A)
    translate([-lonX/2+diametroPoste,lonY/2-espesor,lonZ/4-(minkow/4)])
    cube([diametroPoste/2,diametroPoste/2,lonZ/2-(minkow/2)],center=true);
    
    //Soporte(-1,1,B)
    translate([-lonX/2+espesor,lonY/2-diametroPoste,lonZ/4-(minkow/4)])
    cube([diametroPoste/2,diametroPoste/2,lonZ/2-(minkow/2)],center=true);
    
    
    
    //Poste (1,-1)
    translate([diametroPoste-lonX/2,-diametroPoste+lonY/2,0])
    poste(diametroPoste,tornillo,lonZ);
    
    //Soporte(1,-1,A)
    translate([lonX/2-diametroPoste,-lonY/2+espesor,lonZ/4-(minkow/4)])
    cube([diametroPoste/2,diametroPoste/2,lonZ/2-(minkow/2)],center=true);
    
    //Soporte(1,-1,B)
    translate([lonX/2-espesor,-lonY/2+diametroPoste,lonZ/4-(minkow/4)])
    cube([diametroPoste/2,diametroPoste/2,lonZ/2-(minkow/2)],center=true);
    
    
    //Poste (-1,-1)
    translate([diametroPoste-lonX/2,diametroPoste-lonY/2,0])
    poste(diametroPoste,tornillo,lonZ);
    
    //Soporte(-1,-1,A)
    translate([-lonX/2+diametroPoste,-lonY/2+espesor,lonZ/4-(minkow/4)])
    cube([diametroPoste/2,diametroPoste/2,lonZ/2-(minkow/2)],center=true);
    
    //Soporte(-1,-1,B)
    
    translate([-lonX/2+espesor,-lonY/2+diametroPoste,lonZ/4-(minkow/4)])
    cube([diametroPoste/2,diametroPoste/2,lonZ/2-(minkow/2)],center=true);

}


}//fin parte 0


if(parte==1){
    
        translate([0,0,-lonZ/4])
    intersection(){
       
        difference(){
            resize([lonX,lonY,espesor])
          cylinder(d=lonX*1,h=espesor*2,$fn=16);
            resize([lonX-2*espesor,lonY-2*espesor,espesor*2])
        cylinder(d=(lonX*1)-2*espesor,center=true,h=espesor*4,$fn=16);
            }    
            
       soporte(lonX=lonX,lonY=lonY,lonZ=lonZ,soporteA=soporteA,soporteB=soporteB,soporteC=soporteC,soporteD=soporteD);
            
}
    
difference(){
       if(minkow>0){
     recortadorMink(lonX,lonY,lonZ,minkow);
    }

        cube([lonX-(2*espesor),lonY-(2*espesor),lonZ-(2*espesor)],center=true);
        
    translate([0,0,lonZ])
        cube([lonX*2,lonY*2,lonZ*2],center=true);
    
    //ESPESOR ESTANDAR de 1.5, es necesario mencionar solo se ha probado con éste espesor
    //FALTA AJUSTE
    
	ajusteEsfera=0.75;
    if(diametroPoste>0){
    //Aguero para tornillo (1,1)
    translate([-diametroPoste+lonX/2,-diametroPoste+lonY/2,-lonZ/2])
    //poste(diametroPoste,tornillo,lonZ);
        sphere(d=diametroPoste*ajusteEsfera);
       
    //Aguero para tornillo  (-1,1)
  // translate([-diametroPoste+lonX/2,+diametroPoste-lonY/2,-lonZ/2-(espesor*3)])
        translate([-diametroPoste+lonX/2,+diametroPoste-lonY/2,-lonZ/2])
   //poste(diametroPoste,tornillo,lonZ);
        sphere(d=diametroPoste*ajusteEsfera);


   //Aguero para tornillo  (1,-1)
   translate([diametroPoste-lonX/2,-diametroPoste+lonY/2,-lonZ/2])
  // poste(diametroPoste,tornillo,lonZ);
sphere(d=diametroPoste*ajusteEsfera);

   //Aguero para tornillo  (-1,-1)
   translate([diametroPoste-lonX/2,diametroPoste-lonY/2,-lonZ/2])
   //poste(diametroPoste,tornillo,lonZ);
   sphere(d=diametroPoste*ajusteEsfera);
   
    }//fin opcion para quitar postes
}

if(diametroPoste>0){


//Poste (1,1)
translate([-diametroPoste+lonX/2,-diametroPoste+lonY/2,-lonZ/2+(3*espesor/8)])
    
 difference(){
poste(diametroPoste,tornillo,lonZ-(3*espesor/4));
    sphere(d=diametroPoste);
        }


//Soporte(1,1,A)
translate([lonX/2-diametroPoste,lonY/2-espesor,-lonZ/4+(minkow/4)])
cube([diametroPoste/2,diametroPoste/2,lonZ/2-(minkow/2)],center=true);

//Soporte(1,1,B)
translate([lonX/2-espesor,lonY/2-diametroPoste,-lonZ/4+(minkow/4)])
cube([diametroPoste/2,diametroPoste/2,lonZ/2-(minkow/2)],center=true);

//Poste (-1,1)
translate([-diametroPoste+lonX/2,diametroPoste-lonY/2,-lonZ/2+(3*espesor/8)])
    
 difference(){
poste(diametroPoste,tornillo,lonZ-(3*espesor/4));
    sphere(d=diametroPoste);
        }


//Soporte(-1,1,A)
translate([-lonX/2+diametroPoste,lonY/2-espesor,-lonZ/4+(minkow/4)])
cube([diametroPoste/2,diametroPoste/2,lonZ/2-(minkow/2)],center=true);

//Soporte(-1,1,B)
translate([-lonX/2+espesor,lonY/2-diametroPoste,-lonZ/4+(minkow/4)])
cube([diametroPoste/2,diametroPoste/2,lonZ/2-(minkow/2)],center=true);

//Poste (1,-1)
translate([diametroPoste-lonX/2,-diametroPoste+lonY/2,-lonZ/2+(3*espesor/8)]) 
 difference(){
poste(diametroPoste,tornillo,lonZ-(3*espesor/4));
    sphere(d=diametroPoste);
        }
        
//Soporte(1,-1,A)
translate([lonX/2-diametroPoste,-lonY/2+espesor,-lonZ/4+(minkow/4)])
cube([diametroPoste/2,diametroPoste/2,lonZ/2-(minkow/2)],center=true);

//Soporte(1,-1,B)
translate([lonX/2-espesor,-lonY/2+diametroPoste,-lonZ/4+(minkow/4)])
cube([diametroPoste/2,diametroPoste/2,lonZ/2-(minkow/2)],center=true);

//Poste (-1,-1)
translate([diametroPoste-lonX/2,diametroPoste-lonY/2,-lonZ/2+(3*espesor/8)])  
 difference(){
poste(diametroPoste,tornillo,lonZ-(3*espesor/4));
    sphere(d=diametroPoste);
        }
        
//Soporte(-1,-1,A)
translate([-lonX/2+diametroPoste,-lonY/2+espesor,-lonZ/4+(minkow/4)])
cube([diametroPoste/2,diametroPoste/2,lonZ/2-(minkow/2)],center=true);

//Soporte(-1,-1,B)
translate([-lonX/2+espesor,-lonY/2+diametroPoste,-lonZ/4+(minkow/4)])
cube([diametroPoste/2,diametroPoste/2,lonZ/2-(minkow/2)],center=true);

}//fin opcion para quitar postes


}//fin parte 1





}//Fin modulo case gamma


//#######POSTE CASE GAMMA#####
module poste(diametroPoste=5,tornillo=2.5,alturaTornillo=10){
  
    translate([0,0,alturaTornillo/4])
            difference(){

                cylinder(d=diametroPoste,h=alturaTornillo/2,center=true);
                cylinder(d=tornillo,h=alturaTornillo*2,center=true);
               }
           

}//Fin modulo poste (para case gamma)


module soporte(lonX,lonY,lonZ,soporteA,soporteB,soporteC,soporteD){
    
    
  if(soporteA==1){
         translate([-lonX/4,lonY/4,0])
          cube([lonX/2,lonY/2,lonZ/2],center=true);      
         }
         
        
      if(soporteB==1){
         translate([lonX/4,lonY/4,0])
          cube([lonX/2,lonY/2,lonZ/2],center=true);      
         }
        
         
        if(soporteC==1){
         translate([-lonX/4,-lonY/4,0])
          cube([lonX/2,lonY/2,lonZ/2],center=true);      
         }
         
         if(soporteD==1){
         translate([lonX/4,-lonY/4,0])
          cube([lonX/2,lonY/2,lonZ/2],center=true);      
         }
           else{
                cube([0,0,0],center=true);
               }

}


//####RENDERIZADOS####

//$fn=100;


//No deben existir renders para la compatibilidad con otros proyectos (debugging)

/*lonX=60;
lonY=80;
lonZ=10;
espesor=1.5;
diametroPoste=5;
tornillo=2.5;
*/


//CASE TEMINI
/*
//ajGruPost=6;
ajGruPost=8;
difference(){
CASEgamma(lonX=42+ajGruPost,lonY=16+ajGruPost,medZ=11,espesor=1.5,diametroPoste=5,tornillo=2.5,parte=0,minkow=3,soporteA=0,soporteB=0,soporteC=0,soporteD=0);
d1=12;
translate([d1/2-1,0,0])
cylinder(d=d1,h=30,center=true);

d2=11;
translate([-d2/2+1,0,0])
cylinder(d=d2,h=30,center=true);

   
}

difference(){
CASEgamma(lonX=42+ajGruPost,lonY=16+ajGruPost,medZ=6.5,espesor=1.5,diametroPoste=5,tornillo=2.5,parte=1,minkow=3,soporteA=0,soporteB=0,soporteC=0,soporteD=0);

translate([36/2,0,0])
cylinder(d=2.5,h=30,center=true);

translate([-36/2,0,0])
cylinder(d=2.5,h=30,center=true);

translate([0,-15,-5/2])
cube([10,30,5],center=true);
}
//poste(diametroPoste=5,tornillo=2.5,alturaTornillo=10,modo=1);
*/


 
