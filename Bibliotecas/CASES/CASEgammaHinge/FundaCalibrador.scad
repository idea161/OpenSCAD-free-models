use<LogoIdea161.scad>;
//lon=20;

/*hull(){
    square([lon,0.01]);

    square([0.01,lon]);

    translate([lon,0])
    square([0.01,lon]);


    translate([0,lon])
    square([lon,0.01]);
}*/

//#####MODULOS####

module caliperSilhoutte(){
    pt=0.01;

lonX=238;
lonY=79.12;

lonX1=30;
lonY1=27.88;

lonX2=60.27;
lonY2=10.83;

lonX2_2=lonX-(lonX1+lonX2);

lonX3=16.54;
lonY3=8.46;

lonX4=35.56;
lonY4=6.69;

lonX5=28.36;
lonY5=7.6;
   
lonX6=lonX-(lonX3+lonX4+lonX5);
lonY6=lonY-(lonY1+lonY2+lonY3+lonY4+lonY5);


hull(){
    //LINEAS INFERIORES
    square([lonX1,pt]);
  
    translate([lonX1,0])
    square([pt,lonY1]);
  
    translate([lonX1,lonY1])
    square([lonX2,pt]);
  
    translate([lonX1+lonX2,lonY1])
    square([pt,lonY2]);
    
    translate([lonX1+lonX2,lonY1+lonY2])
    square([lonX2_2,pt]);
  
    //LINEAS SUPERIORES
    
    translate([0,lonY])
    square([lonX3,pt]);
    
     translate([lonX3,lonY-lonY3])
    square([pt,lonY3]);
     
     translate([lonX3,lonY-lonY3])
    square([lonX4,pt]);
    
     translate([lonX3+lonX4,lonY-lonY3-lonY4])
    square([pt,lonY4]);
     
     translate([lonX3+lonX4,lonY-lonY3-lonY4])
    square([lonX5,pt]);
  
     translate([lonX3+lonX4+lonX5,lonY-lonY3-lonY4-lonY5])
    square([pt,lonY5]);
  
     translate([lonX3+lonX4+lonX5,lonY-lonY3-lonY4-lonY5])
    square([lonX6,pt]);
    
     translate([lonX3+lonX4+lonX5+lonX6,lonY-lonY3-lonY4-lonY5-lonY6])
    square([pt,lonY6]);
    
    //LINEA DE CIERRE
        square([pt,lonY]);
   
}

}

//Figura cerrada
/*
linear_extrude(height=3)
union(){
    square([lonX3,lonY]);
    square([lonX1,lonY-lonY3]);
    translate([0,lonY1])
    square([lonX3+lonX4,lonY-lonY3-lonY1]);
    translate([0,lonY1])
    square([lonX3+lonX4+lonX5,lonY-lonY3-lonY1-lonY4]);
    translate([0,lonY1])
    square([lonX3+lonX4+lonX5+(lonX6-lonX2_2),lonY-lonY3-lonY1-lonY4-lonY5]);

    translate([0,lonY1+lonY2])
    square([lonX,lonY6]);
}*/

//######RENDERIZADOS#######

$fn=100;

lonX=238;
lonY=79.12;

grosorCaliper=16.72;
espesor=3;
//translate([0,0,3])
/*difference(){
    linear_extrude(height=(espesor*2)+grosorCaliper)
    resize([lonX+(2*espesor),lonY+(2*espesor)])
    caliperSilhoutte();
    translate([0,espesor,espesor])
   linear_extrude(height=grosorCaliper*2)
   
    caliperSilhoutte();

}*/
/*
translate([lonX/4,lonY/2,grosorCaliper+(1.5*espesor)])
rotate(90)
linear_extrude(height=espesor)
Idea(escala=0.5,mini=0);
*/
