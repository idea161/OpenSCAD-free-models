 
# CASEgamma

![](./PNG/CASEgamma.png)

## Summary/Descripción


If you are looking for a model to make the perfect case for your projects and compile it in scorchCAD, this is the ideal code for you.
This OpenSCAD language code is parameterizable to make CASES with long, high and wide thickness parameterizable and with modifiable text on the cover.

Si estás buscando un modelo para hacer el case perfecto para tus proyectos y compilarlo en scorchCAD, éste es el código ideal para ti.
Éste código en lenguaje de  OpenSCAD es parametrizable para hacer CASES con espesor largo, alto y ancho parametrizable y con texto modificable en la tapa.



