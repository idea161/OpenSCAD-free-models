include<./../../../Bibliotecas/Utilidades/Utilities.scad>;
include<./../../../Ditac/Ditac3D/OpenSCAD/Technic.scad>;

/*
####Explicación de variables#####
lonX                                    ->  longitud de cubo a esculpir en X
lonY                                    ->  longitud de cubo a esculpir en Y
lonZ                                    ->  longitud de cubo a esculpir en Z
espesor                             ->  espesor de cubo a esculpir
redondeadorMinkowski    -> radio de la esfera que se ocupa para la funcion Minkowki
//por borrar aqui
alturaSolapa                    ->altura extension de case para hacer que embone
*/

module CASepsilon(lonX=90,lonY=90,medZ=180,espesor=1.5,parte=0,minkow=3,tol=0.1,    factor=0.55){

lonZ=medZ*2;
 
  //  factor=0.52;

if(parte==0){
   
    difference(){
        if(minkow>0){
         recortadorMink(lonX,lonY,lonZ,minkow);
        }
        else{
            cube([lonX,lonY,lonZ],center=true);
        }
            cube([lonX-(2*espesor),lonY-(2*espesor),lonZ-(2*espesor)],center=true);
            
        translate([0,0,-lonZ])
            cube([lonX*2,lonY*2,lonZ*2],center=true);
            
    }
    
        //clip extremo A negativo
    translate([-lonX/2+(espesor*(1-tol)),(lonY/2)-(4*espesor),espesor])
    innerClip(espesor=espesor,factor=factor);
    
    //clip central negativo
    translate([-lonX/2+(espesor*(1-tol)),0,espesor])
    innerClip(espesor=espesor,factor=factor);
    
    //clip extremo B negativo
    translate([-lonX/2+(espesor*(1-tol)),-(lonY/2)+(4*espesor),espesor])
    innerClip(espesor=espesor,factor=factor);
    
     //clip extremo A positivo
    translate([lonX/2-(espesor*(1-tol)),(lonY/2)-(4*espesor),espesor])
    rotate(180)
    innerClip(espesor=espesor,factor=factor);
     
    //clip central positivo
    translate([lonX/2-(espesor*(1-tol)),0,espesor])
    rotate(180)
    innerClip(espesor=espesor,factor=factor);
    
        //clip extremo B positivo
    translate([lonX/2-(espesor*(1-tol)),-(lonY/2)+(4*espesor),espesor])
    rotate(180)
    innerClip(espesor=espesor,factor=factor);
     

}//fin parte 0

if(parte==1){
   
translate([0,0,espesor/2])
cube([lonX-(espesor*2),lonY-(espesor*2),espesor],center=true);
    
      //clip extremo A negativo
    translate([-(lonX/2)+espesor,(lonY/2)-(4*espesor),0])
    outterClip(espesor=espesor,factor=factor,tol=0.1);
    
    //clip central negativo
    translate([-(lonX/2)+espesor,0,0])
    outterClip(espesor=espesor,factor=factor,tol=0.1);
    
     //clip extremo B negativo
    translate([-(lonX/2)+espesor,-(lonY/2)+(4*espesor),0])
    outterClip(espesor=espesor,factor=factor,tol=0.1);
    
    //clip extremo A positivo
    rotate(180)
    translate([-(lonX/2)+espesor,(lonY/2)-(4*espesor),0])
    outterClip(espesor=espesor,factor=factor,tol=0.1);
    
    //clip central positivo
    rotate(180)
    translate([-(lonX/2)+espesor,0,0])
    outterClip(espesor=espesor,factor=factor,tol=0.1);
    
     //clip extremo B positivo
    rotate(180)
    translate([-(lonX/2)+espesor,-(lonY/2)+(4*espesor),0])
    outterClip(espesor=espesor,factor=factor,tol=0.1);

}//fin parte 1


}//Fin modulo casEpsilon


module innerClip(espesor=1.5,factor=0.5){
    
    rotate([90,90,0])
    translate([-espesor,0,-espesor])
    linear_extrude(height=espesor*2)
    //polygon([[0,0],[espesor,0],[0,espesor*factor]]);
    polygon([[0,0],[espesor,0],[espesor,espesor*factor]]);
    
    }

 //!innerClip(espesor=1.5,factor=0.5);

module outterClip(espesor=1.5,factor=0.5,tol=0.1){
    
    rotate([90,0,0])
    translate([0,0,-espesor*1.5])
       
    difference(){
         linear_extrude(espesor*3)
        polygon([[0,0],[espesor*1.25,0],[espesor*0.75,espesor*2.5],[0,espesor*2.5]]);
    
        translate([0,espesor,0.5*espesor])
        cube([espesor*factor,espesor,espesor*2]);
     }        
    
}

//!outterClip(espesor=1.5,factor=0.5,tol=0.1);

//####RENDERIZADOS####

//No deben existir renders para la compatibilidad con otros proyectos (debugging)

$fn=10;



//HELLO WORLD TEST
/*
factor=0.7;

difference(){
	union(){
		color("cyan")
		CASepsilon(lonX=17.5+13,lonY=23.5+1,medZ=5.3*2+1,espesor=1.5,parte=0,minkow=3,tol=0.1,    factor=factor);

		CASepsilon(lonX=17.5+13,lonY=23.5+1,medZ=5.3*2+1,espesor=1.5,parte=1,minkow=3,tol=0.1,    factor=factor);
	}
	
	translate([-20,0,-20])
	color("red")
	cube([40,40,40]);
	
}
*/

//CASE BATERIA


/*difference(){
CASepsilon(lonX=17.5+13,lonY=23.5+1,medZ=5.3*2+1,espesor=1.5,parte=0,minkow=3,soporteA=0,soporteB=0,soporteC=0,soporteD=0);



    translate([13.5/2+4,-23.5/2,(5.3/2)-2])
    cube([3,10,18],center=true);
    
}
*/


//CASE POWER BANK LEGO POWER FUNCTIONS
factor=0.7;
espesor=1.5;
lonX=20+(2*espesor);
lonY=38;
medZ=11;
alto=medZ+espesor;

	union(){
		color("cyan")
		difference(){
		   CASepsilon(lonX=lonX,lonY=lonY,medZ=medZ,espesor=espesor,parte=0,minkow=3,tol=0.1,    factor=factor);
		   translate([-9/2,0,0])
			cube([9,lonY,6+espesor]);
			
			rotate(180)
			translate([-6/2,0,0])
			cube([6,lonY,3+espesor]);
			
	     }
		
		 translate([(lonX/2)+4-espesor+0.5,-12,alto-8-espesor])
		 rotate(90)
		 barrasTech(tol=0.1,tamaNio=4);
		
	}
	
 !CASepsilon(lonX=lonX,lonY=lonY,medZ=medZ,espesor=espesor,parte=1,minkow=3,tol=0.1,    factor=factor);
		  


