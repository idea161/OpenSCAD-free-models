﻿# Parameterizable box

![](./PNG/CajaParametrizable.png)

     -/+oooo+:.     
  `/syo/:::/+syo-   
 `sy+.     ```-sy/  
 +y+      +::+`-yy- 
 sy-      +-:o:-oy/ 
 sy-       ``   +y/ 
 sy-            /y/ 
 sy-            /y/ 
 sy.            /y/ 
 sy/............oy/ 
 sysoooooooooooosy/ 
`sy/............oy+ 
syyyssssssssssssyyy/
/++++oys++++yy+++++-
     /y+    yy.     
     /y+    yy.     
     `-`    ss`       
## Summary/Descripción 


The following code is used to make DXF drawings that can be used to machine MDF or acrylic boxes (materials that can be used in CNC design machines or laser cutting) and can be entered values of thickness, number of joints, length in dimension in "x" "y" & "z", in addition to this it can be included folding lid or not.
La sugerencia de uso que damos, es que se puede usar para proyecto de construcción con los planos, objetos armables, cajas variantes que puede incluir o no, una tapa.

El siguiente código sirve para hacer planos en DXF que sirvan para maquinar cajas de MDF o acrílico (materiales que se puedan ocupar en máquinas de diseño CNC o corte láser) y se les pueda ingresar valores de espesor, número de empalmes, longitud en dimensión en "x" "y" & "z", además de ésto se le puede incluir tapa abatible o no.
La sugerencia de uso que damos, es que se puede usar para proyecto de construcción con los planos, objetos armables, cajas variantes que puede incluir o no, una tapa.

