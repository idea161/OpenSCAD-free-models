/*
     -/+oooo+:.     
  `/syo/:::/+syo-   
 `sy+.     ```-sy/  
 +y+      +::+`-yy- 
 sy-      +-:o:-oy/ 
 sy-       ``   +y/ 
 sy-            /y/ 
 sy-            /y/ 
 sy.            /y/ 
 sy/............oy/ 
 sysoooooooooooosy/ 
`sy/............oy+ 
syyyssssssssssssyyy/
/++++oys++++yy+++++-
     /y+    yy.     
     /y+    yy.     
     `-`    ss` 
    
     IDEA 1.61        
*/


//modulos para el renderizado de varios tipos de caja a manufacturear en la máquina de corte lAser

//Largo se desplaza en "X"
//Ancho se desplaza en "Y"

module base(largo=80,ancho=80,espesor=3,dientes=20,Up=1,Down=1,Left=1,Right=1){
tamDientesLargo=(largo)/dientes;
tamDientesAncho=(ancho)/dientes;


union(){

//Dientes derecha
  if(Right==1){
    translate([largo/2,-ancho/2-tamDientesAncho/2])
    for(i=[1:dientes/2]){
       translate([0,tamDientesAncho*2*i])
       square([2*espesor,tamDientesAncho],center=true);
    }
}

//Dientes Izquierda

if(Left==1){
   translate([-largo/2,-ancho/2-3*tamDientesAncho/2])
   for(i=[1:dientes/2]){
      translate([0,tamDientesAncho*2*i])
      square([2*espesor,tamDientesAncho],center=true);
   }
}

//Dientes Arriba

if(Up==1){
   translate([-largo/2-3*tamDientesLargo/2,ancho/2])
   for(i=[1:dientes/2]){
     translate([tamDientesLargo*2*i,0])
     square([tamDientesLargo,2*espesor],center=true);
   }
}

//Dientes Abajo

if(Down==1){
   translate([-largo/2-tamDientesLargo/2,-ancho/2])
    for(i=[1:dientes/2]){
     translate([tamDientesLargo*2*i,0])
     square([tamDientesLargo,2*espesor],center=true);
    }
}

//Cuadrado
square([largo,ancho],center=true);
}

}//Fin module base


module pruebaBase (largo=40,ancho=30,alto=40,espesor=3,dientes=6){

  for(i=[1:3]){
    for(j=[1:2]){
      translate([((i*largo)+(i*2.5*espesor)),((j*ancho)+(j*2.5*espesor))])
      base(largo,ancho,espesor,dientes);
    }
  }

}//Fin module pruebaBase

//####CUBO BASE####//

module CuboBase (largo=40,ancho=30,alto=40,espesor=3,dientes=6){

//base y tapa
  for(i=[0:1]){
      translate([((i*largo)+(i*2.5*espesor)),0])
      base(largo,ancho,espesor,dientes,1,1,1,1);
  }

//lados largo
 translate([0,(ancho+alto)/2+(2.5*espesor)])
for(i=[0:1]){
   translate([((i*largo)+(i*2.5*espesor)),0])
   base(largo,alto,espesor,dientes,1,  1,1,1);
}

//lados alto
 translate([0,-ancho-(2.5*espesor)])
   for(i=[0:1]){
      translate([((i*alto)+(i*2.5*espesor)),0])
     base(alto,ancho,espesor,dientes,1,1,1,1);
   }

}//Fin module CuboBase

//#####CAJA SIN O CON TAPA####//

module cajaSCTapa (largo=40,ancho=30,alto=40,espesor=3,dientes=6,modo=1,
  tolBisagras=2.2){

//base
        
    if(modo==2){
    base(largo,ancho,espesor,dientes,1,1,0,0);
        }
       else{
        base(largo,ancho,espesor,dientes,1,1,1,1);
           }
    
//Tapa
    
 if(modo==1){
    translate([((largo)+(2.5*espesor)),0])
    square([largo,ancho],center=true);
    
    translate([((largo)+(2.5*espesor)),-ancho/2+(1.5*espesor)])
    square([largo+2*espesor,espesor],center=true);
  
     translate([((largo)+(2.5*espesor)),+ancho/2])
    square([2*espesor,2*espesor],center=true);
 
     //}

//lados largo
 
 //if(modo==1){
 
    translate([0,(ancho+alto)/2+(2.5*espesor)])
   difference(){
       for(i=[0:1]){
          translate([((i*largo)+(i*2.5*  espesor)),0])
           difference(){
          base(largo,alto,espesor,dientes,0,  1,1,1);
               translate([0,(alto)/2])
           square([i*2*espesor,i*2*espesor],center=true);
            }
       }
       
       
     translate([0,alto])
    square([2*espesor,2*espesor],center=true);
   }
   
   //lados alto
  
      //reinicio
    translate([0,-ancho-(2.5*espesor)])
 difference(){
  union(){
   for(i=[0:1]){
      translate([((i*alto)+(i*2.5*espesor)),0])
     base(alto,ancho,espesor,dientes,1,1,0,1);
   }
   //circulos orillas (Material)
   
   translate([-((alto-espesor)/2),(ancho/2)-(1.5*espesor)])
   circle($fn=100,r=2*(espesor/2+0.5));
   
   translate([-(((alto-espesor)/2))+alto+(2.5*espesor),-(ancho/2)+(1.5*espesor)])
   circle($fn=100,r=2*(espesor/2+0.5));
   
  }
  
  //taladros bisagras
  
  
   translate([-(((alto-espesor)/2)),(ancho/2)-(1.5*espesor)])
   circle($fn=100,r=(espesor/2+0.5)-tolBisagras);
  
  translate([-(((alto-espesor)/2))+alto+(2.5*espesor),-(ancho/2)+(1.5*espesor)])
   circle($fn=100,r=(espesor/2+0.5)-tolBisagras);


 
  }
  
 
   
 }//fin modo 1
 
 
 if(modo==2){
      translate([0,(ancho+alto)/2+(2.5*espesor)])
   for(i=[0:1]){
      translate([((i*largo)+(i*2.5*  espesor)),0])
      base(largo,alto,espesor,dientes,0,  1,0,0);   
   }
 
 } 
 





 if(modo==3){
       translate([0,(ancho+alto)/2+(2.5*espesor)])
   for(i=[0:1]){
      translate([((i*largo)+(i*2.5*  espesor)),0])
      base(largo,alto,espesor,dientes,0,  1,1,1);   
   }
   
      translate([0,-(ancho+alto)/2-(2.5*espesor)])
   for(i=[0:1]){
      translate([((i*ancho)+(i*2.5*  espesor)),0])
      base(ancho,alto,espesor,dientes,1,  0,1,1);   
   }
 }
 
 if(modo==0){
   //reinicio
   translate([0,-ancho-(2.5*espesor)])
     for(i=[0:1]){
      translate([((i*alto)+(i*2.5*espesor)),0])
     base(alto,ancho,espesor,dientes,1,1,0,1);
   }
 }
  
  
}//Fin module cajaSCTapa

//####RENDERIZADOS#####//



//caja Daniela
/*espe=3;
dim1=106;
dim2=200;
cajaSCTapa(largo=dim1-(2*espe),ancho=dim2-(2*espe),alto=dim1-(2*espe),espesor=espe,dientes=8,modo=0,tolBisagras=0);
*/


//caja Dulce
//cajaSCTapa(largo=320,ancho=150,alto=150,espesor=3,dientes=8,modo=0,tolBisagras=0);

//material necesario 82x30 o mayor (aprox 123)
//mas aprox 50 de corte
//mas 3 por 9 de cosas o mas 18

//cajaSCTapa(largo=50,ancho=280,alto=180,espesor=3,dientes=8,modo=0,tolBisagras=0);

//caja Atzin

/*
espe=3;
dim1=50;
dim2=280;
cajaSCTapa(largo=dim1-(2*espe),ancho=dim2-(2*espe),alto=180-(2*espe),espesor=espe,dientes=8,modo=0,tolBisagras=0);
//52x52cm costo de 136
*/

//cajon Atzin
/*
espe=3;
dim3=50-(2*espe);
dim4=(280-(2*espe))/3;
cajaSCTapa(largo=180-(3*espe),ancho=dim4-(2*espe),alto=dim3-(2*espe),espesor=espe,dientes=8,modo=0,tolBisagras=0);
//40cmx15cm costo de 30 (3 piezas)
*/


//cajaRudy
//cajaSCTapa(largo=100,ancho=100,alto=20,espesor=3,dientes=8,modo=1,tolBisagras=0);

//cajaSCTapa(largo=175,ancho=15,alto=105,espesor=3,dientes=8,modo=1,tolBisagras=0);


//sample
//cajaSCTapa(largo=50,ancho=30,alto=40,espesor=6,dientes=6,modo=1,tolBisagras=0);

/*
difference(){


//POSICIOAMIENTO

tolerancia=1;
LARGO1=87.07+(tolerancia*2);
ANCHO1=96.18+(tolerancia*2);
ALTO1=30;
ESPESOR1=3;
    
cajaSCTapa(largo=LARGO1,ancho=ANCHO1,alto=ALTO1,espesor=ESPESOR1,dientes=6,modo=1);


//####CENTRO BASE####
circle(r=3);
    
 //####CENTRO FRONTAL####
  translate([0,((ANCHO1/2)+(ALTO1/2)+(2.5*ESPESOR1))])   
    circle(r=3);
    
    //agujero para la tapa (frontal)
    translate([0,((ALTO1/2)+(ANCHO1/2)+(ALTO1/2)+(2.5*ESPESOR1))])   
    square([2*ESPESOR1,2*ESPESOR1],center=true);
    
    //####CENTRO POSTERIOR####
    translate([(LARGO1)+(2.5*ESPESOR1),((ANCHO1/2)+(ALTO1/2)+(2.5*ESPESOR1))])   
    circle(r=3);
    
//#### CENTRO TAPA ####
translate([((LARGO1)+(2.5*ESPESOR1)),0])  
circle(r=3);

//####CENTRO LATERAL 1 #####
translate([0,-((ANCHO1)+(2.5*ESPESOR1))])
circle(r=3);
    
    //####CENTRO LATERAL 2 ####
translate([ALTO1+(2.5*ESPESOR1),-((ANCHO1)+(2.5*ESPESOR1))])
circle(r=3);

}
*/

//Renderizado de la caja en base a la funcion caja SCTapa
// linear_extrude(height=3)


//cajaSCTapa(largo=150,ancho=100,alto=150,espesor=3,dientes=6,modo=0);
//ATZIN
//cajaSCTapa(largo=150,ancho=60,alto=120,espesor=3,dientes=10,modo=1);

//CAJA REGALOS 1
//linear_extrude(height=3)
//cajaSCTapa(largo=100,ancho=70,alto=80,espesor=6,dientes=8,modo=1,tolBisagras=0);

cajaSCTapa(largo=110,ancho=70,alto=50,espesor=3,dientes=6,modo=1,tolBisagras=0);



//CAJA REGALOS 2
//cajaSCTapa(largo=110,ancho=90,alto=60,espesor=3.5,dientes=4,modo=1);


 //base(largo,alto,espesor,dientes,0,  1,1,1);

//TARJETERO

//cajaSCTapa(largo=55,ancho=50,alto=37,espesor=3,dientes=6,modo=0,tolBisagras=0);

//cajaSCTapa(largo=80,ancho=80,alto=90,espesor=3,dientes=6,modo=0,tolBisagras=0);

//caja tornamesa
//cajaSCTapa(largo=350-(10),ancho=440-(10),alto=60-(10),espesor=5,dientes=8,modo=0,tolBisagras=0);


//DANIELA
//espesor=3;
//cajaSCTapa(largo=106-(2*espesor),ancho=85-(2*espesor),alto=150,espesor=6,dientes=6,modo=0,tolBisagras=0);
    
/*    
    x:215

y:190

z:170

y de la tapa

x:215

y:190

z:10
*/


//caja gabinete
/*
espe=6;
cajaSCTapa(largo=190-(2*espe),ancho=170-(2*espe),alto=215-(2*espe),espesor=espe,dientes=8,modo=1,tolBisagras=0);
*/

//caja medidor  nivel
//cajaSCTapa(largo=150,ancho=70,alto=145,espesor=3,dientes=6,modo=0,tolBisagras=0);

//caja combustion
/*
difference(){
CuboBase(largo=100,ancho=100,alto=100,espesor=3,dientes=6);

translate([0,33,0])
    
circle(d=19.24+0.5);

//17.46
translate([0,-33,0])    
    circle(d=17.76
    );
 
    //16
    circle(d=17.14);
    //NO HAY TOLERANCIA
    translate([33,33,0])
//5.72    
    circle(d=5.52);
    
    
    translate([-33,-33,0])   
   //5.72 
    circle(d=5.52);
}
*/


//pruebaBase(largo=40,ancho=30,alto=40,espesor=3,dientes=6,3D=0);


//base(80,80,3,10,1,1,1,1);


//Roberto: 25*20*10 cm 