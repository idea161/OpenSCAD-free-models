

module tapa(conectores=0){

    difference(){
       square([51.19,32],center=true);
        translate([-1.07,-1.58])
        if(conectores==1){
            translate([(29.95-10)/2,16-5-2.5])
            circle(d=10.4);
            
            translate([-(29.95-10)/2,16-5-2.5])
            circle(d=10.4);
        }
        
        //translate([-(33)/2,-16+2.25+2])
        translate([-(29.5)/2,-16+3])
        circle(d=3);
        //checar tamaño
        
         //translate([(33)/2,-16+2.25+2])
        translate([(29.5)/2,-16+3])
        circle(d=3);
        
    }

}

//########RENDERIZADOS###########


$fn=100;

//linear_extrude(height=3)
//tapa(conectores=0);


difference(){
  
    union(){
       cube([60,3,20],center=true);
         
        translate([22,-1.5,0])
        cube([8,3,20]);
    }  
  
    //tornillo 1
    translate([20,0,0])
    rotate([90,0,0])
    cylinder(d=6,h=30,center=true);
    //tornillo 2
      translate([-20,0,0])
    rotate([90,0,0])
    cylinder(d=6,h=30,center=true);
}

//POLICARBONATO CELULAR


//perfil U 3.66m $117

//perfil H 4.88mm $298

//tornillo $2.7 cada tornillo (no apto para acero

