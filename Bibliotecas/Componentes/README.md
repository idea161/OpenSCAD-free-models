 
# Componentes

![](./CASEgamma.png)

## Summary/Descripción

Many times in design projects we do not find a good enough and adaptable cad for our projects, so we have created this project to facilitate the integration of your favorite cards and sensors such as the Arduino and Raspberry Pi cards.

Muchas veces en proyectos de diseño no encontramos un cad lo suficientemente bueno y adaptable para nuestros proyectos, por lo que hemos creado éste proyecto para facilitar la integración de tus tarjetas y sensores favoritas como lo son las tarjeta Arduino y Raspberry Pi

