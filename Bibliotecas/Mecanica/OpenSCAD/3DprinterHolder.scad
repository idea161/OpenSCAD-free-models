diamFilamento=57;
alturaFilamento=65;
diamTorAnet=21.5;
diamTorSkate=8;
diamBalSkate=22;
alturaBalSkate=7;
tol=1;

$fn=100;

posteMontura=1;
holder=0;

if(posteMontura==1){

    difference(){
        union(){
            hull(){
                 cylinder(d=diamBalSkate*1.5,h=alturaBalSkate);
                translate([diamBalSkate,0,0])
                 cylinder(d=diamBalSkate,h=alturaBalSkate);
            }
            cylinder(d=diamTorAnet,h=alturaBalSkate*2);
        }
    cylinder(d=diamTorSkate,h=alturaBalSkate*8,center=true);
        
         translate([diamBalSkate,0,0])
                 cylinder(d=5,h=alturaBalSkate*4,center=true);
        
        translate([diamBalSkate,0,0])
                 cylinder(d=8,h=alturaBalSkate,center=true);
        
    }

}

if(holder==1){
    difference(){
        cylinder(d=diamFilamento,h=alturaFilamento);
        cylinder(d=diamBalSkate+(tol*1.5),h=alturaBalSkate);
        translate([0,0,alturaFilamento-alturaBalSkate])
        cylinder(d=diamBalSkate+(tol*1.5),h=alturaBalSkate);
        cylinder(d=diamTorSkate+1,h=alturaFilamento*2,center=true);
    }
}