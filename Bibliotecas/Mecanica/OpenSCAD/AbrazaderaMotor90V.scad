
/*
->10.76mm profundidad maletin
->Plástico maletín 9 mm
->97 mm diámetro mayor motor
->67mm diámetro menor motor
->7cm espacio motor con Maletín(encoder)
->11cm altura fondo con
->7cm largo para abrazadera doble
*/

$fn=100;

largoAbrazadera=70;
diamMenorMotor=67;
diamMayorMotor=97;
tol=1;
espesor=15+tol;

diamTor=8;

superior=2;
base=1;

//echo((diamMayorMotor-diamMenorMotor)/2)


if(superior==1){
    difference(){
        //cilindro mayor
       cylinder(d=diamMenorMotor+(2*espesor),h=largoAbrazadera,center=true);
        
        //cilindro para motor
        cylinder(d=diamMenorMotor+tol,h=largoAbrazadera*2,center=true);
        
        translate([0,-largoAbrazadera,0])
        cube([largoAbrazadera*2,largoAbrazadera*2,largoAbrazadera*2],center=true);
        
        //agujero tornillo (1,1)
        translate([(diamMenorMotor/2)+(espesor/2),0,(largoAbrazadera/2)-(espesor/2)])
        rotate([90,0,0])
           cylinder(d=diamTor+tol,h=largoAbrazadera*2,center=true);
        
        //tuerca(1,1)
         translate([(diamMenorMotor/2)+(espesor/2),espesor,(largoAbrazadera/2)-(espesor/2)])
        rotate([-90,90,0])
           cylinder(d=14.5+tol,h=largoAbrazadera*2,$fn=6);
        
            
        //agujero tornillo (-1,1)
        translate([(-diamMenorMotor/2)-(espesor/2),0,(largoAbrazadera/2)-(espesor/2)])
        rotate([90,0,0])
           cylinder(d=diamTor+tol,h=largoAbrazadera*2,center=true);
        
        //tuerca(-1,1)
         translate([-(diamMenorMotor/2)-(espesor/2),espesor,(largoAbrazadera/2)-(espesor/2)])
        rotate([-90,90,0])
           cylinder(d=14.5+tol,h=largoAbrazadera*2,$fn=6);
           
                   
        //agujero tornillo (-1,-1)
        translate([(-diamMenorMotor/2)-(espesor/2),0,-(largoAbrazadera/2)+(espesor/2)])
        rotate([90,0,0])
           cylinder(d=diamTor+tol,h=largoAbrazadera*2,center=true);
        
        //tuerca(-1,-1)
         translate([-(diamMenorMotor/2)-(espesor/2),espesor,-(largoAbrazadera/2)+(espesor/2)])
        rotate([-90,90,0])
           cylinder(d=14.5+tol,h=largoAbrazadera*2,$fn=6);
           
               //agujero tornillo (1,-1)
        translate([(diamMenorMotor/2)+(espesor/2),0,-(largoAbrazadera/2)+(espesor/2)])
        rotate([90,0,0])
           cylinder(d=diamTor+tol,h=largoAbrazadera*2,center=true);
        
        //tuerca(1,-1)
         translate([(diamMenorMotor/2)+(espesor/2),espesor,-(largoAbrazadera/2)+(espesor/2)])
        rotate([-90,90,0])
           cylinder(d=14.5+tol,h=largoAbrazadera*2,$fn=6);
        
        
    }
}

if(base==1){
    difference(){
        //cubo a esculpir
        translate([-(diamMenorMotor+(2*espesor))/2,-((diamMenorMotor/2)+espesor),-largoAbrazadera/2])
        cube([diamMenorMotor+(2*espesor),(diamMenorMotor/2)+espesor,largoAbrazadera]);

      //cilindro para motor
            cylinder(d=diamMenorMotor+tol,h=largoAbrazadera*2,center=true);
        
           //agujero tornillo (1,1)
            translate([(diamMenorMotor/2)+(espesor/2),0,(largoAbrazadera/2)-(espesor/2)])
            rotate([90,0,0])
               cylinder(d=diamTor+tol,h=largoAbrazadera*2,center=true);
        
          //agujero tornillo (-1,1)
            translate([(-diamMenorMotor/2)-(espesor/2),0,(largoAbrazadera/2)-(espesor/2)])
            rotate([90,0,0])
               cylinder(d=diamTor+tol,h=largoAbrazadera*2,center=true);
                       
            //agujero tornillo (-1,-1)
            translate([(-diamMenorMotor/2)-(espesor/2),0,-(largoAbrazadera/2)+(espesor/2)])
            rotate([90,0,0])
               cylinder(d=diamTor+tol,h=largoAbrazadera*2,center=true);
        
              //agujero tornillo (1,-1)
            translate([(diamMenorMotor/2)+(espesor/2),0,-(largoAbrazadera/2)+(espesor/2)])
            rotate([90,0,0])
               cylinder(d=diamTor+tol,h=largoAbrazadera*2,center=true);
            

    }
}       