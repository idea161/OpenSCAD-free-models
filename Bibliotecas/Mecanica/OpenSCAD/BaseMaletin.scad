longX=309.5;
longY=272.5;

longTorX=342;
longTorY=255;

diamTor=6;

difference(){
   square([longX,longY],center=true);

   //tornillo 1
   translate([longTorX/2,longTorY/2])
   circle(d=diamTor);

   //tornillo 2
   translate([-longTorX/2,longTorY/2])
   circle(d=diamTor);

   //tornillo 3
   translate([longTorX/2,-longTorY/2])
   circle(d=diamTor);

   //tornillo 4
   translate([-longTorX/2,-longTorY/2])
   circle(d=diamTor);

}

