diamTornillo=5;
tol=1;
viajeMayor=30;
viajeMenor=15;


module piramide(base=20,ancho=10,viaje=10,factor=0.75){
    hull(){
        cube([base,ancho,0.01],center=true);
        
        translate([0,0,viaje])
        cube([base*factor,ancho*factor,0.01],center=true);
        
    }
}


$fn=10;

difference(){
    union(){
        rotate([-90,0,0])
        piramide(base=diamTornillo*4,ancho=diamTornillo,viaje=viajeMayor,factor=0.75);

        rotate([45+90,0,0])
        piramide(base=diamTornillo*4,ancho=diamTornillo,viaje=viajeMenor,factor=0.75);

        //codo
        rotate([0,90,0])
        cylinder(d=diamTornillo*1.6,h=diamTornillo*4,center=true);

        //puntaMayor
        translate([0,viajeMayor,0])
        rotate([0,90,0])
        cylinder(d=diamTornillo,h=diamTornillo*4,center=true);


        //puntaMenor
        rotate([45,0,0])
        translate([0,-viajeMenor,0])
        rotate([0,90,0])
        cylinder(d=diamTornillo,h=diamTornillo*4,center=true); 
    }

   //agujero para tornillo
    rotate([45/2,0,0])
   cylinder(d=diamTornillo+(tol*2),center=true,h=diamTornillo*6);

  //agujero para tornillo
//    rotate([45/2,0,0])
   //cylinder(d=diamTornillo+tol,center=true,h=diamTornillo*6);


   //corte1
   translate([(viajeMayor*2)+(diamTornillo*1.25),0,0])
   cube([viajeMayor*4,viajeMayor*4,viajeMayor*4],center=true);
    
   //corte2
   translate([-(viajeMayor*2)-(diamTornillo*1.25),0,0])
   cube([viajeMayor*4,viajeMayor*4,viajeMayor*4],center=true);

}