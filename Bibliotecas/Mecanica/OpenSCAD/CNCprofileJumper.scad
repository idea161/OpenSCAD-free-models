
$fn=100;

separacion=20;
linear_extrude(height=3)
difference(){
    minkowski(){
        square([10-8,65],center=true);
        circle(r=4);
    }
    translate([0,-separacion*1.5])
    for(i=[0:3]){
       translate([0,separacion*i])
        circle(d=5+1.5);
    }
}