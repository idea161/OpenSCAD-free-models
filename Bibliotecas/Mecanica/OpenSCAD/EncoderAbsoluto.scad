include<./../../../Ditac/Ditac3D/OpenSCAD/Technic.scad>;


module encoderIncemental(angulo=90,radio=10){

    Pi=3.141592654;
    Perimetro=radio*2*Pi;
    espacios=360/angulo;

    //echo(espacios);

    difference(){
       circle(r=radio);
        for(i=[0:espacios-1]){ 
            rotate(angulo*i*2)
           polygon([[0,0],[2*radio,0],[2*radio*cos(angulo),2*radio*sin(angulo)]]);
        }
    }

}

module espaciador(diamInt=5,diamExt=10){
    difference(){
         circle(d=diamExt);
        circle(d=diamInt);
    }
    
}

module seccionEncoder(diamInt=5,diamExt=10,angulo=90){
    
      difference(){
       encoderIncemental(angulo=angulo,radio=diamExt/2);   
       circle(d=diamInt);
    }
}


module recortadorEncoderAbsoluto(espIntCanalusr=1.5,espSensor=7,canales=4,diamShatfusr=6){

//Entre 5 y 8 cm para encoder
diamShaft=diamShatfusr+espIntCanalusr;
espIntCanal=espIntCanalusr;

        for(i=[0:canales]){

         rotate(90/pow(2,i))
         seccionEncoder(diamInt=(diamShaft+espSensor+espIntCanal)+((espSensor+espIntCanal)*i),diamExt=(diamShaft+espSensor+espIntCanal)+espSensor+((espSensor+espIntCanal)*i),angulo=90/pow(2,i));
            
        }

          //seccionEncoder(diamInt=5,diamExt=10,angulo=90);

        difference(){

        espaciador(diamInt=diamShaft,diamExt=diamShaft+espSensor);

        translate([0,-20])
        square([40,40]);
        }

       // circle(d=diamShaft-espIntCanalusr);
        circle(d=diamShatfusr);
  }
  
  module encoderAbsoluto(espIntCanalusr=1.5,espSensor=7,canales=4,diamShatfusr=6,tol=0.1){
      espesor=3;
      projection(){
          difference(){
              cylinder(r=(diamShatfusr/2)+(espIntCanalusr*canales)+(espSensor*(canales-1)),h=espesor/2,center=true);

             translate([0,0,-espesor/2])
             linear_extrude(height=espesor)
              //recortadorEncoderAbsoluto(espIntCanalusr=espIntCanalusr,espSensor=espSensor);
        
              recortadorEncoderAbsoluto(espIntCanalusr=espIntCanalusr,espSensor=espSensor,canales=canales,diamShatfusr=diamShatfusr-tol);
              
          }
      }
  }
  
    module encoder3canales(tamaNioCanal=6,diam=60,espIntCanal=1,angulo=5,tamaNioZ=1.5,sDoCH=0){
        
        difference(){
        circle(d=diam);
        
  seccionEncoder(diamInt=diam-(2*tamaNioCanal)-(2*espIntCanal),diamExt=diam-(2*espIntCanal),angulo=angulo);
   
	 if(sDoCH==1){
               rotate(angulo/2)
               seccionEncoder(diamInt=diam-(2*2*tamaNioCanal)-(3*espIntCanal),diamExt=diam-(2*2*espIntCanal)-(2*espIntCanal),angulo=angulo);
	 }
        translate([0,(diam/2)-(tamaNioCanal*2.5)-(espIntCanal*2),0])
        square([tamaNioZ,tamaNioCanal],center=true);
            
        }
      
  }
  
  module monRecLed1ch(ajusteHled=2,altura=50){
  

    translate([0,-altura,0])
 difference(){ 
    translate([-anchoPoste/2,0,0])
    
      
        translate([0,0,0])
        cube([anchoPoste,altura,Hled]); 
           
     //receptAculos
      translate([0,(diamShatfusr/2)+(espIntCanalusr*1.4),-0.1])
     
      translate([0,-ajusteHled,0])
            cylinder(d=led+tol,h=Hled-led/2);
      
        //apoyos esfericos
        translate([0,(diamShatfusr/2)+(espIntCanalusr*1.4),Hled-(led/2)])
        translate([0,-ajusteHled,0])
            sphere(d=led+tol);
        
        //filtros
        translate([0,(diamShatfusr/2)+(espIntCanalusr*1.4),0])
         translate([0,-ajusteHled,0])
             cylinder(d=2,h=Hled*4,center=true);
       
  }  
}
  

module monRecLed6ch(){
  

    translate([0,-53.4+7.3,0])
 difference(){ 
    translate([-anchoPoste/2,-anchoPoste/2-diamShatfusr,0])
    
      
        translate([0,12,0])
        cube([anchoPoste,lonPoste+3.9,Hled]); 
           
      
        
    
    
    
     //receptAculos
      translate([0,(diamShatfusr/2)+(espIntCanalusr*1.4),-0.1])
        for(i=[0:canales+1]){   
            translate([0,(led+espIntLed)*i,0])
            cylinder(d=led+tol,h=Hled-led/2);
        }
        
        //apoyos esfericos
        translate([0,(diamShatfusr/2)+(espIntCanalusr*1.4),Hled-(led/2)])
        for(i=[0:canales+1]){   
            translate([0,(led+espIntLed)*i,0])
            sphere(d=led+tol);
        }
        
        //filtros
        translate([0,(diamShatfusr/2)+(espIntCanalusr*1.4),0])
        for(i=[0:canales+1]){   
            translate([0,(led+espIntLed)*i,0])
             cylinder(d=2,h=Hled*4,center=true);
        }
        
  }  
}

module cuartoCilindro(medX=20,medY=30,medZ=40){
    
    resize([medX,medY,medZ])
    difference(){
    cylinder(d=40,h=40);
    
        translate([0,40,0])
        cube([80,80,80,],center=true);
        
        
        translate([40,0,0])
        cube([80,80,80,],center=true);
        }
}


module monturaLed6ch(){
    alturaTuerca=3.9;
    //dimNoMod=16;
    //dimNoMod=8.7;
    ajuste=7.3;
    ajusteEje=4;
    anchoTech=16;
    ajusteAnchoTech=4;
    dimNoMod=16-ajuste;
    //0.4 error
 
    //capucha technic montura frame
 translate([7,-7.9,4.8])
 difference(){
     translate([0,-4,0])
    cube([8,8,7.25],center=true);
     sphere(r=4);
 }
 
 
    //capucha technic montura frame
 translate([-7,-7.9,4.8])
 difference(){
     translate([0,-4,0])
    cube([8,8,7.25],center=true);
     sphere(r=4);
 }
 
 
 
    translate([6,-42.25,4])
    rotate([0,90,0])
    cuartoCilindro(medX=4.35,medY=12.75,medZ=2);
    
     translate([6,-42.5,0])
      cube([2,(12.75*2)+2,4]);
    
    mirror([1,0,0]){
        
         translate([6,-42.25,4])
    rotate([0,90,0])
    cuartoCilindro(medX=4.35,medY=12.75,medZ=2);
    
     translate([6,-42.5,0])
      cube([2,(12.75*2)+2,4]);
    
    }
    
    translate([0,-53.4,0])
 difference(){ 
    translate([-anchoPoste/2,-anchoPoste/2-diamShatfusr,0])
    union(){
        //conector a jumper
       rotate(-90)
       translate([4-ajuste,4,-3.5])
       barrasTech(tol=0.1,tamaNio=2);
        
         //base a esculpir sin cambios
       translate([- ajusteAnchoTech,ajuste,0])
      cube([anchoTech,dimNoMod+ajusteEje,Hled]); 
   
     //base a esculpir, defasada en +Z
             translate([- ajusteAnchoTech,dimNoMod+ajuste+ajusteEje,alturaTuerca])
             cube([anchoTech,lonPoste-8.5-ajusteEje,Hled]); 
        
        
        
        translate([-3,62.4,0.9+alturaTuerca])
        rotate([90,0,0])
        barrasTech(tol=0.1,tamaNio=1);
        
        translate([11,62.4,0.9+alturaTuerca])
        rotate([90,0,0])
        barrasTech(tol=0.1,tamaNio=1);
              
   //conector technic bajo
  
       translate([4,58.4,0.4])
      rotate(-90)
     barrasTech(tol=0.1,tamaNio=2);    
    
    
    }
    
    //apoyo a Eje
    translate([0,ajuste,0])
    cylinder(d=diamShatfusr+0.1,h=Hled*4,center=true);
      
   
     //receptAculos
      translate([0,(diamShatfusr/2)+(espIntCanalusr*1.4)+ajuste, alturaTuerca-0.1])
        for(i=[0:canales+1]){   
            translate([0,(led+espIntLed)*i,0])
            cylinder(d=led+tol,h=Hled-led/2);
        }
        
        //apoyos esfericos
        translate([0,(diamShatfusr/2)+(espIntCanalusr*1.4)+ajuste,Hled-(led/2)+alturaTuerca])
        for(i=[0:canales+1]){   
            translate([0,(led+espIntLed)*i,0])
            sphere(d=led+tol);
        }
        
        //filtros
        translate([0,(diamShatfusr/2)+(espIntCanalusr*1.4)+ajuste,alturaTuerca])
        for(i=[0:canales+1]){   
            translate([0,(led+espIntLed)*i,0])
             cylinder(d=2,h=Hled*4,center=true);
        }
        
  }
  
 }
 
 module Frame(){

ajusteDistRecept=1;
distanciaReceptores=24.5+ajusteDistRecept;


//MONTURA ENCODER INCREMENTAL
    translate([0,-distanciaReceptores,0])
    rotate([-90,0,0])
difference(){
    monRecLed6ch();

translate([0,-(8+30),0])
cube([16,16,16],center=true);
}

//MONTURA ENCODER ABSOLUTO
    translate([0,distanciaReceptores,0])
    rotate([-90,0,180])
difference(){
    monRecLed6ch();
     
translate([0,-(8+42.5),0])
cube([16,16,16],center=true);
}
     

      translate([25,-distanciaReceptores,0])
    rotate([-90,0,0])
   monRecLed1ch(ajusteHled=2,altura=56.5-6.2);

 
      translate([25,-11.75+ajusteDistRecept,0])
    rotate([-90,0,180])
   monRecLed1ch(ajusteHled=2,altura=56.5-6.2);


    translate([4*2-1,-4*3-0.8,0])
    pinRedondo();

    translate([4*2-1,4*3+0.8,0])
    pinRedondo();

    translate([-4*2+1,4*3+0.8,0])
    pinRedondo();

    translate([-4*2+1,-4*3-0.8,0])
    pinRedondo();

translate([0,-25.5,-2.75+0.1])
cube([29.25,14.5,2.75]);

   difference(){

        translate([0,0,-2.75+0.1])
        linear_extrude(height=2.75)
        circle(d=59,$fn=6);
       
       translate([20,0,0])
       cylinder(d=5,h=40,center=true);
       
        translate([-20,0,0])
       cylinder(d=5,h=40,center=true);
       
   }

}

//####VARIABLES####
  
   
  $fn=100;
  
  //nuevo tamaNio de led
    led=3.06;
  Hled=4.5;
  //RSled=1.7;
  espIntCanalusr=2;
  espIntLed=1.1;
  //offsetIni=2.5+1;
  espSensor=led*2;
  espesor=1.5;
  canales=4;
  diamShatfusr=5;
  
  tol=0;
  
  lonPoste=diamShatfusr+(diamShatfusr*1.5)+(espSensor*canales)+(espesor*2);
anchoPoste=diamShatfusr+(espesor*2);

//####RENDERIZADOS####

encoders=0;


//color("black")
//translate([0,4,67])
//rotate([0,90,90])
//jumperDouble();


//color("black")
//translate([0,8.1,12])
//rotate([0,90,90])
//!jumperDoubleVar(lon=16.4);
//!jumperDoubleVar(lon=6);
//jumperDoubleVar(lon=15.5);

color("cyan")
translate([0,4*2,0])
rotate([-90,0,0])
//monturaLed6ch();
import("../STL/Encoder/monturaLeds.stl");



color("cyan")
translate([0,-4*2,0])
rotate([-90,0,180])
// monturaLed6ch();
import("../STL/Encoder/monturaLeds.stl");

color("gray")
 //Frame();
 import("../STL/Encoder/Frame.stl");
 
 /*
 linear_extrude(height=0.6)
 difference(){
     circle(d= diamShatfusr+3);
 circle(d= diamShatfusr);
 }
 */
 
 //rotate([90,0,0])
 //cylinder(d=30,h=40);
 
if(encoders==1){

  //ENCODER ABSOLUTO
  color("black")
  translate([0,19.5,47])
  rotate([90,0,0])
  linear_extrude(height=espesor*2)
  encoderAbsoluto(espIntCanalusr=espIntCanalusr,espSensor=espSensor,canales=canales,diamShatfusr=diamShatfusr);
  
  //ENCODER 3 CANALES
  
  color("black")
  translate([0,-16.5,47])
  rotate([90,5,0])
   linear_extrude(height=espesor*2)
  difference(){
       encoder3canales(tamaNioCanal=espSensor/2,diam=57.75,espIntCanal=espIntCanalusr,angulo=10,tamaNioZ=1.5);
      circle(d=diamShatfusr);
  }
  
  }

//ENCODER Incremental de 10 grados
/*
 difference(){
       encoder3canales(tamaNioCanal=8,diam=60,espIntCanal=1,angulo=5,tamaNioZ=0,sDoCH=0);
      circle(d=6);
  }
  */
  
 
  
  


