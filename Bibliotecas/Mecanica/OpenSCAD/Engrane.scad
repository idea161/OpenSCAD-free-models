
module dienteEngrane20(){

tol=0.1;
ancho45A=1.24-(2*tol);
alto45A=1.56;
ancho45B=1.55-(2*tol);
alto45B=1.58;
sepTriangulosH=2.57;
sepTriangulosV=2.5;
   
mediaAlturaTech=4;

mediaCentro=mediaAlturaTech-sepTriangulosH;


//union(){
  //  polyhedron(points=[[ancho45A/2,mediaAlturaTech,0],[-ancho45A/2,mediaAlturaTech,0],[ancho45A/2,-mediaAlturaTech,0],[-ancho45A/2,-mediaAlturaTech,0],[ancho45B/2,mediaCentro,sepTriangulosV],[-ancho45B/2,mediaCentro,sepTriangulosV],[ancho45B/2,-mediaCentro,sepTriangulosV],[-ancho45B/2,-mediaCentro,sepTriangulosV]]
    //,faces=[  [0,3,2],[0,4,5],[0,4,6],[0,6,2],[3,2,6],[3,6,7],[6,7,4],[7,4,5],[5,0,1],[1,5,7],[1,7,3],[0,1,3]]);
    
    translate([0,0,1.5])
    cube([ancho45B,2*mediaCentro,3],center=true);

hull(){

translate([0,sepTriangulosH+mediaCentro,0])
    hull(){
        rotate([90,0,0])
        linear_extrude(height=0.01)
        //circle(r=3,$fn=3);
        resize([ancho45A,alto45A])
        polygon(points=[[-1,0],[1,0],[0,1]]);

        translate([0,-sepTriangulosH,sepTriangulosV-0.01])
        
         linear_extrude(height=0.01)
        resize([ancho45B,alto45B])
         polygon(points=[[-1,0],[1,0],[0,1]]);
        //circle(r=4,$fn=);
    
    }
    
    rotate(180)
    translate([0,sepTriangulosH+mediaCentro,0])
    hull(){
        rotate([90,0,0])
        linear_extrude(height=0.01)
        //circle(r=3,$fn=3);
        resize([ancho45A,alto45A])
        polygon(points=[[-1,0],[1,0],[0,1]]);

        translate([0,-sepTriangulosH,sepTriangulosV-0.01])
        
         linear_extrude(height=0.01)
        resize([ancho45B,alto45B])
         polygon(points=[[-1,0],[1,0],[0,1]]);
        //circle(r=4,$fn=);
    
    }
    
}
    
       

}






 dienteEngrane20();