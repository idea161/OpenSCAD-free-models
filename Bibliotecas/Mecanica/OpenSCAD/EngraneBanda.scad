


module EngraneBanda(alturaEngrane=5.9,espesor=1,flecha=4,tol=1,diamDiente=1.3,diamEngrane=9,dientes=14){


angulo=360/dientes;


difference(){
    union(){
cylinder(d=diamEngrane-tol,h=alturaEngrane);
    translate([0,0,-espesor])
        //tapa 1
cylinder(d=diamEngrane+diamDiente+(espesor*2),h=espesor);

 //tapa 2
translate([0,0,alturaEngrane])
cylinder(d=diamEngrane+diamDiente+(espesor*2),h=espesor);
}
cylinder(d=flecha+tol,h=alturaEngrane*4,center=true);
}

for(i=[0:dientes-1])
    rotate(angulo*i)
    translate([diamEngrane/2,0,0])
cylinder(d=diamDiente-(tol/4),h=alturaEngrane);

for(i=[0:dientes-1])
    rotate(angulo*i)
    translate([(diamEngrane/2)-0.75,-(diamDiente-(tol/4))/2,0])
//cylinder(d=diamDiente-(tol/4),h=alturaEngrane);
cube([diamDiente/2,diamDiente-(tol/4),alturaEngrane])

echo("alturaEng");
echo(alturaEngrane+(2*espesor));

echo("espesorEng");
echo(espesor);
}


//####RENDERIZADOS####

//$fn=50;

//TOL 1 funciono para estarflojo
//TOL 0.75 funciono para estar apretado
//EngraneBanda(alturaEngrane=6.5,espesor=1,flecha=4,tol=0.75,diamDiente=1.3,diamEngrane=9,dientes=14);
