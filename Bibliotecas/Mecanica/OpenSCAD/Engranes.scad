use <MCAD/involute_gears.scad>

module RedMotShaft(){
 
    difference(){
    circle(d=5.4);
        
    translate([0,3+(3.7/2)])
    square([6,6],center=true);   
       
     mirror([0,1,0]){
     translate([0,3+(3.7/2)])
    square([6,6],center=true);   
     }
   }
   
   }
   
 
 
   module motorPololu(){
espesor=2;
distPijas=20;
diamPijas=2.5;
	   tol=1;
	   alturaMotor=10;
	   baseMotor=12;
	   claroApoyo=22;

difference(){
	
//translate([0,0,(espesor/2)+(alturaMotor/2)])
//cube([claroApoyo+(2*espesor),12,espesor+alturaMotor],center=true);
	
minkowski(){
	//cube([claroApoyo+(2*espesor),12,espesor+alturaMotor],center=true);
		cube([claroApoyo+(2*espesor)-(2*2),12-(2*2),0.1],center=true);
		cylinder(r=2,h=espesor+alturaMotor);
		}

	translate([distPijas/2,0,0])
cylinder(d=diamPijas+tol,h=alturaMotor*4,center=true);	   
		translate([-distPijas/2,0,0])
cylinder(d=diamPijas+tol,h=alturaMotor*4,center=true);	   
	
	//recortador motor
	rotate([90,0,0])
	minkowski(){
	cube([baseMotor-4,((10+tol*0.5)*2)-4,40],center=true);
		cylinder(d=4,h=20);
	}
	
	recortador=40;
	prueba=5.5;
	
	//recortador 1
	translate([(claroApoyo/2)+(recortador/2)+espesor-prueba,0,recortador/2+espesor])
	cube([recortador,recortador,recortador],center=true);
	
	//recortador2
	translate([-(claroApoyo/2)-(recortador/2)-espesor+prueba,0,recortador/2+espesor])
	cube([recortador,recortador,recortador],center=true);
}

}//fin motor pololu
   
   module pololuShaft(){
	difference(){
		circle(r=(3-0.1)/2);
		translate([0,-((3-0.1)/2+(1/2))+0.6+0.1])
		square([3,1],center=true);    
	   }
   }
   
   module Poste(altura=10,espesor=3,tol=1,diamShaft=2){
	   
	   difference(){
	      cylinder(d=(espesor*2)+diamShaft+tol,h=altura);
	   
		  cylinder(d=diamShaft+tol,h=altura*4,center=true);
	      
	   }
   }
   
   //####RENDERIZADOS#####
   
   $fn=100;
   
diamShaft=7;

//shaftOffset=13;

shaftOffset=13+1.5;

//ENGRANE MAYOR
   translate([-shaftOffset,0,3.65])
union(){
	
	difference(){
	Poste(altura=15,espesor=2,tol=1,diamShaft=diamShaft);
	
		translate([0,0,7.25])
		rotate([90,0,0])
		cylinder(d=3+1,h=20,center=true);
		}   
   
		difference(){
  gear(circular_pitch = 200,
		number_of_teeth = 130,
		gear_thickness = 3,
		rim_thickness = 3,
		hub_thickness = 3,
		bore_diameter = 10);
			
			for(i=[0:7]){
				rotate(i*(360/8))
				translate([15,0,-5])
				minkowski(){
				cube([50,0.1,10]);
					sphere(d=3);
					
				}	
			}
			
			
			rotate((360/16))
			for(i=[0:7]){
				rotate(i*(360/8))
				translate([25,0,-5])
				minkowski(){
				cube([40,0.1,10]);
					sphere(d=3);
					
				}	
			}
			
			
			
		}
}

//diametro=6;

/*
gear_thickness-> espesor de la montura
rim_thickness->espesor del engrane
bore_diameter-> diametro del eje
bore (perforación)
*/

/*
gear(circular_pitch = 200,
	number_of_teeth = 27,
		gear_thickness = 10,
		rim_thickness = 5,
		hub_thickness = 8,
		bore_diameter = 3);
     */   
        

		
		//ENGRANE PEQUEÑO
       
	   //Distancia entre centros
	   distEngCent=78;
	   alturaEngPeq=14;
            translate([distEngCent-shaftOffset,0,0])
			rotate(360/20)
	 ! difference(){
        gear(circular_pitch = 200,
		number_of_teeth = 10,
		gear_thickness = alturaEngPeq,
		rim_thickness = alturaEngPeq,
		hub_thickness = alturaEngPeq,
		bore_diameter = 0);
            
            translate([0,0,-alturaEngPeq])
            linear_extrude(height=alturaEngPeq*2)		
           //RedMotShaft();
		  pololuShaft();
		}
		
		
		//####MONTURA MOTORES ####
		
		diamMontura=70;
		largo=60;
		espesor=3;
      diamMotorAC=50;
		distanciaPijas=57.5;
		alturaPoste=5;
		tol=1;
		
		placaPololu=30;
		
difference(){		
	//Placa base para modelar montura
	   union(){
		   //cylindro motura motor AC
			translate([0,0,-espesor/2])
		   cylinder(d=diamMontura,h=espesor,center=true);
			
	
		   //barra de unión cilindro montura motorAC y placa motor pololu   
		   	   largoBarra=distEngCent-(diamMontura/2)+espesor-shaftOffset;
		   translate([(diamMontura/2)+(largoBarra/2)-(placaPololu/4),0,-espesor/2])
		   cube([largoBarra,placaPololu,espesor],center=true);
		   
						   translate([diamMontura/2-(placaPololu/4),0,0])
						rotate([90,0,0])
						translate([largoBarra,0,-espesor/2])
						rotate(180)
						linear_extrude(height=espesor)
						polygon([[0,alturaMontura],[largoBarra,0],[0,0]]);
						
		   
				//poste pija motor AC1
				translate([0,distanciaPijas/2,-alturaPoste])
				Poste(altura=alturaPoste,espesor=1.5,tol=1,diamShaft=1);
			//poste pija motor AC2
				translate([0,-distanciaPijas/2,-alturaPoste])
				Poste(altura=alturaPoste,espesor=1.5,tol=1,diamShaft=1);		
				
				//monturaPolou
				alturaMontura=23;
				translate([-espesor/2+distEngCent-4.5-shaftOffset,0,-alturaMontura/2])
					minkowski(){
					cube([0.1,placaPololu-4,alturaMontura-4],center=true);
						rotate([0,90,0])
						cylinder(d=4,h=espesor,center=true);	
						}
						
						//poste  pija motor Pololu 1
							translate([distEngCent-alturaPoste+0.1-shaftOffset,distPijas/2,-17])
				rotate([0,-90,0])	
						Poste(altura=alturaPoste*0.75,espesor=1.5,tol=1,diamShaft=1);	
						
								//poste  pija motor Pololu 2
							translate([distEngCent-alturaPoste+0.1-shaftOffset,-distPijas/2,-17])
				rotate([0,-90,0])	
						Poste(altura=alturaPoste*0.75,espesor=1.5,tol=1,diamShaft=1);	
								
					
				}
				
//corte motor AC
   cylinder(d=diamMotorAC+tol,h=espesor*4,center=true);

//corte pija 1 motor AC
translate([0,-distanciaPijas/2,0])
				cylinder(d=2,h=alturaPoste*4,center=true);

//corte pija 2 motor AC				
				translate([0,distanciaPijas/2,0])
				cylinder(d=2,h=alturaPoste*4,center=true);
		

//corte pijas motor pololu
distPijas=20;
diamPijas=2.5;
				
				//corte pija 1
				translate([0,distPijas/2,-17])
				rotate([0,90,0])
				cylinder(d=diamPijas,h=300,center=true);
				
				//corte pija 2
					translate([0,-distPijas/2,-17])
				rotate([0,90,0])
				cylinder(d=diamPijas,h=300,center=true);

//corte elasticidad PLA
rotate([0,-90,0])
rotate(45)
cylinder($fn=4,d=20,h=100);

//corte tornillos apoyo
diamTor=4.5;

				translate([diamMontura/2,-8,0])
				for(i=[0:1]){
					for(j=[0:1]){
						translate([i*16,j*16,0])
				           cylinder(d=diamTor,h=50,center=true);
					}
				}


		}
		
		
		translate([ distEngCent-(9/2)-shaftOffset,0,-17])
    rotate([90,0,90])
		  motorPololu();