
ladoLargo=215;
ladoCorto=80;
espesor=1.5;
espesorMetal=1.2;
tol=1;
alturaMetal=10;
alturaCase=120;
case=0;

module clipMetal(espesor=1.5,tol=1,espesorMetal=1.2,alturaMetal=10){
espesorMetal=1.2;
alturaMetal=10;
    translate([0,espesor+espesorMetal+tol,0])
    rotate(180)
    union(){
        translate([-alturaMetal/2,0,0])
        cube([alturaMetal,espesor,alturaMetal+espesor]);
        
           translate([-alturaMetal/2,espesor,alturaMetal])
        cube([alturaMetal,espesorMetal*2+tol,espesor]);
    }   
    
}


if(case==1){
//CLIP X: espesor+espesorMetal+tol
//CLIP Y: -alturaMetal-espesor

    //Clip A Lado Largo 1
     translate([-(ladoLargo/2)+(alturaMetal/2),0])
    clipMetal(espesor=espesor,tol=tol,espesorMetal=espesorMetal,alturaMetal=alturaMetal);

    //Clip B Lado Largo 1
     translate([(ladoLargo/2)-(alturaMetal/2),0])
    clipMetal(espesor=espesor,tol=tol,espesorMetal=espesorMetal,alturaMetal=alturaMetal);

    //Clip A Lado Largo 2
      translate([-ladoLargo/2,0])
        rotate(180-60)
        translate([ladoCorto,0])
        rotate(-60+180)
      translate([-(alturaMetal/2),0])
    clipMetal(espesor=espesor,tol=tol,espesorMetal=espesorMetal,alturaMetal=alturaMetal);

    //Clip B Lado Largo 2
      translate([-ladoLargo/2,0])
        rotate(180-60)
        translate([ladoCorto,0])
        rotate(-60+180)
       translate([(-ladoLargo)+(alturaMetal/2),0])
    clipMetal(espesor=espesor,tol=tol,espesorMetal=espesorMetal,alturaMetal=alturaMetal);

    //Clip A lado Largo 3
      translate([-ladoLargo/2,0])
        rotate(180-60)
        translate([ladoCorto,0])
        rotate(-60)
        translate([ladoLargo,0])
        rotate(-60)
        translate([ladoCorto,0])
        rotate(-60+180)
     translate([-(ladoLargo)+(alturaMetal/2),0])
    clipMetal(espesor=espesor,tol=tol,espesorMetal=espesorMetal,alturaMetal=alturaMetal);

    //Clip B lado Largo 3
     translate([-ladoLargo/2,0])
        rotate(180-60)
        translate([ladoCorto,0])
        rotate(-60)
        translate([ladoLargo,0])
        rotate(-60)
        translate([ladoCorto,0])
        rotate(-60+180)
     translate([-(alturaMetal/2),0])
    clipMetal(espesor=espesor,tol=tol,espesorMetal=espesorMetal,alturaMetal=alturaMetal);

    difference(){

        linear_extrude(height=alturaCase)
        union(){
            //lado largo 1
            translate([-ladoLargo/2,-espesor])
            square([ladoLargo,espesor]);

            //union 1
            translate([-(ladoLargo/2),0])
            rotate(180-30)
            translate([-(espesor/2),0])
            square([espesor,espesor*0.9]);

            //lado corto 1
            translate([-ladoLargo/2,0])
            rotate(180-60)
            square([ladoCorto,espesor]);

            //lado largo 2
            translate([-ladoLargo/2,0])
            rotate(180-60)
            translate([ladoCorto,0])
            rotate(-60)
            square([ladoLargo,espesor]);

            //union2

            translate([-(ladoLargo/2),0])
            rotate(180-60)
            translate([ladoCorto,0])
            rotate(-30)
            translate([-(espesor/2),0])
            square([espesor,espesor*0.9]);

            //lado corto2
            translate([-ladoLargo/2,0])
            rotate(180-60)
            translate([ladoCorto,0])
            rotate(-60)
            translate([ladoLargo,0])
            rotate(-60)
            square([ladoCorto,espesor]);

            //union3
            translate([-ladoLargo/2,0])
            rotate(180-60)
            translate([ladoCorto,0])
            rotate(-60)
            translate([ladoLargo,0])
            rotate(-30)
            translate([-(espesor/2),0])
            square([espesor,espesor*0.9]);


            //lado largo3
            translate([-ladoLargo/2,0])
            rotate(180-60)
            translate([ladoCorto,0])
            rotate(-60)
            translate([ladoLargo,0])
            rotate(-60)
            translate([ladoCorto,0])
            rotate(-60)
            square([ladoLargo,espesor]);

            //union 4
            translate([-ladoLargo/2,0])
            rotate(180-60)
            translate([ladoCorto,0])
            rotate(-60)
            translate([ladoLargo,0])
            rotate(-60)
            translate([ladoCorto,0])
            rotate(-30)
            translate([-(espesor/2),0])
            square([espesor,espesor*0.9]);

            //lado corto3
            translate([-ladoLargo/2,0])
            rotate(180-60)
            translate([ladoCorto,0])
            rotate(-60)
            translate([ladoLargo,0])
            rotate(-60)
            translate([ladoCorto,0])
            rotate(-60)
            translate([ladoLargo,0])
            rotate(-60)
            square([ladoCorto,espesor]);

            //union 5
            translate([-ladoLargo/2,0])
            rotate(180-60)
            translate([ladoCorto,0])
            rotate(-60)
            translate([ladoLargo,0])
            rotate(-60)
            translate([ladoCorto,0])
            rotate(-60)
            translate([ladoLargo,0])
            rotate(-30)
            translate([-(espesor/2),0])
            square([espesor,espesor*0.9]);

            //union 6
            translate([-ladoLargo/2,0])
            rotate(180-60)
            translate([ladoCorto,0])
            rotate(-60)
            translate([ladoLargo,0])
            rotate(-60)
            translate([ladoCorto,0])
            rotate(-60)
            translate([ladoLargo,0])
            rotate(-60)
            translate([ladoCorto,0])
            rotate(-30)
            translate([-(espesor/2),0])
            square([espesor,espesor*0.9]);
        }
        
        esferaMinkow=42;
        factorVentX=0.5;
        factorVentY=0.1;
        
        //ventana 1 "frente"
        translate([0,0,alturaCase/2])
        minkowski(){
            cube([ladoLargo*  factorVentX,ladoLargo,alturaCase*factorVentY],center=true);
            sphere(r=esferaMinkow);
        }
        
        //ventana2
            translate([-ladoLargo/2,0])
            rotate(180-60)
            translate([ladoCorto,0])
            rotate(-60)
            translate([ladoLargo,0])
            rotate(-60)
            translate([ladoCorto,0])
            rotate(-60)
             translate([+ladoLargo/2,0,alturaCase/2])
       minkowski(){
            cube([ladoLargo*factorVentX,ladoLargo,alturaCase*factorVentY],center=true);
            sphere(r=esferaMinkow);
        }
        
         //ventana 3
       translate([-ladoLargo/2,0])
            rotate(180-60)
            translate([ladoCorto,0])
            rotate(-60)
                translate([+ladoLargo/2,0,alturaCase/2])
       minkowski(){
            cube([ladoLargo*factorVentX,ladoLargo,alturaCase*factorVentY],center=true);
            sphere(r=esferaMinkow);
        }

        
    }
    
}//fin if



distTorMay=215;

distTorMen=90;


distTrayecto=150;
diamTornillo=6;

estructura=0;

if(estructura==1){
    difference(){
        translate([0,0,espesor/2])
       cube([distTorMay+(diamTornillo*2),diamTornillo*2,espesor],center=true);
        
        translate([distTorMay/2,0,0])
        cylinder(d=diamTornillo,h=10,center=true);


        translate([-distTorMay/2,0,0])
        cylinder(d=diamTornillo,h=10,center=true);
    }


    translate([0,distTrayecto,espesor/2])
    difference(){
        
       cube([distTorMen+(diamTornillo*2),diamTornillo*2,espesor],center=true);
        
        translate([distTorMen/2,0,0])
        cylinder(d=diamTornillo,h=10,center=true);

        translate([-distTorMen/2,0,0])
        cylinder(d=diamTornillo,h=10,center=true);
    }

    translate([0,(distTrayecto/2),espesor/2])
    cube([(diamTornillo*2),distTrayecto+(diamTornillo*2),espesor],center=true);
}


$fn=100;

cylinder(d=31,h=30);

translate([(57-31)/2,0,30])
cylinder(d=57,h=65);

