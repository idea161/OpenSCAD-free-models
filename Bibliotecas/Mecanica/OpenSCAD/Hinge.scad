     
module Hinge(diamEje=2,anchoBisagra=30,utol=0.1,tol=2+0.1,espesor=1.5,facSepara=1.6,espacioPlano=10,angulo=30,separador=1,clip=1,viaje=20){

altDiag=espesor;

//eje de apoyo
    rotate([0,90,0])
cylinder(d=diamEje,h=anchoBisagra,center=true);
    
    
    //establizadores (balero) de eje
    for(i=[0:2]){
       rotate([120*i,0,0])
      translate([0,((diamEje/2)+tol),0])
      sphere(r=tol-utol);
    }

//parte ancha eje (Receptora)
rotate([0,90,0])
    difference(){
cylinder(d=diamEje+(2*espesor)+(2*tol),h=anchoBisagra/3-(2*altDiag),center=true);
    
        //hueco
        cylinder(d=diamEje+(2*tol),h=anchoBisagra/3-(2*tol),center=true);
        }
   
   //CORRECCION TIPO CONOS PARA PROBLEMA DE PUENTE 
        
        //CONO A BASE A 
    translate([(anchoBisagra/6)+(altDiag)-(espesor/2),0,0])
    rotate([0,-90,0])
    difference(){
       cylinder(d1=diamEje+(2*espesor)+(2*tol),d2=diamEje+tol+(espesor/2),h=altDiag);
        
       cylinder(d=diamEje+(tol*2)+(espesor/2),h=altDiag*2); 
    }
    
    
    //CONO A RECEPTORA A
      translate([-(anchoBisagra/3-(2*altDiag))/2,0,0])
    rotate([0,-90,0])
      difference(){
       cylinder(d1=diamEje+(2*espesor)+(2*tol),d2=diamEje+tol+(espesor/2),h=altDiag);
        
       cylinder(d=diamEje+(tol*2)+(espesor/2),h=altDiag*2); 
    }
    
    //CONO A RECEPTORA B
     mirror([1,0,0]){
      translate([-(anchoBisagra/3-(2*altDiag))/2,0,0])
    rotate([0,-90,0])
      difference(){
       cylinder(d1=diamEje+(2*espesor)+(2*tol),d2=diamEje+tol+(espesor/2),h=altDiag);
        
       cylinder(d=diamEje+(tol*2)+(espesor/2),h=altDiag*2); 
    }
}
  
  //CONO A BASE B
    mirror([1,0,0]){
     translate([(anchoBisagra/6)+(altDiag)-(espesor/2),0,0])
    rotate([0,-90,0])
    difference(){
       cylinder(d1=diamEje+(2*espesor)+(2*tol),d2=diamEje+tol+(espesor/2),h=altDiag);
        
       cylinder(d=diamEje+(tol*2)+(espesor/2),h=altDiag*2); 
    }   
    }
    

//FIN CORRECCION CONOS

//parte plana comienzo de "T"
rotate(180)
translate([-(anchoBisagra/3-(2*tol))/2,0,(diamEje/2)+tol])
cube([anchoBisagra/3-(2*tol),diamEje*facSepara,espesor]);

//parte larga de "T"
rotate(180)
translate([-anchoBisagra/2,diamEje*(facSepara-1),(diamEje/2)+tol])
cube([anchoBisagra,diamEje,espesor]);

//espacio plano de "T"
rotate(180)
translate([-anchoBisagra/2,diamEje*(facSepara-1)+(diamEje),(diamEje/2)+tol])
//difference(){
   cube([anchoBisagra,espacioPlano,espesor+separador]);
   /* translate([espacioPlano,10,0])
    rotate(60)
    minkowski(){
        cube([espacioPlano/2,espacioPlano*2,espacioPlano/2]);
        sphere(r=3);
        }
}*/
rotate([angulo,0,0])

union(){
    
    //parte plana central union
translate([-anchoBisagra/6,diamEje*(facSepara-1),(diamEje/2)+tol])
cube([anchoBisagra/3,diamEje,espesor]);
    
    //parte plana anclada a abrazadera original
translate([anchoBisagra/6,0,(diamEje/2)+tol])
cube([anchoBisagra/3,diamEje*facSepara,espesor]);
 
//espacio plano de "corchete"
    //primeros 3 casos en donde si se necesita una separaciOn
    if(clip==0||clip==1||clip==2){
translate([-anchoBisagra/2,diamEje*(facSepara-1)+(diamEje),(diamEje/2)+tol])
cube([anchoBisagra,espacioPlano,espesor+separador]);
    }
    
      if(clip==3){
translate([-anchoBisagra/2,diamEje*(facSepara-1)+(diamEje),(diamEje/2)+tol])
cube([anchoBisagra,viaje-(diamEje*(facSepara-1)+(diamEje))+(2*espesor),espesor]);
    }
    
    
    
echo("Y1,Y2,DT");
    //minimo de bisagra par afuncionar
echo((diamEje*(facSepara-1)+(diamEje))*2)
echo("Y2");
    //bisagra con espacios planos
echo((diamEje*(facSepara-1)+(diamEje))*2+(2*espacioPlano));
    //diametro de bisagra
    echo(diamEje+(tol*2)+(espesor*2));

//copia parte plana anclada a eje y abrazadera
mirror([1,0,0]){
  translate([anchoBisagra/6,0,(diamEje/2)+tol])
  cube([anchoBisagra/3,diamEje*facSepara,espesor]);
    
    //abrazadera a eje
    translate([(anchoBisagra/6)+altDiag-(espesor/2),0,0])
    rotate([0,90,0])
    difference(){
    cylinder(d=diamEje+(2*espesor)+(2*tol),h=(anchoBisagra/3)-altDiag+(espesor/2));

    //cylinder(d=diamEje+(2*tol),h=anchoBisagra/3,center=true);
        
        sphere(d=diamEje+(2*tol));

    }
    
}

//abrazadera a eje
translate([(anchoBisagra/6)+altDiag-(espesor/2),0,0])
rotate([0,90,0])
difference(){
cylinder(d=diamEje+(2*espesor)+(2*tol),h=(anchoBisagra/3)-altDiag+(espesor/2));

//cylinder(d=diamEje+(2*tol),h=anchoBisagra/3,center=true);
     sphere(d=diamEje+(2*tol));

}



}//fin union

//#####CLIP#####
//espesor=1.5;
//espacioPlano=10;
//claroBisagra=17.5;
anchoClip=anchoBisagra;
claroBisagra=(diamEje*(facSepara-1)+(diamEje))*2;
baseClip=(espacioPlano*2)+claroBisagra;
//viaje=20;
//angulo=-0;
if(clip==1){
    //contra clip
translate([-anchoClip/2,-(baseClip/2)+(espacioPlano*1.25),viaje])
rotate([0,90,0])
linear_extrude(height=anchoClip)
polygon([[0,espacioPlano/2],[-espesor,0],[0,0]]);

translate([0,-(baseClip/2)+(espacioPlano),viaje+espesor/2])
cube([anchoClip,espacioPlano/2,espesor],center=true);

rotate([angulo,0,0])
union(){
    
    //triangulo clip
translate([-anchoClip/2,-baseClip/2+(espacioPlano/2),viaje+espesor])
rotate([0,90,0])
linear_extrude(height=anchoClip)
polygon([[0,-espacioPlano/2],[espesor,0],[0,0]]);

//base clip
translate([0,0,viaje+(espesor/2)+espesor])
cube([anchoClip,baseClip,espesor],center=true);
    
    factorApoyo=2;
    //apoyo clip
    translate([0,baseClip/2-(espacioPlano*factorApoyo),viaje-(espesor/2)+espesor])
  cube([anchoClip,espacioPlano*2*factorApoyo,espesor],center=true);  
}
}//fin if clip

if(clip==2){
    
    //base clip
translate([0,0,viaje+(espesor/2)+espesor])
cube([anchoClip,baseClip,espesor],center=true);
    
    factorApoyo=2;
    //apoyo clip
    translate([0,-baseClip/2+(espacioPlano*factorApoyo),viaje-(espesor/2)+espesor])
  cube([anchoClip,espacioPlano*2*factorApoyo,espesor],center=true);  
    
    
       //triangulo clip
translate([-anchoClip/2,baseClip/2-(espacioPlano/2),viaje+espesor])
rotate([0,90,0])
linear_extrude(height=anchoClip)
polygon([[0,espacioPlano/2],[espesor,0],[0,0]]);


    
    //contra clip
rotate([angulo,0,0])
union(){
translate([-anchoClip/2,(baseClip/2)-(espacioPlano*1.25),viaje])
rotate([0,90,0])
linear_extrude(height=anchoClip)
polygon([[0,-espacioPlano/2],[-espesor,0],[0,0]]);

translate([0,(baseClip/2)-(espacioPlano),viaje+espesor/2])
cube([anchoClip,espacioPlano/2,espesor],center=true);
}

}//fin if clip

if(clip==3){
    //contra clip
translate([-anchoClip/2,-(baseClip/2)+(espacioPlano*1.25),viaje])
rotate([0,90,0])
linear_extrude(height=anchoClip)
polygon([[0,espacioPlano/2],[-espesor,0],[0,0]]);

translate([0,-(baseClip/2)+(espacioPlano),viaje+espesor/2])
cube([anchoClip,espacioPlano/2,espesor],center=true);

rotate([angulo-90,0,0])


union(){
   
    
    //triangulo clip
translate([-anchoClip/2,-baseClip/2+(espacioPlano/2)+(espesor/4),viaje+espesor])
rotate([0,90,0])
linear_extrude(height=anchoClip)
polygon([[0,-espacioPlano/2],[espesor,0],[0,0]]);

//base clip
translate([0,-baseClip/3+(espesor/6),viaje+(espesor/2)+espesor])
cube([anchoClip,(baseClip/3)-(espesor/6),espesor],center=true);
    
    factorApoyo=2;
    //apoyo clip
    //translate([0,baseClip/2-(espacioPlano*factorApoyo),viaje-(espesor/2)+espesor])
  //cube([anchoClip,espacioPlano*2*factorApoyo,espesor],center=true);  
}

}//fin if clip


}//fin hinge

//#####RENDERIZADOS#####

//se recomnienda no tener renderizados

$fn=10;





//Angulos negativos para clip modo 1 y 2
//Angulos positivos para clip 3
//Hinge(diamEje=3.5,anchoBisagra=60,utol=0.1,tol=0.6+0.1,espesor=1.5,facSepara=2.5,espacioPlano=2,angulo=30,separador=3,clip=3,viaje=30);


//BISAGRA PEDAL BOTE BASURA

y1=17.5;
y2=57.5;
DT=10.9;
dTor=4.7;
tol=1;
espesor=1.5*2;
radMink=10;

$fn=100;

//translate([0,-DT/2-25,-(DT/2)-(espesor/2)])

/*minkowski(){
    

  cube([85-(2*radMink),35-(2*radMink),1.5],center=true);
  cylinder(r=radMink,h=espesor);
}*/



/*difference(){
    rotate([90])
    Hinge(diamEje=3.5,anchoBisagra=85,utol=0.1,tol=0.6+0.1,espesor=1.5*2,facSepara=2.5*1.5,espacioPlano=20,angulo=90,separador=0,clip=0,viaje=30);
    translate([32,0,-20])
    rotate([90,0,0])
    cylinder(d=dTor+tol,h=40,center=true);
    translate([-32,0,-20])
    rotate([90,0,0])
    cylinder(d=dTor+tol,h=40,center=true);
}*/



difference(){
 rotate([90,90,45])
Hinge(diamEje=3.5,anchoBisagra=70,utol=0.1,tol=0.6+0.1,espesor=1.5*2,facSepara=2.5*1.5,espacioPlano=71,angulo=-90,separador=0,clip=0,viaje=20);
    
    
    lonX=60;
    lonY=160;
    lonZ=18;
    translate([-33,0,25])
     rotate([0,60,0])
    
    minkowski(){
        translate([0,-lonY/2,-lonZ])
        cube([lonX+10,lonY,lonZ-10]);
        sphere(r=3);
    }
    
    translate([-65,0,0])
     rotate([0,60,0])
     translate([0,-lonY/2,-lonZ])
     cube([lonX+10,lonY,40]);
    
}




