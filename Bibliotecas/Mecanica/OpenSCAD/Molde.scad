use<Track.scad>



module molde(lon=30,base=15,margenes=5,modo=2,tol=1){

diamTornillos=5;

        diamPostes=2;
        radio=5;
        espesor=3;
        ancho=10;
    espesorDesmolde=1.5;

if(modo==1){
    difference(){
        
        
       cube([lon,lon,base],center=true);
        
        translate([0,0,base/2])
        cube([lon*2,lon*2,base],center=true);
        
        
    
        //tubo de entrada de silicon
  translate([0,-10,0])
        rotate([90,0,0])
        cylinder(d=2,h=base*2);
        
        
        //tronillos de union
        
        //tornillo(1,1)
        translate([(lon/2)-margenes,(lon/2)-margenes,0])
        cylinder(d=diamTornillos,h=base*2,center=true);
        
        
        //tornillo(1,-1)
        translate([(lon/2)-margenes,-(lon/2)+margenes,0])
        cylinder(d=diamTornillos,h=base*2,center=true);
        
        
        //tornillo(-1,1)
        translate([-(lon/2)+margenes,(lon/2)-margenes,0])
        cylinder(d=diamTornillos,h=base*2,center=true);
        
        
        //tornillo(-1,-1)
        translate([-(lon/2)+margenes,-(lon/2)+margenes,0])
        cylinder(d=diamTornillos,h=base*2,center=true);
        
                 
                 //anillo para desmoldador
            difference(){
                cylinder(r=radio+espesor,h=ancho+(espesorDesmolde*2),center=true);
                cylinder(r=radio,h=ancho+(espesorDesmolde*2),center=true);
            }
            
            //objeto a moldear
             track(radio=5,tol=0.1,espesor=3,ancho=10,lonL=0,modo=1,dientes=8,alturaDentado=2,alturaDentadoInterno=2,espesorDentado=2.25);
        
            //rieles para desmoldador
            
              //tornillo(1,1)
        rotate(45)
        translate([radio+(espesor/2),0,0])
        cylinder(d=diamPostes+(1*tol),h=base*2,center=true);
        
        
        //tornillo(1,-1)
       rotate(45+90)
        translate([radio+(espesor/2),0,0])
        cylinder(d=diamPostes+(1*tol),h=base*2,center=true);
        
        
        
        //tornillo(-1,1)
     rotate(45+180)
        translate([radio+(espesor/2),0,0])
        cylinder(d=diamPostes+(1*tol),h=base*2,center=true);
        
        
        
        //tornillo(-1,-1)
      rotate(45+270)
        translate([radio+(espesor/2),0,0])
        cylinder(d=diamPostes+(1*tol),h=base*2,center=true);
            
        
        }
    
    }
    
    else if(modo==2){
            //Desmoldador
            
         difference(){
            cylinder(r=radio+espesor-(tol/2),h=espesorDesmolde);
            cylinder(r=radio+(tol/2),h=(espesorDesmolde*4),center=true);
        }
          
        //tornillo(1,1)
        rotate(45)
        translate([radio+(espesor/2),0,0])
        cylinder(d=diamPostes,h=base/2);
        
        
        //tornillo(1,-1)
       rotate(45+90)
        translate([radio+(espesor/2),0,0])
        cylinder(d=diamPostes,h=base/2);
        
        
        
        //tornillo(-1,1)
     rotate(45+180)
        translate([radio+(espesor/2),0,0])
        cylinder(d=diamPostes,h=base/2);
        
        
        
        //tornillo(-1,-1)
      rotate(45+270)
        translate([radio+(espesor/2),0,0])
        cylinder(d=diamPostes,h=base/2);
        
        
    }
    
}

//#####RENDERIZADOS#####

$fn=100;



 molde(lon=30,base=15,margenes=5,modo=1);
    
  
translate([0,0,-4])
rotate([180,0,0])
!molde(lon=30,base=15,margenes=5,modo=2);



