include<./../../Utilidades/Utilities.scad>;

/*

Sección corte de Rodamiento

A la figura característica se le llamara perfilTrapecio

                     LonG          LonG
             _________     _      _________
            |           |    | |   |            | LonD
            |          /    /   \    \           | LonE
 LonA      |         |    |    |    |          | LonF
            |          \    \   /    /           | LonE
            |________|    |_|   |_________| LonD
                LonB      LonC     LonB
                
                
                
//### RODAMIENTO NUEVO MODELO

                         lonG-> diferencia entre parte interna y externa de la figura
            ______     ____
            |       |   |     |  LonD
            |        \   \    /   LonE
    LonA   |        |   |   |    LonF
            |        /   /    \
            |_____|   |____|
                         2*lonB

*/

module perfilTrapecio(lonA=12,lonB=4,lonD=2,lonE=2,lonF=4,lonG=2){
             polygon([[lonB,lonA/2],[0,lonA/2],[0,-lonA/2],[lonB,-lonA/2],[lonB,-lonA/2+lonD],[lonB-lonG,-lonA/2+lonD+lonE],[lonB-lonG,lonA/2-lonD-lonE],[lonB,lonA/2-lonD]]);
}


//lonC

//radioInterno=6;
//radioExterno=14;
//tol=0.1;


//#####BALINES#####

module balinReloj(lonA=14,lonB=3){
    
    lonD=lonA/5;
    lonE=lonA/5;
    lonF=lonA/5;
    lonG=lonB*0.5;
    
rotate_extrude(convexity = 10)
perfilTrapecio(lonA=lonA,lonB=lonB,lonD=lonD,lonE=lonE,lonF=lonF,lonG=lonG);
    
}



module balinTrapecio(lonA=12,lonB=3,lonD=1,lonE=1,lonF=4,lonE=1,lonG=1,radioInterno=4,radioExterno=11,tol=0.1){ 
   lonC=radioExterno-radioInterno-(2*lonB)-(2*tol); 
        difference(){
            cylinder(r=lonB,h=lonA,center=true);
    
              rotate_extrude(convexity = 10)
          translate([-lonB-lonC,0,0])
          perfilTrapecio(lonA=lonA,lonB=lonB,lonD=lonD,lonE=lonE,lonF=lonF,lonG=lonG);
    
          }
      }

//####RODAMIENTOS#####

module rodamiento(radioExterno=12,radioInterno=6,tol=1,balines=8,lonA=12,lonB=2.5,lonD=2,lonE=2,lonF=4,lonE=1,lonG=1){
    
    lonC=radioExterno-radioInterno-(2*lonB)-(2*tol);

//Pieza interna
rotate_extrude(convexity = 10)
translate([radioInterno, 0, 0])
perfilTrapecio(lonA=lonA,lonB=lonB,lonD=lonD,lonE=lonE,lonF=lonF,lonG=lonG);

//Pieza externa
rotate_extrude(convexity = 10)
translate([-radioInterno-(radioExterno-radioInterno),0,0])
perfilTrapecio(lonA=lonA,lonB=lonB,lonD=lonD,lonE=lonE,lonF=lonF,lonG=lonG);

//separadores balines
rotate((360/balines)/2)
for(i=[0:balines-1]){
    angulo=360/balines;
     rotate(i*angulo)
translate([radioInterno,0,0])
    rotate([0,90,0])
    linear_extrude((radioExterno-radioInterno)/2,scale=0.75)
    square([lonF,lonC],center=true);
}

//Banlines
for(i=[0:balines-1]){
    angulo=360/balines;
    rotate(i*angulo)
translate([radioExterno-((radioExterno-radioInterno)/2),0,0])
balinTrapecio(lonA=lonA,lonB=lonB,lonD=lonD,lonE=lonE,lonF=lonF,lonE=lonE,lonG=lonG,radioInterno=radioInterno,radioExterno=radioExterno,tol=tol);
}

}

module rodamientoCone(radioExterno=12,radioInterno=6,tol=1.2,lonA=12,lonB=3,ajusteEnteroBalines=1){
 
     lonD=lonA/5;
    lonE=lonA/5;
    lonF=lonA/5;
    lonG=lonB*0.5;
        
  Pi=3.14159;
radioTrayectoria=(radioExterno-radioInterno)/2+radioInterno;
perimetroTrayectoria=2*Pi*radioTrayectoria;



//PARED EXTERNA
rotate_extrude(convexity = 10)
    translate([radioExterno+(1.5*lonB),0])
perfilTrapecio(lonA=lonA,lonB=-lonB,lonD=lonD,lonE=lonE,lonF=lonF,lonG=lonG);
    
 //PARED Interba   
    rotate_extrude(convexity = 10)
    translate([-(radioInterno-(lonB*0.5)),0])
perfilTrapecio(lonA=lonA,lonB=-lonB,lonD=lonD,lonE=lonE,lonF=lonF,lonG=lonG);
    

/*
EXPERIMENTOS DE CALCULO AUTOMATICO
radioEsfera=2*lonB;

//Calculo numero de balines
alfa=asin(radioEsfera/radioTrayectoria);
//beta=2*alfa
betaRadianes=(alfa*2)*(Pi/180);
segmentoEsfera=radioTrayectoria*betaRadianes;
balines=perimetroTrayectoria/(segmentoEsfera);
balinesCeil=ceil(balines);

//####Debugging####
//echo(balinesCeil);
echo("AnchoBalero",lonA);


angulo=360/(balinesCeil-ajusteEnteroBalines);
*/

balines=9;
angulo=360/balines;

for(i=[0:balines-1]){
    rotate(angulo*i)
    translate([5,radioTrayectoria,0])
balinReloj(lonA,lonB-tol);
    
}



 
  
    
}










//###RENDERIZADOS###
//Se recomienda no tener renderizados (debugging)
$fn=200;
/*
radioExterno        ->          radio Externo del rodamiento
radioInterno        ->          radio Interno del rodamiento
tol                         ->          tolerancia entre "baline y piezas externas e internas
lonA                       ->         ancho del rodamiento
lonB                        ->        medida de cierre entre las piezas interna y externa
*/
//rodamiento(radioExterno=11,radioInterno=4,tol=0.7,balines=10,lonA=12,lonB=2.5);


//balinReloj();

rodamientoCone();
