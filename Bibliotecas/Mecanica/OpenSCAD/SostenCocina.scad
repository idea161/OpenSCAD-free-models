

module Sosten(ancho1=30,espesor=4,Poste=70){

    difference(){
        union(){
            cube([ancho1,espesor,Poste-(ancho1/2)]);
            
            translate([ancho1/2,0,Poste-(ancho1/2)])
            rotate([-90,0,0])
            cylinder(d=ancho1,h=espesor);
            
            cube([ancho1,56,espesor]);

            translate([0,56,0])
            rotate(-45)
            cube([ancho1,64,espesor]);

            translate([0,56,0])
            rotate(-45)
            translate([ancho1/2,64,0])
            cylinder(d=ancho1,h=espesor+12);
        }
     
            translate([0,56,0])
            rotate(-45)
            translate([ancho1/2,64,0])
            cylinder(d=ancho1-(espesor*2),h=(espesor+12)*4,center=true);
        
        //tornillo1
        translate([ancho1/2,0,(ancho1/2)])
        rotate([90,0,0])
        cylinder(d=3.5,h=espesor*4,center=true);
        
        //tornillo2
        translate([ancho1/2,0,Poste-(ancho1/2)])
        rotate([90,0,0])
        cylinder(d=3.5,h=espesor*4,center=true);
      
    }
    
}

//####RENDERIZADOS####

$fn=100;

//prueba
/*
Sosten(ancho1=30,espesor=4,Poste=70);

translate([0,0,200])
rotate([0,180,0])
mirror([1,0,0]){
   Sosten(ancho1=30,espesor=4,Poste=70);
}
*/

//impresion



//Sosten(ancho1=30,espesor=4,Poste=70);

mirror([1,0,0]){
   Sosten(ancho1=30,espesor=4,Poste=70);
}