

module eslabon(espesor=2.5,alturaCadena=15,tol=1){
  ajustePorcentaje=1.3;
 porcentajeEsferas=0.95;
 translate([0,0,(alturaCadena/2)])
difference(){
    

        //cilindro abrazadera
        difference(){
          cylinder(d=espesor+(2*tol)+espesor,h=alturaCadena,center=true);
           cylinder(d=espesor+(2*tol),h=alturaCadena*2,center=true);  
            
        }
  
        viajeCorte=sqrt(2*pow((alturaCadena*2),2));
        
        //cubo corte inferior a abrazadera
        translate([-espesor*2,0,-(viajeCorte/2)-(alturaCadena/2)+(espesor*2.5)+(tol*2)])
    rotate([0,45,0])
    cube([alturaCadena*2,alturaCadena*2,alturaCadena*2],center=true);
    
        //cubo corte superior a abrazadera
    mirror([0,0,1]){
         translate([-espesor*2,0,-(viajeCorte/2)-(alturaCadena/2)+(espesor*2.5)+(tol*2)])
    rotate([0,45,0])
    cube([alturaCadena*2,alturaCadena*2,alturaCadena*2],center=true);
    }
    


}

//esfera 1
translate([(espesor/2)+tol,0,alturaCadena/2])
sphere(r=tol*porcentajeEsferas);

//esfera 1
translate([-(espesor/2)-tol,0,alturaCadena/2])
sphere(r=tol*porcentajeEsferas);

difference(){
    union(){
        //primer cilindro
        translate([espesor+tol,0,0])
        cylinder(d=espesor,h=alturaCadena);

        //segundo cilindro
        //TAMANIO DE ESLABON
        translate([(espesor*3)+(tol*2),0,0])
        cylinder(d=espesor,h=alturaCadena);

        
        //tapa inferior
        translate([espesor+tol,-espesor/2,0])
        cube([(2*tol)+(1.5*espesor),espesor,espesor*1.5]);

        //tapa superior
        translate([espesor+tol,-espesor/2,alturaCadena-(1.5*espesor)])
        cube([(2*tol)+(1.5*espesor),espesor,espesor*1.5]);
    }

   //cubo corte inferior a tapas
    translate([(espesor*3)+(4*tol),0,0])
    rotate([0,45,0])
    cube([espesor*2,espesor*2,espesor*2],center=true);
    
        //cubo corte superior a tapas
    
     translate([(espesor*3)+(4*tol),0,alturaCadena])
    rotate([0,45,0])
    cube([espesor*2,espesor*2,espesor*2],center=true);

}

}

//####RENDERIZADOS####

$fn=100;


espesor=2.5;
alturaCadena=30;
tol=1;

tamanioEslabon=(espesor*3)+(tol*2);

angulo=10;

copias=360/angulo;
echo(copias);

Pi=3.14159;

Perimetro=tamanioEslabon*copias;

//0.67 de ajustes
radioCadena=((Perimetro)/(2*Pi))-0.67;

cadena=1;

if(cadena==1){
    for(i=[0:copias-1]){
        rotate(angulo*i)
        translate([-tamanioEslabon/2,radioCadena,0])
        eslabon(espesor=espesor,alturaCadena=alturaCadena,tol=tol);
    }
}

/*
difference(){
    union(){
        translate([0,0,alturaCadena+(tol/2)])
        cylinder(r=radioCadena-(espesor/2),h=espesor/2);

        translate([0,0,-(espesor/2)-(tol/2)])
        cylinder(r=radioCadena-(espesor/2),h=espesor/2);
    }
    cylinder(r=radioCadena-((espesor+(2*tol)+espesor)/2)-espesor,h=4*alturaCadena,center=true);
}

translate([0,0,alturaCadena/2])
difference(){


        cylinder(r=radioCadena-(espesor/2)-(tol/4),h=alturaCadena+espesor+tol,center=true);
    
    for(i=[0:copias-1]){
        rotate(angulo*i)
     translate([-tamanioEslabon/2,radioCadena,0])
      cylinder(d=espesor+(2*tol)+espesor,h=alturaCadena*2,center=true);
    }
    
    //taladro central
       cylinder(r=radioCadena-((espesor+(2*tol)+espesor)/2)-(2*espesor),h=4*alturaCadena,center=true);

}
*/

