
module poste(diametroPoste=5,tornillo=2.5,alturaTornillo=10){
  
    translate([0,0,alturaTornillo/2])
            difference(){

                cylinder(d=diametroPoste,h=alturaTornillo,center=true);
                cylinder(d=tornillo,h=alturaTornillo*2,center=true);
               }
           

}//Fin modulo poste (para case gamma)

$fn=100;

tol=1;
diametro=60;
diamDet=20;
difference(){
	union(){
		cylinder(d=diametro,h=25);
		for(i=[0:5]){
			rotate((360/6)*i)
			translate([(diametro/2)-(diamDet*0.3),0,0])
			cylinder(d=diamDet,h=25);
		}
	}
	//recorte cabeza  tornillo
	  cylinder(d=21.5+(tol*2),h=(8*2)+(tol*2),$fn=6,center=true);
	
	//recorte externo maneral
	rotate((360/6)/2)
	for(i=[0:5]){
			rotate((360/6)*i)
			translate([(diametro/2)+(diamDet*0.3),0,0])
			cylinder(d=diamDet,h=100,center=true);
		}
		
		//recorte tornillo
		cylinder(d=12.5+tol,h=100,center=true);

//recorte altura
translate([0,0,25/2])
   poste(diametroPoste=diametro*2,tornillo=40,alturaTornillo=50);
	
		//recorte prisionero
	translate([12.5,0,25])
	cube([3.5+(2*tol),8+(2*tol),30],center=true);
		
		//recorte tornillo prisionero
		translate([0,0,19])
		rotate([0,90,0])
		cylinder(d=4+tol,h=60);


}