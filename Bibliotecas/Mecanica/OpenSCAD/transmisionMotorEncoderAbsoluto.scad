include<EngraneBanda.scad>;

 //EngraneBanda(alturaEngrane=6.5,espesor=1,flecha=5,tol=0.75,diamDiente=1.3,diamEngrane=9,dientes=14);
 
 //#######POSTE CASE GAMMA#####
module poste(diametroPoste=5,tornillo=2.5,alturaTornillo=10){
  
    translate([0,0,alturaTornillo/4])
            difference(){

                cylinder(d=diametroPoste,h=alturaTornillo/2,center=true);
                cylinder(d=tornillo,h=alturaTornillo*2,center=true);
               }
           

}//Fin modulo poste (para case gamma)


module EngraneSimple(flecha=11.5,tol=0.75,alturaEng=8.5,flecha=11.5,alturaPoste=33){
    
    
    
       translate([0,0,alturaPoste-alturaEng])

     EngraneBanda(alturaEngrane=6.5,espesor=1,flecha=11.5,tol=0.75,diamDiente=1.3,diamEngrane=14,dientes=20);
    
 //EngraneBanda(alturaEngrane=6.5,espesor=1,flecha=flecha,tol=0.75,diamDiente=1.3,diamEngrane=9,dientes=14);

    
  //rondana a engrane Simple
   //translate([0,0,0])
 //linear_extrude(height=7.9+4+5)
//rondanas(radioExterno=4,radioInterno=(flecha/2)+(tol/2));  
translate([0,0,7])
poste(diametroPoste=13+5,tornillo=flecha+tol,alturaTornillo=alturaPoste);    
    
         rotate([0,0,90])
      difference(){
     poste(diametroPoste=5+16+5,tornillo=flecha+tol,alturaTornillo=15);
        //TUERCA
        viajeTuerca=6.5+3;
          
        translate([viajeTuerca,0,6])
         rotate([0,90,0])
     cylinder(d=6,$fn=6,h=2.5);
         translate([viajeTuerca,0,4])
         rotate([0,90,0])
     cylinder(d=6,$fn=6,h=2.5);
        //EJEtornillo 
        translate([0,0,4])
        rotate([0,90,0])
           cylinder(d=3.5,h=40);
         }

}
 
$fn=100;

espesor=1.5;
alturaEng=8.5;

  rotate([90,0,0])
EngraneSimple(flecha=11.5,tol=0.75,alturaEng=8.9,alturaPoste=33);
import("../STL/BandasEncoder/engraneSimpleBandaEncoder.stl");


translate([70,0,2])
  rotate([90,0,0])
//import("../STL/BandasEncoder/engraneQuintuple.stl");
for(i=[0:4]){
    translate([0,0,(alturaEng-1.5)*i]) 
    EngraneBanda(alturaEngrane=6.5,espesor=1,flecha=11.5,tol=0.75,diamDiente=1.3,diamEngrane=14,dientes=20);
}

translate([70,-17.5,2])
apoyoBanda(dimTornillo=5,diamApoyo=20,espesor=1.5,sePlacas=38,alturaRecta=50,seTor=7);

translate([220,0,2])
  rotate([90,0,0])
//import("../STL/BandasEncoder/engraneCuadruple.stl");
for(i=[0:3]){
    translate([0,0,(alturaEng-1.5)*i]) 
    EngraneBanda(alturaEngrane=6.5,espesor=1,flecha=11.5,tol=0.75,diamDiente=1.3,diamEngrane=14,dientes=20);
}

translate([220,-13,2])
apoyoBanda(dimTornillo=5,diamApoyo=20,espesor=1.5,sePlacas=30,alturaRecta=50,seTor=7); 


module apoyoBanda(dimTornillo=5,diamApoyo=30,espesor=1.5,sePlacas=20,alturaRecta=35,seTor=7){

difference(){    
    union(){
        rotate([90,0,0])
        cylinder(d=diamApoyo,h=sePlacas+(espesor*2),center=true);
        translate([0,0,-alturaRecta/2])
        cube([diamApoyo,sePlacas+(espesor*2),alturaRecta],center=true);
    }
    rotate([90,0,0])
    cylinder(d=dimTornillo,h=sePlacas*2,center=true);
    
    translate([0,0,-(diamApoyo/2)-(espesor*2)])
    cube([diamApoyo*2,sePlacas,diamApoyo+alturaRecta],center=true);
    
    translate([0,seTor,0])
    cylinder(d=dimTornillo,h=alturaRecta*6,center=true);
    
    translate([0,-seTor,0])
    cylinder(d=dimTornillo,h=alturaRecta*6,center=true);
  } 
}
 
//ALTURA FLECHA 78 mm -> alturaRecta en modulo apoyo banda
//!apoyoBanda();