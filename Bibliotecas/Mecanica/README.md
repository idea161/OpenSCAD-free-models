# Elementos Máquinas


![](./PNG/10Abril18/CNC.png)


## Summary/Descripción


In the following documents you will find useful files for the design and planning of design machines such as 3D printers or CNC type machines; in them you will find corners, profiles, screws, washers, etc.

En los siguientes documentos se encuentran archivos de utilidad para el diseño y planeación de máquinas de diseño tales como impresoras 3D o máquinas tipo CNC; en el encontrarán esquineros, perfiles, tornillos, rondanas, etc.

