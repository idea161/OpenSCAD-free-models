# Utilidades

## Summary/Descripción

This project aims to have code that contains easy to use and parameterizable modules, to make use of it there are parallel projects that import files from here using the following line:

include<./../../Bibliotecas/Utilities/utilities.scad>;

Éste proyecto tiene como objetivo tener código que contenga módulos parametrizables y fáciles de usar, para hacer uso del mismo existen proyectos en paralelo que importan archivos desde aqui haciendo uso de la siguiente línea:

include<./../../Bibliotecas/Utilidades/utilities.scad>;

