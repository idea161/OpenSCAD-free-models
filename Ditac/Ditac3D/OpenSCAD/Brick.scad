

module Brick(alturaFac=1,anchoFac=2,largoFac=4,texto="1.61"){


alturaPin=1.95;
diamPin=4.9;
diamAntiPin=6.5;
//alturaTercio=3.18;
//echo(3.18*3);
alturaEntero=9.6;
anchoEntero=7.8;
espesor=1.23;



difference(){
cube([anchoEntero*anchoFac,anchoEntero*largoFac,(alturaEntero/3)*alturaFac]);
    
    translate([espesor,espesor,0])
    cube([(anchoEntero)*anchoFac-(2*espesor),(anchoEntero)*largoFac-(2*espesor),((alturaEntero/3)*alturaFac-espesor)]);
}

//PINES
translate([anchoEntero/2,anchoEntero/2,((alturaEntero/3)*alturaFac)-espesor])

for(i=[0:anchoFac-1]){
    for(j=[0:largoFac-1]){
        translate([i*anchoEntero,j*anchoEntero])
        difference(){
          cylinder(d=diamPin,h=alturaPin+espesor);
            cylinder(d=diamPin-(0.99*2),h=alturaPin*4,center=true);
        }
    }
}



if(largoFac==1||anchoFac==1){
}
else if(largoFac>1||anchoFac>1){
    translate([anchoEntero*1,anchoEntero*1])
for(i=[0:anchoFac-2]){
    for(j=[0:largoFac-2]){
        
      translate([i*anchoEntero,j*anchoEntero])  
difference(){
          cylinder(d=diamAntiPin,h=(alturaEntero/3)*alturaFac);
            cylinder(d=diamAntiPin-(0.79*2),h=(alturaEntero/3)*2*alturaFac,center=true);
        }
    }
    
}

}// fin else if
    


translate([(anchoFac*anchoEntero)/2,espesor/2,((alturaEntero/3)*alturaFac)*0.5])
rotate([90,0,0])
linear_extrude(height=espesor)
 text(size=5,font = "Simplex",texto,halign ="center",valign ="center");
 

    }//fin module Brick
    
    //####RENDERIZADOS####
    
    $fn=50;
    
    //Brick(alturaFac=3,anchoFac=5,largoFac=1,texto="IDEA 1.61");
    
    for(i=[2:4]){
        translate([0,10*i,0])
    Brick(alturaFac=3,anchoFac=i,largoFac=1,texto="");
        
    }


