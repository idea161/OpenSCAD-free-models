
altura=30;
divisiones=8;
diametro=20;
decremento=diametro/divisiones;

/*for(i=[0:divisiones-1])
    translate([0,0,(altura/divisiones)*i])
cylinder(d1=diametro-(i*decremento),d2=diametro-(4*i*decremento),h=altura/divisiones);
*/


cylinder(d1=diametro,d2=0.1,h=altura);
giro=360;

diamEsfera=3;

translate([0,0,diamEsfera])
for(i=[0:8]){
   for(j=[0:4]){
       rotate((giro/divisiones)*i)
    translate([(diametro/2)-(j*diamEsfera*0.7)-1,0,j*diamEsfera*2])
   sphere(d=diamEsfera);
   }
}