                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:34394
Christmas tree Jamboree 01 by cerberus333 is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

A few trees for your holiday needs.

note:
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
xtr3.stl --- has errors when slicing 
xtr4.stl --- has errors when slicing
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Mostly modeled in truespace5

# Instructions

My  print setting are
.27 slice
0% infill
1 shell

I am looking to make very light low plastic use trees.

printed 
xtr1.stl 25 grams
xtr2.stl 28 grams
xtr5.stl 41grams
xtr6.stl 37.6grams
xtr7.stl 41grams
xtr8.stl 28.5grams

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
xtr3.stl --- has errors when slicing
xtr4.stl --- has errors when slicing
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!