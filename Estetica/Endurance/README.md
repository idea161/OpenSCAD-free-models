# Endurance

![](./PNG/Endurance.png)

## Summary/Descripción

This is a piece for work or some engineering, art, sculpture or construction project, the figure is parameterizable and/or modifiable, in the photos we can see each component of the total piece, remembering that we can change the size, the number of components or some specific characteristic, as much as we want.The ideas for use that we can recommend are to build a spacecraft, rocket or some armature. 

Esta es una pieza para trabajo o algún proyecto ingenieril,de arte,escultura o construcción,la figura es  parametrizable y/o  modificable, en las fotos podemos ver cada componente de la pieza total, recordado que podemos cambiar el tamaño, el número de componentes o alguna característica específica, tanto como queramos.Las ideas para uso que podemos recomendar es para construir una nave espacial, cohete o algún armable. 

