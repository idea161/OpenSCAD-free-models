include<./../../Bibliotecas/text_on_OpenSCAD/text_on.scad>


default_extrusion_height = 2;

default_size = 7;

module SkullNamed(texto="Pedro"){
 
 radio=27;
 translate([0,32,30])
 rotate([-54,0,0])
 translate([0,7,-6])
{
%cylinder(r=radio,h=10, center=true);

//%text_on_cylinder("face = bottom, cyl_center",[0,0,0],r=20,h=40,face="bottom",cylinder_center=true);
//%text_on_cylinder("rotate = 30, east = 90, cyl_center",[0,0,0],r=20,h=40,spacing=1.2,rotate=30,eastwest=90,cylinder_center=true);

text_on_cylinder(texto,[0,0,0],r=radio,h=10,spacing=0.8,cylinder_center=true);

}
//viaje=85;
//translate([-viaje,viaje,-6])

scale([0.5,0.5,0.5])
 import("STL/Skull_s.stl");

}//fin modulo

SkullNamed(texto="María");
