# Slave I

![](./PNG/SlaveI.png)

https://www.thingiverse.com/thing:2974526

## Summary/Descripción

This model is an amateur model based on Boba Fett's favorite ship, the model can be used as a test print to verify the printer's bridges, layers and arches.

Este modelo es un modelo amateur basado en la nave favorita de Boba Fett, el modelo puede ser usado como impresión de prueba para verificar los puentes, capas y arcos de la impresora.

