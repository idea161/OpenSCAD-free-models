
lonX=60;
lonY=55;
lonZ=80;
minkow=3;
espesor=2;
$fn=100;

   module recortadorMink(lonX=20,lonY=20,lonZ=20,esf=1){
        minkowski(){
        cube([lonX-(2*esf),lonY-(2*esf),lonZ-(2*esf)],center=true);
            sphere(r=esf);
            }
    }
    
    translate([-24,-26,-((lonZ+3)/4)-2])
rotate([90,0,0])
 linear_extrude(height=2.5)
text(size=8,font = "Simplex","Idea1.61");

rotate([0,180,0])
difference(){
  
     recortadorMink(lonX,lonY,lonZ,minkow);
    
        cube([lonX-(2*espesor),lonY-(2*espesor),lonZ-(2*espesor)],center=true);
        
    translate([0,0,-lonZ])
        cube([lonX*2,lonY*2,lonZ*2],center=true);
        
}