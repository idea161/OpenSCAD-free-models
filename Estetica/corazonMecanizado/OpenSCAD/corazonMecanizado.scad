  use <modulosCorazon.scad>;
   
//####RENDERIZADOS#####
    
    $fn=200;
//color("red",alpha=0.5)  
color("red")  

!tapaCorazon();

alturaEng=4;

PitchRadius=13.3;

color("black")
translate([0,0,2.5])
union(){
    translate([PitchRadius,0])
    rotate(360/48)
     engraneCorazon();   
     translate([-PitchRadius,0])
       engraneCorazon();   
     translate([-PitchRadius,PitchRadius*2])
     rotate(360/48)
     engraneCorazon();    
  }      

color("red")
baseCorazon();  
        
 