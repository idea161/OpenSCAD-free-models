use <MCAD/involute_gears.scad>;

//#####POSTE####

//utilidad para los modulos siguientes
   module Poste(altura=10,espesor=3,tol=1,diamShaft=2){
	   
	   difference(){
	      cylinder(d=(espesor*2)+diamShaft+tol,h=altura);
	   
		  cylinder(d=diamShaft+tol,h=altura*4,center=true);
	      
	   }
   }
   
   //####TAPA CORAZON####
   
   //tapa para la base y contiene los engranes en su lugar
   module tapaCorazon(PitchRadius=13.3,mikoC=0,texto="M.M"){
         difference(){
        union(){
            hull(){ 
                translate([PitchRadius,0,7+2.9])
                cylinder(d=11.5,h=2);
                
               translate([-PitchRadius,0,7+2.9])
                    cylinder(d=11.5,h=2);
            }
            
             hull(){ 
           
                
               translate([-PitchRadius,0,7+2.9])
                    cylinder(d=11.5,h=2);
                 
                 translate([-PitchRadius,PitchRadius*2,7+2.9])
                    cylinder(d=11.5,h=2);
            }
        }
        
        //agujeros tornillos
      translate([PitchRadius,0,7])
           cylinder(d=2.7,h=20,center=true);
         
        translate([-PitchRadius,0,7])
           cylinder(d=2.7,h=20,center=true);
         ;

   translate([-PitchRadius,PitchRadius*2,7])  
     cylinder(d=2.7,h=20,center=true);
        
        if(mikoC==1){
            //translate([0,0,0])
            minkowski(){
                cube([14,0.1,40],center=true);
                cylinder(d=2,h=0.1);
            }
            
           translate([-PitchRadius,PitchRadius,0])
            minkowski(){
                cube([0.1,14,40],center=true);
                cylinder(d=2,h=0.1);
            }
       }
          
              
        
    }
    
      translate([PitchRadius,0,7])
           Poste(altura=3,espesor=2,tol=1,diamShaft=6.5);
         
        translate([-PitchRadius,0,7])
           Poste(altura=3,espesor=2,tol=1,diamShaft=6.5);

   translate([-PitchRadius,PitchRadius*2,7])  
   Poste(altura=3,espesor=2,tol=1,diamShaft=6.5);
     
    

//rotate
    translate([0,0,11])
linear_extrude(height=3)
 text(size=7,font = "Simplex:style=Bold",texto,halign ="center",valign="center");
    
   }//fin tapa corazon
   
   
   
   
   tapaCorazon(PitchRadius=13.3,mikoC=0,texto="M.M");
   //#######ENGRANE CORAZON#####
   
   //modulo de pieza mecanica movil tipo engrane
   module engraneCorazon(alturaEng=4,PitchRadius=13.3){
       
         difference(){
        gear(circular_pitch = 200,
                number_of_teeth = 24,
                gear_thickness = alturaEng,
                rim_thickness = alturaEng,
                hub_thickness = alturaEng,
                bore_diameter = 7.5);
        for(i=[0:7]){
            rotate(60*i)
            translate([8,0,0])
            cylinder(d=4,h=20,center=true);
        }
    }
   }
   
   
   //###### BASE CORAZON #####
   module baseCorazon(ajC=2,alturaEng=4,PitchRadius=13.3,radCo=12){
 
 adjC=10;
adjD=2;
       


  union(){
      linear_extrude(height=2)
      difference(){
          union(){
              //difference(){
                  hull(){
                     
                      
                      translate([-(PitchRadius)-adjC,-adjC])
                       circle(r=radCo/4+ajC);
                      // translate([-(PitchRadius),0])
                   //   square([(PitchRadius+ajC)*2,(PitchRadius+ajC)*2],center=true);
                      
                      //translate([PitchRadius,0]) 
                      //circle(r=PitchRadius+ajC);
                      
                     translate([-PitchRadius,PitchRadius*2]) 
                      circle(r=radCo+ajC);
                     
                  
                  
                    
                  translate([-0.5+adjD,13+adjD]) 
                  circle(d=7,$fn=4);  
                      
                      
                      }
                  
                 
                  
                  
                  
                //  translate([10,PitchRadius+10])
                 // circle(d=20);
              //}
              hull(){
                  
                  //translate([-(PitchRadius),0])
                  //square([(PitchRadius+ajC)*2,(PitchRadius+ajC)*2],center=true);
                   translate([-(PitchRadius)-adjC,-adjC])
                       circle(r=radCo/4+ajC);
                  
                      translate([PitchRadius,0]) 
                      circle(r=radCo+ajC);
                  
                   translate([-0.5+adjD,13+adjD])  
                  circle(d=7,$fn=4); 
                  
                  //translate([-PitchRadius,PitchRadius*2]) 
                  //circle(r=PitchRadius+ajC);
                  
              }
          }
          //agujero colgante
          translate([-0.5+adjD,13+adjD])
         circle(d=4);
          
          //agujero apoyo 1
          translate([PitchRadius,0,0])
         circle(d=2.7);
       
          //agujero apoyo 2
        translate([-PitchRadius,0,0])
               circle(d=2.7);
          
          //agujero apoyo 3
       translate([-PitchRadius,PitchRadius*2,0])
        circle(d=2.7);
          
           //agujero estetico 1
       //translate([-PitchRadius,PitchRadius*2,0])
        circle(d=12,$fn=6);
          
          //agujero estetico 1
       translate([-PitchRadius,PitchRadius,0])
       rotate(30)
        circle(d=12,$fn=6);
        
        //recortes tipo besier
        comp=110;
        diam=comp*2;
        adjB=5;
        
        translate([-(PitchRadius/2)+adjB,-PitchRadius-comp,0])
        circle(d=diam);
        
        translate([-PitchRadius*2-comp,(PitchRadius/2)+adjB,0])
        circle(d=diam);
        
          
      }
      

      //poste agarre
       translate([-0.5+adjD,13+adjD])
      Poste(altura=6,espesor=1.5,tol=0,diamShaft=4);
      
      translate([PitchRadius,0,0])
        
          Poste(altura=10,espesor=2,tol=1,diamShaft=1.7);
      //cylinder(d=11,h=2);
        translate([-PitchRadius,0,0])
               Poste(altura=10,espesor=2,tol=1,diamShaft=1.7);
        //cylinder(d=11,h=2);
       translate([-PitchRadius,PitchRadius*2,0])
        //cylinder(d=11,h=2);
               Poste(altura=10,espesor=2,tol=1,diamShaft=1.7);
      
  }
       
 }
 
 //$fn=120;
 
 //baseCorazon();  
 
 

   
   
