
espesor=3.5*2;
tol=1;

//[0,1,0]
mirror([0,1,0]){

difference(){
    rotate([0,45,0])
    translate([0,-4.5,-espesor/2])
    cube([23.26-(12.8/2)-(tol/2),9,espesor]);
  
     translate([0,0,20])
    cube([40,40,40],center=true);
}


rotate([0,45,0])
translate([23.26,0,0])
    rotate([90,0,0])
difference(){

  
    cylinder(d=12.8+tol+(2*espesor),h=9,center=true);
        
   
    cylinder(d=12.8+(tol*0.5),h=9,center=true);
    
}

difference(){
     
   union(){   
        translate([0,0,-espesor])
        minkowski(){
            linear_extrude(height=espesor)
            polygon([[-18.89,0],[-18.89-13.22-1.44,8.53+1.44],[-18.89-13.22-0.1-1.44,8.53+1.44-0.1],[-18.89-0.1,-0.1]]);
            cylinder(d=13,h=0.1);
        }
           
        translate([-18.9+(espesor/4),-9/2,-espesor])
        cube([18.89+(espesor/2),9,espesor]);
    }
       
    translate([-18.89,0,0])
    cylinder(d=3.6+tol,h=20,center=true);

    translate([-18.89-13.22-1.44,8.53+1.44,0])
    cylinder(d=3.6+tol,h=20,center=true);
    
    translate([-18.89,0,-espesor])
    sphere(d=8);

    translate([-18.89-13.22-1.44,8.53+1.44,-espesor])
    sphere(d=8);
   
}

}


$fn=120;