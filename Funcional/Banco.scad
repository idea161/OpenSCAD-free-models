lon=18;
dim=16;
tol=1;
esp=4;

cuboIn=dim+tol;
cubo=cuboIn+(2*esp);

difference(){
    union(){
        translate([0,0,(cubo/2)-(esp/2)])
        cube([cubo,lon,cubo-esp],center=true);
        translate([0,0,esp/2])
        cube([cubo+(2*dim),lon,esp],center=true);
    }
    cube([cuboIn,lon*2,cuboIn*2],center=true);
    
    
     //tornillo 1
    translate([(cubo/2)+(dim/2),0,0])
    cylinder(d=3+tol,h=lon,center=true);
    
     //tornillo 2
      translate([-((cubo/2)+(dim/2)),0,0])
    cylinder(d=3+tol,h=lon,center=true);
    /*
    //tornillo (1,1)
    translate([(cubo/2)+(dim/2),(lon/2)-(dim/2),0])
    cylinder(d=3+tol,h=lon,center=true);
    
     //tornillo (-1,-1)
      translate([-((cubo/2)+(dim/2)),-((lon/2)-(dim/2)),0])
    cylinder(d=3+tol,h=lon,center=true);
    
    //tornillo (1,-1)
      translate([((cubo/2)+(dim/2)),-((lon/2)-(dim/2)),0])
    cylinder(d=3+tol,h=lon,center=true);
    
     //tornillo (-1,1)
      translate([-((cubo/2)+(dim/2)),((lon/2)-(dim/2)),0])
    cylinder(d=3+tol,h=lon,center=true);
    */
}