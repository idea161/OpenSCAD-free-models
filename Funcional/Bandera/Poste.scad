
module bandera(diamPoste=12.7,tol=1,espesor=3,alturaBandera=60,largoBandera=130,logo=1){

mirror([1,0,0]){

union(){
    
    
    translate([0,0,(alturaBandera*0.5)-espesor*1.5])
    cylinder(d=diamPoste+(2*tol)+(2*espesor),h=espesor*1.5,center=true);
    
    
    translate([0,0,12]) 
    difference(){
        cylinder(d=diamPoste+(2*tol)+(2*espesor),h=alturaBandera*0.7,center=true);
        cylinder(d=diamPoste+(2*tol),h=alturaBandera*2,center=true);
    }

//lienzo Bandera
    translate([(largoBandera/2),(diamPoste/2)+(espesor*0.25)+tol,alturaBandera*0.25])


difference(){
    cube([largoBandera,espesor*1.5,alturaBandera],center=true);
   rotate([0,18,0])
    translate([0,0,largoBandera*0.55])
    cube([largoBandera*2,largoBandera,largoBandera],center=true);
  
	//recorte poste
	translate([-alturaBandera/2-diamPoste/2,-diamPoste/2,0])
	  cylinder(d=diamPoste+(2*tol),h=alturaBandera*2,center=true);
	
    //recorte marco
    translate([3,-espesor*1.8,-3])
	scale([0.9,2,0.9])
	difference(){
	       cube([largoBandera,espesor*2,alturaBandera],center=true);
           rotate([0,18,0])
           translate([0,0,largoBandera*0.55])
           cube([largoBandera*2,largoBandera,largoBandera],center=true);
		}
	
	}


}

if(logo==1){
   translate([68,(diamPoste/2)+(espesor/2)+tol,-2])
    rotate([90,0,0])
    scale([5.5,5.5,1.5])
    mirror([1,0,0]){
        import("./STL/BARK.stl");
    }
}




  }
  
if(logo==2){
   translate([-67.5,(diamPoste/2)+(espesor/2)+tol,4])
    rotate([90,0,0])
    scale([5.8,5.8,1.5])
    import("./STL/perforDogsB.stl");
}
  
  
  
}



//#####RENDERIZADOS#####

$fn=200;

bandera(diamPoste=7.97,tol=1,espesor=3,alturaBandera=120,largoBandera=130,logo=1);


