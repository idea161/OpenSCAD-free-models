

include<./../CASEgamma/CASEgamma.scad>;
include<./../Logos/LogoIdea161.scad>;
include<./../Logos/StarWars.scad>;
include<./../Endurance/OpenSCAD/Endurance.scad>;


module cartera(lonX=60,lonY=95,lonZ=16,espesor=1.5,minkow=3,texto1="IDEA 1.61",texto2="",texto3="",logo=0,cruzado=0){
   
//triangulo soporte (impresion vertical)
    /*
    translate([-lonX/2+(espesor*0.75),lonY/2-espesor,espesor/2])
    rotate([90,0,-90])
    translate([0,0,-espesor/2])
    linear_extrude(height=espesor)
    polygon([[4,0],[0,4],[0,0]]);
*/
difference(){
    union(){
        CASEgamma(lonX=lonX,lonY=lonY,lonZ=lonZ,espesor=espesor,diametroPoste=0,tornillo=2.5,parte=0,minkow=minkow,soporteA=0,soporteB=0,soporteC=0,soporteD=0);

       CASEgamma(lonX=lonX,lonY=lonY,lonZ=lonZ,espesor=1.5,diametroPoste=0,tornillo=2.5,parte=1,minkow=3,soporteA=0,soporteB=0,soporteC=0,soporteD=0);
    }
 
    
 //recorte en ángulo   
    translate([-20.5,-25,0])
    rotate(62)
    translate([-lonX,1,espesor*0.5])
cube([lonX*4,lonY*4,lonZ*2]); 
    
       //recorte parte abierta

  /*
    recPartAb=lonY-(2*espesor);
    translate([-lonX-(lonX/2)+(espesor*1.1),-(lonY-(2*espesor))/2,espesor/2])
 difference(){
cube([lonX,lonY-(2*espesor),lonZ]); 
    corte=4.5;
       diagCorte=(sqrt((corte*corte)+(corte*corte)));
       translate([0,recPartAb,-diagCorte/2])
       rotate([45,0,0])
        cube([lonX,corte,corte]);
      
      translate([0,0,-diagCorte/2])
      rotate([45,0,0])
        cube([lonX,corte,corte]);
      
      
        }
        */
    
   //copia recorte en angulo (73 grados)
    
    if(cruzado==0){
    mirror([0,0,1]){
         translate([-20.5,-20,0])
        rotate(73)
        translate([-lonX,0,lonZ/2-espesor*1.5])
    cube([lonX*4,lonY*4,lonZ*2]); 
    }
    
   }else if(cruzado==1){
       rotate([180,0,0])
       translate([-20.5,-20,0])
        rotate(73)
        translate([-lonX,0,lonZ/2-espesor*1.5])
    cube([lonX*4,lonY*4,lonZ*2]); 
   
       
    }
    
}

//poste interno



difference(){
cube([lonX+(espesor),lonY+(espesor),espesor],center=true);
    orillas=4;
    
    translate([0,0,-espesor])
    linear_extrude(height=espesor*2)
    difference(){
    square([lonX-(orillas/2),lonY-(orillas/2-(2*minkow))],center=true);
RecortadorEstrella(lonX=lonX, lonY=lonY,orillas=orillas,redondeadorPoly=2);
  }
  
  //recortador de orillas minkowski
  difference(){
      cube([lonX+(2*espesor),lonY+(2*espesor),lonZ+(2*espesor)],center=true);
      recortadorMink(lonX=lonX+(espesor/8),lonY=lonY+(espesor/8),lonZ=lonZ,esf=minkow);
  }
  
}  

if(logo==0){
 //logo idea
    translate([0,-20,lonZ/2-espesor/2])
    linear_extrude(height=espesor)
    Idea(0.18);
}
else if(logo==1){
    //logo imperial
    translate([0,-20,lonZ/2-espesor/2])
    linear_extrude(height=espesor)
Imperial(radio=lonX/4);

}

else if(logo==2){
    translate([0,-20,lonZ/2-espesor/2])
    linear_extrude(height=espesor)
PrimeraOrden(radio=lonX*0.20);

}

else if(logo==3){
    translate([0,-20,lonZ/2-espesor/2])
    linear_extrude(height=espesor)
Republica(radio=lonX/4);

}
else if(logo==4){
translate([0,-20,lonZ/2-espesor/2])
 linear_extrude(height=espesor)
    resize([lonX/2,lonX/2])
    projection()
    rotate([90,0,0])
    GanzEndurance();
}

else if(logo==5){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2,lonX/2,espesor])
    import("./../Logos/STL/aperture.stl");

}

else if(logo==6){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2-1,lonX/2-1,espesor])
    import("./../Logos/STL/artic_monkeys.stl");


}

else if(logo==7){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2,lonX/2,espesor])
    import("./../Logos/STL/death_star_logo.stl");


}
else if(logo==8){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2,lonX/2,espesor])
   import("./../Logos/STL/eva_01_logo.stl");


}
else if(logo==9){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2-8,lonX/2,espesor])
 import("./../Logos/STL/falcon_logo.stl");
}

else if(logo==10){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2-5,lonX/2-2,espesor])
  
import("./../Logos/STL/fd_logo.stl");

}

else if(logo==11){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2-8,lonX/2-8,espesor])
 import("./../Logos/STL/fq_logo.stl");

}

else if(logo==12){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2-8,lonX/2-8,espesor])
 import("./../Logos/STL/inge_logo.stl");
}

else if(logo==13){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2,lonX/2,espesor])
 import("./../Logos/STL/iron_man_logo.stl");

}

else if(logo==14){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2,lonX/2,espesor])
 import("./../Logos/STL/jedi_logo.stl");
}

else if(logo==15){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2,lonX/2,espesor])
import("./../Logos/STL/mand_logo.stl");

}

else if(logo==16){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2,lonX/2,espesor])
import("./../Logos/STL/nerv_logo.stl");
}

else if(logo==17){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2-8,lonX/2,espesor])
import("./../Logos/STL/noFace.stl");

}

else if(logo==18){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2,lonX/2,espesor])
import("./../Logos/STL/pumas_logo.stl");

}

else if(logo==19){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2-8,lonX/2-4,espesor])
import("./../Logos/STL/rasp_logo.stl");

}

else if(logo==20){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2,lonX/2,espesor])
import("./../Logos/STL/Rebel_Alliance_logosvg.stl");
}

else if(logo==21){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2,lonX/2-12,espesor])

import("./../Logos/STL/tie_vader.stl");

}

else if(logo==22){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2,lonX/2,espesor])

import("./../Logos/STL/tux_logo.stl");

}

else if(logo==23){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2-4,lonX/2-4,espesor])

import("./../Logos/STL/UNAM.stl");

}

else if(logo==24){
    translate([0,-20,lonZ/2-espesor/2])
    resize([lonX/2,lonX/2-16,espesor])

import("./../Logos/STL/xwing_logo.stl");

}



//textoBASE
//TAMAÑO DE TEXTO ORIGINAL==6
//translate([0,-lonY/2+5,lonZ/2-espesor/2])
//linear_extrude(height=espesor)
 //text(size=3,font = "Simplex",texto1,halign ="center");

translate([0,-lonY/2+5,lonZ/2-espesor/2])
linear_extrude(height=espesor)
 text(size=6,font = "Simplex",texto1,halign ="center");

rotate([0,180,90])
translate([0,-lonX/2+6+(minkow*4),lonZ/2-espesor/2])
linear_extrude(height=espesor)
 text(size=6,font = "Simplex",texto2,halign ="center");

rotate([0,180,90])
translate([0,-lonX/2+(minkow*2),lonZ/2-espesor/2])
linear_extrude(height=espesor)
 text(size=6,font = "Simplex",texto3,halign ="center");
 

}//fin modulo


//####RENDERIZADOS####

$fn=100;

//EVANGELION MOD
cartera(lonX=60,lonY=95,lonZ=16,espesor=1.5,minkow=3,texto1="Penserbjorne",texto2="God in his heaven,",texto3="all right with the world",logo=8,cruzado=0);

//cartera(lonX=60,lonY=95,lonZ=16,espesor=1.5,minkow=3,texto1="Penserbjorne",texto2="God in his heaven,",texto3="all right with the world",logo=16,cruzado=0);