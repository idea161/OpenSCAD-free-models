# Wallet/Cartera

![](./PNG/Idea AzulIdea.png)

## Summary/Descripción

The next project is a one piece wallet, in which you have two compartments one secure (you can only get the objects one by one) and one quick access. In the design you can edit the text included and a logo.

El siguiente proyecto es una cartera de una sola pieza, en ella se tienen dos compartimientos uno seguro (solo se pueden obtener los objetos de uno en uno) y uno de acceso rápido. En el diseño se le puede editar el texto incluido y un logo.




