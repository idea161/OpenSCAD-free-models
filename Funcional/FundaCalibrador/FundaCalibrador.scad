use<LogoIdea161.scad>;

//#####MODULOS####


module CaliperSilouette(espesor=3,hulled=0){
    pt=0.01;

lonX=238;
lonY=79.12;

lonX1=30;
lonY1=27.88;

lonX2=60.27;
lonY2=10.83;

lonX2_2=lonX-(lonX1+lonX2);

lonX3=16.54;
lonY3=8.46;

lonX4=35.56;
lonY4=6.69;

lonX5=28.36;
lonY5=7.6;
   
lonX6=lonX-(lonX3+lonX4+lonX5);
lonY6=lonY-(lonY1+lonY2+lonY3+lonY4+lonY5);

minkAd=6;

  if(hulled==0){
   union(){
       
         
        translate([-espesor,-espesor])
        square([lonX3+(espesor*2),lonY+(2*espesor)]);
       
        translate([-espesor,-espesor])
        square([lonX1+(espesor*2),lonY-lonY3+(espesor*2)]);
       
        translate([-espesor,lonY1-espesor])
        square([lonX3+lonX4+(espesor*2),lonY-lonY3-lonY1+(espesor*2)]);
       
        translate([-espesor,lonY1-espesor])
        square([lonX3+lonX4+lonX5+(espesor*2),lonY-lonY3-lonY1-lonY4+(espesor*2)]);
       
        translate([-espesor+minkAd,lonY1-espesor+minkAd])
       //cuadrado original //square([lonX3+lonX4+lonX5+(lonX6-lonX2_2)+(espesor*2),lonY-lonY3-lonY1-lonY4-lonY5+(espesor*2)]);

     minkowski(){
        square([lonX3+lonX4+lonX5+(lonX6-lonX2_2)+(espesor*2)-(2*minkAd),lonY-lonY1-lonY4-lonY5+(espesor*2)-(2*minkAd)]);
         circle(r=minkAd);
     }

        translate([-espesor,lonY1+lonY2-espesor])
        square([lonX+(espesor*2),lonY6+(espesor*2)]);
    }
  }else if(hulled==1){
  hull(){
    union(){
        translate([-espesor,-espesor])
        square([lonX3+(espesor*2),lonY+(2*espesor)]);
        translate([-espesor,-espesor])
        square([lonX1+(espesor*2),lonY-lonY3+(espesor*2)]);
        translate([-espesor,lonY1-espesor])
        square([lonX3+lonX4+(espesor*2),lonY-lonY3-lonY1+(espesor*2)]);
        translate([-espesor,lonY1-espesor])
        square([lonX3+lonX4+lonX5+(espesor*2),lonY-lonY3-lonY1-lonY4+(espesor*2)]);
        translate([-espesor,lonY1-espesor])
        square([lonX3+lonX4+lonX5+(lonX6-lonX2_2)+(espesor*2),lonY-lonY3-lonY1-lonY4-lonY5+(espesor*2)]);

        translate([-espesor,lonY1+lonY2-espesor])
        square([lonX+(espesor*2),lonY6+(espesor*2)]);
    }//end union
   }//end hull
  }//end if
}//end module

module triangulo(lonY=10,lonX=10,alto=10){
    linear_extrude(height=alto);
    polygon([[0,0],[lonX,0],[0,lonY]]);
}

//!triangulo(lonY=10,lonX=10,alto=10);

//######RENDERIZADOS#######

$fn=100;

lonX=238;
lonY=79.12;

grosorCaliper=16.72;
espesor=3;

corte=200;

difference(){
  //union(){  
      minkowski(){
          linear_extrude(height=grosorCaliper)
          CaliperSilouette(espesor=0,hulled=1);
          sphere(r=espesor);
       }
      
   //}
    //translate([0,0,espesor])
  linear_extrude(height=grosorCaliper*2)
  CaliperSilouette(espesor=0,hulled=0);
   
   //corte seccion delgada
  translate([90,0,espesor*3])
  cube([corte,corte,corte]);
   
   //corte seccion en angulo
  translate([90,0,espesor*3])
  rotate([0,-44,0])
  cube([corte,corte,corte]);
   
   //corte manija
   translate([55,17,0])       
       minkowski(){
         cube([30,0.01,0.1],center=true);
         cylinder(d=10,grosorCaliper*4,center=true);
       }
       
    //corte tornillo (ahorro soporte)
       
       //triangulo punta mini
translate([51,lonY-16.5,-grosorCaliper])
  linear_extrude(height=grosorCaliper*3)
    minkowski(){
      polygon([[0,0],[0,5],[5,0]]);
      circle(d=6);
    }

}

//frono de caliper con semicirculo
lonX1=20;
translate([lonX-lonX1,47.25,(espesor*2)])
difference(){
    translate([0,-lonX1/2,0])
    cube([lonX1,lonX1,espesor]);
    cylinder(d=lonX1,h=espesor*3,center=true);
}

//triangulo punta grande
translate([30,0,0])
  linear_extrude(height=grosorCaliper+espesor)
    polygon([[0,0],[0,28],[-6,0]]);


//triangulo punta mini
translate([16.75,lonY,0])
  linear_extrude(height=grosorCaliper+espesor)
    polygon([[0,0],[0,-8.77],[-3.5,0]]);

//base para adherencia cama
translate([-espesor,0,0])
minkowski(){
  cube([0.01,lonY,grosorCaliper]);
  rotate([0,90,0])
  cylinder(r=espesor,h=espesor);
}

translate([lonX/2,lonY/2,-espesor*1.25])
rotate(-90)
linear_extrude(height=espesor)
Idea(escala=0.5,mini=0);
