
include<./../CASEgamma/CASEgamma.scad>;
include<./../Componentes/boards.scad>;
include<./../Componentes/sensores.scad>;
include<./../Componentes/motores.scad>;
include<./../Utilidades/Utilities.scad>;

$fn=200;


module piezaBombilla(){

alto2=82.07;
radio2=37.018/2;
alto1=19.42;
radio1=31.71/2;
espesor=2;
  radioTornillos=0.8;
    
    lon=(alto1+alto2)*2;

   
    
difference(){
    union(){
        

        translate([0,0,alto2/2])
         rondana (ancho=alto2,radioInterno=radio2,radioExterno=radio2+espesor);
          
        //rondana (ancho=espesor,radioInterno=radio1,radioExterno=radio2+espesor);
        
        translate([0,0,-alto1/2])
         rondana (ancho=alto1,radioInterno=radio1,radioExterno=radio1+espesor);
    
        
         rotate_extrude($fn = 200)
        translate([radio2,0])
        rotate(90)
        polygon([[0,0],[espesor,0],[0,radio2-radio1]]);
        
         rotate_extrude($fn = 200)
        translate([radio1+espesor,0])
        rotate(-90)
        polygon([[0,0],[espesor,0],[0,radio2-radio1]]);
        
        
        }
//tornillos
    
    translate([0,radio2+(espesor/2),alto2/4])
    rotate([0,90,0])
    cylinder(r=radioTornillos,h=radio2*2,center=true);
    
    
   translate([0,radio2+(espesor/2),alto2*(3/4)])
    rotate([0,90,0])
    cylinder(r=radioTornillos,h=radio2*2,center=true);
    
    mirror([0,1,0]){
        
    translate([0,radio2+(espesor/2),alto2/4])
    rotate([0,90,0])
    cylinder(r=radioTornillos,h=radio2*2,center=true);
    
    
   translate([0,radio2+(espesor/2),alto2*(3/4)])
    rotate([0,90,0])
    cylinder(r=radioTornillos,h=radio2*2,center=true);
    
        
        }
        
        translate([-lon/2,0,0])
cube([lon,lon,lon],center=true);

}

}


piezaBombilla();

