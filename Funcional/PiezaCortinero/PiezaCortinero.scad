module cortinero(
diamEje=36,espesor=10){


difference(){

//cubo a esculpir
translate([-diamEje/2-espesor,-diamEje/4,-(diamEje+(2*espesor))/2])
cube([diamEje+(espesor*3),diamEje/2,diamEje+(2*espesor)]);  



//recorte poste cortina
    rotate([90,0,0])
    cylinder(d=diamEje,h=20,$fn=100,center=true);

//recorte
translate([-diamEje/2-espesor,-diamEje,0])
cube([diamEje+(espesor*2),diamEje*2,diamEje+10]); 


//recorte tornillo
translate([0,0,10])
 rotate([0,90,0])
    cylinder(r=6/2,h=100*2,$fn=100,center=true);
    
}

translate([-diamEje/2-espesor,-diamEje/4,0])
cube([espesor,diamEje/2,espesor/2]);

}

module taquete(diamTaq=6.53,diamTor=4.5,div=7,alturaTaq=20.7,tol=1){


    difference(){
        for(i=[0:div-1])
            translate([0,0,(alturaTaq*i)/div])
        cylinder(d1=diamTaq,d2=diamTor+tol,h=alturaTaq/div);
        cylinder(d=diamTor,h=alturaTaq*2,center=true);
    }

}

module Camisa(){
    
    difference(){
        
        
        
     cylinder(d=36+16,h=20,center=true); 
        
     cylinder(d=36,h=1000,center=true);   
        
        rotate([90,0,0])
        cylinder(d=5,h=100);
        
        
        for(a=[0:4]){
            translate([0,-20,-2*a])
             rotate([90,90,0])
            cylinder(d=10,h=4,$fn=6);
        }
        
        
        
        
    }
}


//####RENDERIZADOS####

//CORTINERO PABLO
//cortinero(diamEje=36,espesor=10);

//
Camisa();
//cortinero(diamEje=12,espesor=12);

$fn=150;

//taquete(diamTaq=6.53,diamTor=4.5,div=7,alturaTaq=20.7,tol=1);

//taquete(diamTaq=7.5,diamTor=4.5,div=7,alturaTaq=15.5,tol=1);
