# Curtain piece / Pieza cortinero

![](./PNG/cortinero.png)


## Summary/Descripción

Special piece for curtain, the piece is designed to be specific, but you can edit the code to adapt it as the case may be


Pieza especial para cortinero, la pieza está diseñada para ser específica, pero se puede editar el código para adaptarla según sea el caso

