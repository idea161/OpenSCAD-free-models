

module apoyo(espejo=1){

ancho1=30;
espesor=4;
Poste=70;
tol=1;


 if(espejo==1){
    mirror([0,1,0]){
      difference(){
            union(){
                cube([ancho1,espesor,Poste-(ancho1/2)]);
                
                translate([ancho1/2,0,Poste-(ancho1/2)])
                rotate([-90,0,0])
                cylinder(d=ancho1,h=espesor);
                
                cube([ancho1,56,espesor]);

                translate([0,56,0])
                rotate(-45)
                cube([ancho1,64,espesor]);

                translate([0,56,0])
                rotate(-45)
                translate([ancho1/2,64,0])
                cylinder(d=ancho1,h=espesor*2);
            }
         
                translate([0,56,0])
                rotate(-45)
                translate([ancho1/2,64,0])
                cylinder(d=ancho1-(espesor*2),h=(espesor+12)*4,center=true);
            
            //tornillo1
            translate([ancho1/2,0,(ancho1/2)])
            rotate([90,0,0])
            cylinder(d=3.5,h=espesor*4,center=true);
            
            //tornillo2
            translate([ancho1/2,0,Poste-(ancho1/2)])
            rotate([90,0,0])
            cylinder(d=3.5,h=espesor*4,center=true);
          
        }
    }

   }else{
             difference(){
            union(){
                cube([ancho1,espesor,Poste-(ancho1/2)]);
                
                translate([ancho1/2,0,Poste-(ancho1/2)])
                rotate([-90,0,0])
                cylinder(d=ancho1,h=espesor);
                
                cube([ancho1,56,espesor]);

                translate([0,56,0])
                rotate(-45)
                cube([ancho1,64,espesor]);

                translate([0,56,0])
                rotate(-45)
                translate([ancho1/2,64,0])
                cylinder(d=ancho1,h=espesor*2);
            }
         
                translate([0,56,0])
                rotate(-45)
                translate([ancho1/2,64,0])
                cylinder(d=ancho1-(espesor*2),h=(espesor+12)*4,center=true);
            
            //tornillo1
            translate([ancho1/2,0,(ancho1/2)])
            rotate([90,0,0])
            cylinder(d=3.5,h=espesor*4,center=true);
            
            //tornillo2
            translate([ancho1/2,0,Poste-(ancho1/2)])
            rotate([90,0,0])
            cylinder(d=3.5,h=espesor*4,center=true);
          
        }
       
       
   }
}

module pasador(){
   cylinder(d=ancho1,h=espesor);
    
       cylinder(d=ancho1-(espesor*2)-(2*tol),h=(espesor)*6);
}

//####RENDERIZADOS####

$fn=100;

apoyo(espejo=1);

//pasador();