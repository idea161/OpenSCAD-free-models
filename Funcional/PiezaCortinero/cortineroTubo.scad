






module apoyo(pieza=0){

ancho1=20;
    ancho2=30;
espesor=4;


largo=40;
largo1=50;
tol=1;
apoyoTor=15;


   if(pieza==0){
             difference(){
            union(){
              
                translate([-espesor-apoyoTor,0,0])
                cube([ancho1+(2*espesor)+(apoyoTor*2),espesor,espesor*4]);
                
                cube([ancho1,largo,espesor]);

                translate([0,largo,0])
                rotate(-45)
                cube([ancho1,largo1,espesor]);

                 //cubiertapoyo
                 translate([ancho1/2,0,0])
               
                cylinder(d=ancho1+(2*tol)+(2*espesor),h=espesor*4);
                
               //cubierta pasador
                translate([0,largo,0])
                rotate(-45)
                translate([ancho1/2,largo1,0])
                cylinder(d=ancho2+(2*tol)+(2*espesor),h=espesor*2);
            }
         
            //cilindro taladro pasador
              translate([0,largo,0])
                rotate(-45)
                translate([ancho1/2,largo1,0])
                cylinder(d=ancho2+(2*tol),h=(espesor+12)*4,center=true);
            
            //cilindro taladro apoyo
            translate([(ancho1/2),0,0])
             cylinder(d=ancho1+(2*tol),h=(espesor+12)*4,center=true);
            
            
            //corte mitad
            translate([0,-40,0])
            cube([80,80,80],center=true);
            
              //tornillo1
            translate([(ancho1)+(apoyoTor/2)+espesor,0,espesor*2])
            rotate([90,0,0])
            cylinder(d=3.8,h=espesor*4,center=true);
            
            //tornillo2
            translate([-apoyoTor+espesor,0,espesor*2])
            rotate([90,0,0])
            cylinder(d=3.8,h=espesor*4,center=true);
            
            
          
        }
    }else if(pieza==1){
        mirror([0,1,0]){
           difference(){
            union(){
              
                translate([-espesor-apoyoTor,0,0])
                cube([ancho1+(2*espesor)+(apoyoTor*2),espesor,espesor*4]);
                
               
                 //cubiertapoyo
                 translate([ancho1/2,0,0])
               
                cylinder(d=ancho1+(2*tol)+(2*espesor),h=espesor*4);
                
             
            }
         
       
            //cilindro taladro apoyo
            translate([(ancho1/2),0,0])
             cylinder(d=ancho1+(2*tol),h=(espesor+12)*4,center=true);
            
            
            //corte mitad
            translate([0,-40,0])
            cube([80,80,80],center=true);
            
              //tornillo1
            translate([(ancho1)+(apoyoTor/2)+espesor,0,espesor*2])
            rotate([90,0,0])
            cylinder(d=3.8,h=espesor*4,center=true);
            
            //tornillo2
            translate([-apoyoTor+espesor,0,espesor*2])
            rotate([90,0,0])
            cylinder(d=3.8,h=espesor*4,center=true);
            
            
          
        }
      }
    }
}

module pasador(ancho1=20,espesor=4,tol=1){


   cylinder(d=ancho1+(espesor*2),h=espesor);
    
       cylinder(d=ancho1-(2*tol),h=(espesor)*6);
}

//####RENDERIZADOS####

$fn=100;
ancho1=20;
espesor=4;


largo=40;
largo1=50;
tol=1;
apoyoTor=15;

!
mirror([0,1,0]){
  apoyo(pieza=0);
}

apoyo(pieza=1);
translate([0,largo,-espesor])
                rotate(-45)
                translate([ancho1/2,largo1,0])
pasador(ancho1=30,espesor=4,tol=1);