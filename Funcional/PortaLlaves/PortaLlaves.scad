
//LOGOS
include<./../../Logos/LogoIdea161.scad>;
include<./../../Logos/LogoDitacRegSCorch.scad>;

include<./../../Logos/StarWars.scad>;

include<./../../Bibliotecas/Utilidades/Utilities.scad>;


module topeLlaveHangen(diamExt=15,diamInt=6.5,altura=2.5){

    difference(){
        hull(){
            cylinder(d=diamExt,h=altura,center=true);

            translate([diamExt*1.5,0,0])
            cylinder(d=diamExt*0.75,h=altura,center=true);
        }
        
       cylinder(d=diamInt,h=altura*2,center=true);
     
          translate([diamExt*1.5,0,0])
            cylinder(d=diamInt*0.75,h=altura*2,center=true);
        
    }
}



module Base(distLlaves=85,diametro=20,lonMin=3){
      //base
    hull(){
        
        translate([-distLlaves/2,0,0])
        /*difference(){
        sphere(d=diametro);
        translate([0,0,-diametro/2])
            cube([diametro,diametro,diametro],center=true);
        }*/
        cylinder(d=diametro,h=lonMin);

        translate([distLlaves/2,0,0])
        /*difference(){
        sphere(d=diametro);
        translate([0,0,-diametro/2])
            cube([diametro,diametro,diametro],center=true);
        }*/
        cylinder(d=diametro,h=lonMin);

    }
    
    
}

module TapaPorta(distLlaves=85,diametro=18,lonMin=3,altTornillo=4,espesor=1.5,diamMenor=12){
    
    hull(){
              Base(distLlaves=distLlaves,diametro=diametro,lonMin=3);
        
        translate([0,0,altTornillo+espesor])
        Base(distLlaves=distLlaves,diametro=8,lonMin=0.01);
        }
        
         //refuerzo tornillos 1
        translate([distLlaves/2,0,0])
        linear_extrude(height=5.5)
        union(){
            intersection(){
              resize([diametro,diametro*1.2])
              circle(d=diametro);
              translate([diametro,0])
              square([diametro*2,diametro],center=true);
            }
            
            intersection(){
              resize([diametro,diametro*1])
              circle(d=diametro);
              translate([-diametro,0])
              square([diametro*2,diametro],center=true);
           }
        }
 
         //refuerzo tornillos 2
        translate([-distLlaves/2,0,0])
        linear_extrude(height=5.5)
        union(){
            intersection(){
              resize([diametro,diametro*1.2])
              circle(d=diametro);
              translate([-diametro,0])
              square([diametro*2,diametro],center=true);
            }
            
            intersection(){
              resize([diametro,diametro*1])
              circle(d=diametro);
              translate([diametro,0])
              square([diametro*2,diametro],center=true);
           }
        } 
}




module topeLlave(altura=2,diamExt=16,diamInt=6.5){
    difference(){
    
   cylinder(d=diamExt,h=altura,center=true);
    cylinder(d=diamInt,h=altura*2,center=true);
   
}
    
}

module Llavero(
distLlaves=85,
alturaTapas=9.5,
logo=1,
texto1="Pablo VC",
texto2="IDEA 1.61",
toL=0.5,tamaNio=6.5,
){

    
    //22 de enero se redujeron diametro mayor y diametro menor en 2 mm
diametro=18;
    diamMenor=14;
    diamTornillo=9;
    tol=1;
    diamTuerca=11.5;
    
espesor=1.5;    
    
    //alturaTapas=9.5;
   altTornillo=4;
    altTuerca=6;
    

redonMink=1;
lonX=distLlaves+diametro;
lonY=diametro;
lonZ=espesor*2;
    lonMin=0.01;
      factorSoportes=0.6;

difference(){

union(){

//tapa tornillo
translate([0,0,alturaTapas/2])
difference(){
    
         
        TapaPorta(distLlaves=distLlaves,diametro=diametro,lonMin=lonMin,altTornillo=altTornillo,espesor=espesor,diamMenor=diamMenor);
 
    //agujero 1
      translate([-distLlaves/2,0,0])
    cylinder(d=6.5,h=espesor*2,center=true);
    //agujero 1 (tornillo)
    translate([-distLlaves/2,0,espesor])
    cylinder(d=diamTornillo+tol,h=altTornillo*2);
    
    //agujero 2
    //cylinder(d=6.5,h=espesor*2,center=true);
    
      //agujero 3
     translate([distLlaves/2,0,0])
    cylinder(d=6.5,h=espesor*2,center=true);
     //agujero 3 (tornillo)
    translate([distLlaves/2,0,espesor])
    cylinder(d=diamTornillo+tol,h=altTornillo*2);
    
}

//cubo union (puente)
rotate(9)
cube([distLlaves-(diametro*2)-(espesor*8),espesor*2,alturaTapas+espesor*2],center=true);



//tapa tuerca
rotate([180,0,0])
translate([0,0,alturaTapas/2])
difference(){

   //union(){    
       
       TapaPorta(distLlaves=distLlaves,diametro=diametro,lonMin=lonMin,altTornillo=altTornillo,espesor=espesor,diamMenor=diamMenor);
       
    //agujero 1
      translate([-distLlaves/2,0,0])
    cylinder(d=6.5,h=espesor*2,center=true);
    //agujero 1 (tuerca)
    translate([-distLlaves/2,0,espesor])
      rotate(90)
    cylinder(d=diamTuerca+toL,h=altTornillo*2,$fn=6);
    
   
      //agujero 2
     translate([distLlaves/2,0,0])
    cylinder(d=6.5,h=espesor*2,center=true);
     //agujero 2 (tuerca)
    translate([distLlaves/2,0,espesor])
    rotate(90)
    cylinder(d=diamTuerca+toL,h=altTornillo*2,$fn=6);
    
}

}//fin union



//}//fin diffrence 

//LOGO


//logo de Idea
if(logo==1){
  
    //translate([0,2.5,0])
    //rotate([0,0,90])
  translate([2,0,0])  
 translate([-7,0,(alturaTapas/2)+altTornillo])
rotate(90)
 linear_extrude(height=espesor*1.5)
       Idea(escala=0.2,mini=1);
    //Idea(escala=0.1,mini=1);
}

//logo leo
if(logo==2){
translate([0,0.5,(alturaTapas/2)+altTornillo+espesor/2])
rotate(90)
//resize([diamMenor,diamMenor/,espesor*1.5])
scale([1,1,2])
  

//import("./../Logos/STL/skulltrooper(1).stl");
import("./../../Logos/STL/leosigno.stl");
}

//logo spartan
if(logo==3){
translate([0,0.5,(alturaTapas/2)+altTornillo+espesor/2])
rotate(90)
//resize([diamMenor,diamMenor/,espesor*1.5])
scale([1.2,1.2,1.6])
  

//import("./../Logos/STL/skulltrooper(1).stl");
import("./../../Logos/STL/spartan.stl");
}

if(logo==4){
translate([0,0.5-1,(alturaTapas/2)+altTornillo+espesor/2+0.7])
rotate(180)
//resize([diamMenor,diamMenor/,espesor*1.5])
scale([0.9,0.9,1.25])
  

    difference(){
        cube([19,15.5,1], center=true);
import("./../../Logos/STL/LogoPedroINK4.stl");
        translate([0,21.37,0])
        cube([30,30,30,],center=true);
    }
}

//logo bike chido
if(logo==5){
translate([0,-3.5,(alturaTapas/2)+altTornillo+espesor/2])
rotate(180)

scale([1.3,1.3,1.5])
  

//import("./../Logos/STL/skulltrooper(1).stl");
import("./../../Logos/STL/bikechido.stl");
}

//Alien
if(logo==6){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2])
rotate(90)
 scale([0.8,1,1.4])
       import("./../../Logos/STL/alienLOGO.stl");
}

if(logo==7){
    
 
     translate([0,0,(alturaTapas/2)+altTornillo])
rotate(90)
 linear_extrude(height=espesor*1.5)
    Imperial(radio=10);
}

//PACMAN
if(logo==8){
   rotate([0,0,180])
   union(){ 
     translate([-distLlaves/4,0,(alturaTapas/2)+altTornillo+espesor/2])
     scale([0.8,0.8,1.4])
           import("./../../Logos/STL/pacman.stl");
        
         translate([-distLlaves/8,0,(alturaTapas/2)+altTornillo])
    rotate(90)
     linear_extrude(height=espesor*1.5)
        circle(d=3);
        
        translate([0,0,(alturaTapas/2)+altTornillo])
    rotate(90)
     linear_extrude(height=espesor*1.5)
        circle(d=3);
        
        translate([distLlaves/8,0,(alturaTapas/2)+altTornillo])
    rotate(90)
     linear_extrude(height=espesor*1.5)
        circle(d=3);
        
         translate([distLlaves/4,0,(alturaTapas/2)+altTornillo+espesor/2])
     scale([0.7,0.7,1.4])
           import("./../../Logos/STL/pacmanGhost.stl");
       }
}

//UFO
if(logo==9){
    
 translate([0,4,(alturaTapas/2)+altTornillo+espesor/2])
rotate(180)
 scale([2,2,1.7])
       import("./../../Logos/STL/UFO.stl");
}

if(logo==10){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2])
rotate(90)
 scale([0.8,0.8,1.4])
       import("./../../Logos/STL/bobafett.stl");
}

if(logo==11){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2])
 scale([0.95,0.95,1.5])
       import("./../../Logos/STL/pacman.stl");
}

if(logo==12){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2])
rotate([0,0,180-45])
 scale([2.2,2.2,1.5])
       import("./../../Logos/STL/DNA.stl");
    
}



if(logo==13){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2])
rotate(90)
 scale([1.2,1.2,1.55])
       import("./../../Logos/STL/heladoA.stl");
}

if(logo==14){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2])
rotate(90)
 scale([0.9,0.9,1.55])
       import("./../../Logos/STL/punisher.stl");
}





if(logo==15){
    
 translate([0,-2.5,(alturaTapas/2)+altTornillo+espesor/2])
//rotate(90)
 scale([4,2.1,1.4])
       import("./../../Logos/STL/artic_monkeys.stl");
}

if(logo==16){
    
 translate([0,-1.2,(alturaTapas/2)+4.5])
rotate([0,0,180])
 scale([1,1,6.4])
       import("./../../Logos/STL/Deathly.stl");
}

if(logo==17){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2])
rotate(90)
 scale([0.85,0.85,1.5])
       import("./../../Logos/STL/AlienX.stl");
}

if(logo==18){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2])
rotate(90)
 scale([1.05,1.05,1.5])
       import("./../../Logos/STL/ghost.stl");

}

if(logo==19){
    
 translate([0,0.6,(alturaTapas/2)+altTornillo+espesor/2])
rotate(90)
 scale([0.9,0.9,1.5])
       import("./../../Logos/STL/dogPAWperro.stl");

}


if(logo==20){
    
 translate([0,-0.1,(alturaTapas/2)+(altTornillo)+(espesor/2)-4.75])
rotate(180)
 scale([1.25,1.25,6])
       import("./../../Logos/STL/huesos.stl");

}


if(logo==21){
    
 translate([1.8,2.2,(alturaTapas/2)+altTornillo+espesor/2])
rotate(180)
 scale([3.4,3.4,1.5])
       import("./../../Logos/STL/AvatarStripe.stl");

}


if(logo==22){
    
 translate([0,0,(alturaTapas/2)+4.5])
rotate(180)
 scale([2.5,2.5,6])
       import("./../../Logos/STL/AlasAngelRTM.stl");

}

if(logo==23){
    
 translate([0,0.6,(alturaTapas/2)+altTornillo+espesor/2])
rotate(90)
 scale([1,1,1.5])
       import("./../../Logos/STL/swastika.stl");

}


if(logo==24){
    
 translate([0,0.2,(alturaTapas/2)+altTornillo+espesor/2])
rotate(90)
 scale([1.17,1.17,1.5])
       import("./../../Logos/STL/spaflower.stl");
    
}

if(logo==25){
    
 translate([0,0.2,(alturaTapas/2)+altTornillo+espesor/2])
rotate(180)
 scale([1.12,1.12,1.5])
       import("./../../Logos/STL/gatito.stl");
   
}

if(logo==26){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2])
rotate(180)
 scale([0.99,0.99,1.5])
       import("./../../Logos/STL/Rollerskate.stl");
    
}



if(logo==27){
    
// translate([0,0,(alturaTapas/2)+altTornillo+espesor/2])
//rotate(180)
 //scale([0.99,0.99,1.5])
  //     import("./../../Logos/STL/Rollerskate.stl");
   
   rotate([180,180,0])
translate([-23-1,0,(alturaTapas/2)+altTornillo])
linear_extrude(height=(espesor*1.5)+0.7)
 text(size=9,font = "Simplex:style=Bold","C",halign ="center",valign="center");

   rotate([180,180,0])
translate([-23+15,0,(alturaTapas/2)+altTornillo])
linear_extrude(height=(espesor*1.5)+0.7)
 text(size=7.5,font = "Simplex:style=Bold","B",halign ="center",valign="center");

  rotate([180,180,0])
translate([-23+(15*2),0,(alturaTapas/2)+altTornillo])
linear_extrude(height=(espesor*1.5)+0.7)
 text(size=6.5,font = "Simplex:style=Bold","J",halign ="center",valign="center");
    
    
  rotate([180,180,0])
translate([-23+(15*3)-0.5,0,(alturaTapas/2)+altTornillo])
linear_extrude(height=(espesor*1.5)+0.7)
 text(size=5.5,font = "Simplex:style=Bold","K",halign ="center",valign="center");




//color("grey")
  rotate([180,180,0])
    for(i=[0:3]){
translate([-23+(i*15),0,(alturaTapas/2)+altTornillo])
    cylinder(d=13-(i*1.75),h=2.25);
    }
}

if(logo==28){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2])
rotate(180)
 scale([1.35,1.35,1.5])
       import("./../../Logos/STL/ChicagoBears.stl");
    
}

if(logo==29){
    
    
     translate([16,0,(alturaTapas/2)+altTornillo+espesor/2])
rotate(180)
 scale([0.69,0.69,1.5])
       import("./../../Logos/STL/marioB.stl");
    

    
     translate([-16,0,(alturaTapas/2)+altTornillo+espesor/2])
rotate(180)
 scale([0.85,0.85,1.5])
       import("./../../Logos/STL/HongoMario.stl");
    

}

if(logo==30){
    
 translate([18,0,(alturaTapas/2)+altTornillo+espesor/2])
rotate(180)
 scale([0.8,0.8,1.5])
       import("./../../Logos/STL/swiss.stl");
    
}

if(logo== 31){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2])
rotate(180)
 scale([1.6,1.6,1.55])
       import("./../../Logos/STL/batman icon.stl");
    
}

if(logo== 32){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2+2])
rotate(180)
 scale([0.0435,0.0435,0.3])
       import("./../../Logos/STL/IEEE.stl");
    
}

if(logo== 33){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2])
rotate(180)
 scale([1.6,1.6,1.55])
       import("./../../Logos/STL/ultimateFrisbee.stl");
    
}

if(logo== 34){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2])
rotate(180)
 scale([1.6,1.6,1.55])
       import("./../../Logos/STL/ultimateFris.stl");
    
}

if(logo== 35){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2])
rotate(180)
 scale([1.6,1.6,1.55])
       import("./../../Logos/STL/ekpeek.stl");
    
}

if(logo== 36){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2-1])
rotate(180)
 scale([1.6,1.6,4])
       import("./../../Logos/STL/
nubesRTM.stl");
    
}

if(logo==37){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2-1])
rotate(180)
 scale([2.2,2.2,4])
       import("./../../Logos/STL/
patycantu.stl");
    
    rotate([180,0,0])
    translate([0,0,(alturaTapas/2)+altTornillo+espesor/2-1])
 scale([1.1,1.1,2])
       import("./../../Logos/STL/
333.stl");
    
}

if(logo==38){
    
 translate([0,0.2,(alturaTapas/2)+altTornillo+espesor/2])
rotate(90)
 scale([1.17,1.17,1.5])
       import("./../../Logos/STL/spaflower.stl");
    
    
     translate([-20,0.2,(alturaTapas/2)+altTornillo+espesor/2])
    rotate([0,0,20])
rotate(90)
    
 scale([1.17,1.17,1.5])
       import("./../../Logos/STL/spaflower.stl");
    
    translate([20,0.2,(alturaTapas/2)+altTornillo+espesor/2])
       rotate([0,0,-20])
rotate(90)
 scale([1.17,1.17,1.5])
       import("./../../Logos/STL/spaflower.stl");
    
}

if(logo== 39){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2-1])
rotate(180)
 scale([1.6,1.6,4])
       import("./../../Logos/STL/UFC.stl");
    
}

if(logo== 40){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2-1])
rotate(180)
 scale([1.6,1.6,4])
       import("./../../Logos/STL/DavidLogo2.stl");
    
}

if(logo== 41){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2-1])
rotate(180)
 scale([1,1,4])
       import("./../../Logos/STL/LEGO.stl");
    
}

if(logo== 42){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2-1])
rotate(180)
 scale([1,1,4])
       import("./../../Logos/STL/quimi.stl");
    
}


if(logo== 43){
    
 translate([0,0,(alturaTapas/2)+altTornillo+espesor/2-1])
rotate(180)
 scale([1,1,4])
       import("./../../Logos/STL/RebelAlliance.stl");
    
}


if(logo== 44){
    
 translate([-2,-2,(alturaTapas/2)+altTornillo+espesor/2-1])
rotate(180)
 scale([1.4,1.4,4])
       import("./../../Logos/STL/AguilaJuarista.stl");
    
}






//}//fin diffrence 




//#####TEXTOS#####

rotate([180,0,0])
translate([0,0,(alturaTapas/2)+altTornillo])
linear_extrude(height=espesor*1.5)
 //text(size=tamaNio,font = "Simplex:style=Bold",texto2,halign ="center",valign="center");
text(size=tamaNio,texto2,halign ="center",valign="center");
//URW Gothic L:style=Bold
rotate([180,180,0])
translate([0,0,(alturaTapas/2)+altTornillo])
linear_extrude(height=espesor*1.5)
 //text(size=tamaNio,font = "Simplex:style=Bold",texto1,halign ="center",valign="center");
 text(size=tamaNio,texto1,halign ="center",valign="center");

//text(size=10,font = "Accanthis ADF Std No3:style=Bold",texto,halign ="center",valign="center");

}//fin diffrence 


}//fin llavero

//######RENDERIZADOS#####

//####CALCULO DE LLAVES####

//9.5-> 4 espacios (8 llaves)
//2 espacio es 2.375
//1 espacio es 1.1875

//10.62 -> 9 espacios (18 llaves)


//2 predeterminado
//"extra " 3
//topeLlave(altura=1);

//LISTA DE LOGOS
//0-> nada
//1-> idea
//2 -> leo
//3 -> spartan
//4 -> logo MediPlus
//5 -> bike
//6 -> alien
//7 -> imperio
//8 -> pacman con fantasma
//9 -> UFO
//10 -> bobafett
//11 -> pacman
//12 -> DNA
//13 -> heladoA
//14 -> punisher
//15 -> artic_monkeys
//16 -> deathly hollows
//17 -> AlienX
//18 -> ghost
//19 -> perro
//20-> huesosPistons
//21-> avatar Stripe
//22-> alas Angel
//23->swastika
//24->flor 5 petalos
//25->gatito (Belem)
//26->Rollerskate
//27->CustomCircles
//28->ChicagoBears

//29->Mario scene
//30-> Cruz suiza
//31-> Batman
//32-> IEEE
//33-> ultimate frisbee
//34-> ultimate frisbee 2
//38->3 flores 5 petalos
//39->UFC
//40->DavidSanchezJuanchi
//41->LEGO
//42->quimi
//43->rebel alliance
//44->aguilaJuarista


//tamaNIO estandarizado 1 llave es 2.3 antes del 15/01/18

//tamaNIO estandarizado 1 llave es 2.2 despuEs del 15/01/18

$fn=120;



/*rotate([-90,0,0])
Llavero(distLlaves=85,
alturaTapas=2.3*5,
logo=0,
texto1="G.N.        J.F.",
texto2="SUBAGENTE",
toL=0.5,
tamaNio=8.5
);*/

Llavero(distLlaves=85,
alturaTapas=2.3*5,
logo=7,
texto1="",
texto2="Pablo VC",
toL=0.5,
tamaNio=10
);


//tamaNio default 12
//distLlaves=85
//distLlaves=65
//tamanio 9

//LOGO EXTRA
/*
alturaTapas=9.5;
rotate([90,0,0])
 translate([0,0.06,(alturaTapas/2)+1])
rotate([0,0,90])
 scale([1,1,6.4])
       import("./../../Logos/STL/Deathly.stl");
*/

//texto minimio 6.5

//TAMANIO 10 ->2.3*5
//TAMANIO 18 ->2.2*9


//topeLlave(altura=2.5,diamExt=15,diamInt=6.5);

//topeLlaveHangen(diamExt=15,diamInt=6.5,altura=2.5);
