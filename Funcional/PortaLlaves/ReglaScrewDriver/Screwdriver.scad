
$fn=100;

difference(){
    //intersection(){
      //rotate([0,0,90])
    //import("C:/Users/pablo/Downloads/Small_Hex_Screwdriver_Handle/Small_Hex_Screwdriver_Handle/Hex_Handle_rotated_for_printing.STL");
        //translate([-0.4,0,20])
    
    lonz=20;
    mink=5;
    translate([0,0,lonz/2+mink])
        minkowski(){
        cube([0.01,10,20],center=true);
          rotate([90,0,0])
          cylinder(r=mink,h=0.01);
      }
    //}
    translate([0,0,mink])
     rotate([90,0,0])
  cylinder(d=6.5,h=100,center=true);
    
    rotate([0,0,90])
   translate([0,0,2*mink+4])
    cylinder(d=7.6,h=100,$fn=6);
      /*
      translate([0,10+1.5,0])
      cube([20,20,25],center=true);
      
      translate([0,-10-1.5,0])
      cube([20,20,25],center=true);
      */
}



//Regla(med=0);

module Regla(med=0){
    minkR=7.5;
translate([minkR,6,-3])
rotate([90,-90,0])
    difference(){
    linear_extrude(height=3)
        union(){
            translate([minkR,minkR])
            difference(){
                minkowski(){
                    square([0.01,4]);
                    circle(r=minkR);
                }
                circle(d=6.5);
            }
            //square([12,10]);
            //translate([6,10])
            //circle(r=6);
            translate([minkR,13])
            square([60,6]);
        }
        if(med==0){
            translate([minkR,17,6])
            for(i=[0:5]){
              translate([10*i,0,0])
              cube([0.5,6,10],center=true);
            }
            
            translate([minkR,20.5,6])
            for(i=[0:30]){
              translate([2*i,0,0])
              cube([0.5,6,10],center=true);
            }
           }else if (med==1){
             camb=25.4;
               
                translate([minkR,17,6])
            for(i=[0:2]){
              translate([camb*i,0,0])
              cube([0.5,6,10],center=true);
            }
            
             translate([minkR,20.5,6])
            for(i=[0:9]){
              translate([(camb/4)*i,0,0])
              cube([0.5,6,10],center=true);
            }
            
             translate([minkR,19,6])
            for(i=[0:3]){
              translate([(camb/2)*i,0,0])
              cube([0.5,6,10],center=true);
            }
       }
    }
}//fin regla