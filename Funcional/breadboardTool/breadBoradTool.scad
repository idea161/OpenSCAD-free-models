

module breadboardTool(escala=4){
distPinaPin=2.5;
unidad=8;


difference(){     
     union(){
         //parte baja
         for(i=[1:escala-1]){
             
         translate([unidad*i,0,0])
         cube([unidad,distPinaPin*i,3.8]);
             
         }
         
         //parte gruesa
         translate([-4,-distPinaPin/2,0])
          for(i=[0:escala]){
             
         translate([unidad*i,0,0])
         cube([unidad*0.7,distPinaPin*i*3,5]);
             
         }
     }     
 translate([0,distPinaPin/2,-unidad*1.5])
 rotate(17.5)
 cube([unidad*(escala*2),unidad*escala,unidad*3]);
 }
 
 translate([unidad*(0.7+(0.7/4)),1.1,unidad*0.5])
 for(i=[0:(escala-1)]){
     translate([unidad*i,0,0])
     linear_extrude(height=1.5)
   text(size=2.8,font = "Simplex:style=Bold",text = str(i),halign ="center",valign="center");
 
 }
 
 }//fin module
 
 //#####RENDERIZADOS#####
 
 breadboardTool(escala=16);
 
 
// translate([0,40,0])
 //import("/home/pablovc/Descargas/files/breadboard_wire_tool.stl");