







module tomaTuerca(interior=7,grueso=5){
   
   difference(){
       cylinder(r=interior,h=grueso,center=true);
    translate([interior/2,0,0])
    cube([interior,interior,grueso*2],center=true);
       
       translate([-interior*((1/4)-(1/16)),0,0])
       intersection(){
           translate([interior,0,0])
        cylinder(r=interior,h=grueso*2,center=true);
    translate([interior/2,0,0])
           
           // translate([interior/2,0,0])
    cube([interior,interior,grueso*2],center=true);
       
           
       }
   }
}


//distancia
$fn=100;
grueso=5;
distancia=88;
angulo=30;
tol=1;
mink=2.499999999999999;

//translate([0,0,-1])

minkowski(){
cube ([distancia-15,10-2*(mink),grueso-(2*mink)],center=true);
sphere(r=mink);
}


translate([distancia/2,0,0])
rotate(angulo)
tomaTuerca(interior=7+tol,grueso=grueso);

translate([-distancia/2,0,0])
rotate(180+angulo)
tomaTuerca(interior=8+tol,grueso=grueso);