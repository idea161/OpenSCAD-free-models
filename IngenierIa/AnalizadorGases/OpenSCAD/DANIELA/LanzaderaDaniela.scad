$fn=100;

difference(){
    intersection(){
        
            translate([-20,0,0])
        cylinder(d=50,h=40,center=true);
        
        translate([20,0,0])
        cylinder(d=50,h=40,center=true);
        
            rotate([0,90,0])
            intersection(){
            
                translate([-20,0,0])
            cylinder(d=50,h=40,center=true);
            
            translate([20,0,0])
            cylinder(d=50,h=40,center=true);
        }
    }
    
    
    minkowski(){
        
        cube([0.1,10,40],center=true);
        cylinder(d=4);
    }
    
    
    
    
}

rotate([0,90,0])
    cylinder(d=5,h=9,center=true);


//doblar punta con extrusion lineal desviada
