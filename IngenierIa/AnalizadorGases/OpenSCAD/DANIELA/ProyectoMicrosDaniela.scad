//Extractor de gases de propósito general


include<modulos.scad>;

espe=3;

lonX=200;
lonY=100;
lonZ=60;




base(largo=lonX-(2*espe),ancho=lonY-(2*espe),espesor=espe,dientes=8,Up=1,Down=1,Left=1,Right=1);



cube([lonX,lonY,lonZ],center=true);

//ventilador
color("gray")
translate([lonX/2,0,0])
rotate([0,90,0])
cylinder(d=60,h=20,center=true);

//motor a pasos
color("blue")
translate([-lonX/2,0,lonZ/2])
rotate([0,-90,0])
cylinder(d=30,h=20,center=true);

//sensores de gases y de temperatura
color("green")
translate([0,0,lonZ/2])
cylinder(d=30,h=20,center=true);
