//modulos para el renderizado de varios tipos de caja a manufacturear en la máquina de corte lAser

//Largo se desplaza en "X"
//Ancho se desplaza en "Y"

module base(largo=80,ancho=80,espesor=3,dientes=20,Up=1,Down=1,Left=1,Right=1){
tamDientesLargo=(largo)/dientes;
tamDientesAncho=(ancho)/dientes;


union(){

//Dientes derecha
  if(Right==1){
    translate([largo/2,-ancho/2-tamDientesAncho/2])
    for(i=[1:dientes/2]){
       translate([0,tamDientesAncho*2*i])
       square([2*espesor,tamDientesAncho],center=true);
    }
}

//Dientes Izquierda

if(Left==1){
   translate([-largo/2,-ancho/2-3*tamDientesAncho/2])
   for(i=[1:dientes/2]){
      translate([0,tamDientesAncho*2*i])
      square([2*espesor,tamDientesAncho],center=true);
   }
}

//Dientes Arriba

if(Up==1){
   translate([-largo/2-3*tamDientesLargo/2,ancho/2])
   for(i=[1:dientes/2]){
     translate([tamDientesLargo*2*i,0])
     square([tamDientesLargo,2*espesor],center=true);
   }
}

//Dientes Abajo

if(Down==1){
   translate([-largo/2-tamDientesLargo/2,-ancho/2])
    for(i=[1:dientes/2]){
     translate([tamDientesLargo*2*i,0])
     square([tamDientesLargo,2*espesor],center=true);
    }
}

//Cuadrado
square([largo,ancho],center=true);
}

}//Fin module base
