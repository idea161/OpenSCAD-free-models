include<./../../Bibliotecas/CASES/CASEgamma/CASEgamma.scad>;
include<./../../Bibliotecas/Utilidades/Utilities.scad>;
include<./../../Bibliotecas/Mecanica/OpenSCAD/EngraneBanda.scad>;
include<./../../Bibliotecas/Mecanica/OpenSCAD/piezas2D.scad>;
include<./../../Bibliotecas/Componentes/motores.scad>;
include<./../../Bibliotecas/Componentes/boards.scad>;



module apoyoMotor(){
	
	espesor=2;
anchoMontura=42;
altoMontura=16;
sepTornillos=22;
anchoPCB=40;
tol=1;
sepTornillosFondo=13.25;

	translate([0,-5,(anchoMontura/4)+(altoMontura/4)+1.5-0.1])
rotate([-90,0,0])
difference(){
union(){
		cylinder(d=anchoMontura,h=espesor);
		translate([0,(altoMontura/2)+(2*espesor),espesor/2])
		cube([anchoMontura,altoMontura*1.5,espesor],center=true);

		angulo=50+90; //ayuda a crear los agujeros

		rotate(270)
		union(){
			//recorte tornillo 1
			translate([17,0,0])
			cylinder(d=3.5+1.5, h=5);
			//recorte tornillo 2
			rotate(angulo)
			translate([17,0,0])
			cylinder(d=3.5+1.5, h=5);
		}
	}

rotate(270)
translate([0,0,espesor*2])
motorNamiki(modo=1,info=false);

//ajuste parte baja
translate([0,(anchoMontura/2)+(altoMontura/4)+(espesor/2),0])
		cube([anchoMontura*2,altoMontura,espesor*4],center=true);

}

difference(){
	translate([-(anchoPCB+2)/2,-5,-espesor*2])
	cube([anchoPCB+2,anchoPCB*0.7,espesor]);
//tornillo1
	translate([sepTornillos/2,sepTornillosFondo,0])
	cylinder(d=3,h=30,center=true);

//tornillo3
	translate([-sepTornillos/2,sepTornillosFondo,0])
	cylinder(d=3,h=30,center=true);

}
	
	alturaBase=100;
claroBase=110;
reduccion=0.25;

volApoyo=5;
dimApoyo=42;

	translate([dimApoyo/2,0,-4])
	rotate([90,90,-90])
	linear_extrude(height=dimApoyo)
	polygon([[0,0],[volApoyo,0],[0,volApoyo]]);

	difference(){
		hull(){
				//translate([-(anchoPCB+2)/2,-5,-espesor])
			translate([-(anchoPCB+2)/2,0,-espesor])
			cube([anchoPCB+2,anchoPCB*0.7,0.1]);
			
			translate([-claroBase/2,0,-alturaBase])
			cube([claroBase,anchoPCB*0.7,0.1]);
		}
		
		//
	translate([0,0,-alturaBase/2])
		//scale([1,4,1])
	rotate([90,0,0])
			minkowski(){
		   cube([0.1,40,alturaBase],center=true);
		  cylinder(d=22,h=0.1,center=true);
		}
	//	cylinder(d1=40,d2=20,h=90,$fn=4);
		
		//recorte par a tornillos abrazadera
		translate([0,(anchoPCB+2)/4+(espesor*1.5),-(alturaBase/2)-0.1])
		minkowski(){
		   cube([22,0.1,alturaBase],center=true);
		  cylinder(d=16,h=0.1,center=true);
		}
		
		//recortador apoyo A
		translate([anchoPCB,(anchoPCB+2)/4+(espesor*1.5),-(alturaBase/2)+espesor*2])
		cube([anchoPCB,anchoPCB/2,alturaBase],center=true);
		
		
		//recortador apoyo B
		translate([-anchoPCB,(anchoPCB+2)/4+(espesor*1.5),-(alturaBase/2)+espesor*2])
		cube([anchoPCB,anchoPCB/2,alturaBase],center=true);
		
		//recortadorTornillo 1
		translate([-anchoPCB+10,(anchoPCB+2)/4+(espesor*1.5),0])
		 cylinder(d=5,h=alturaBase*4,center=true);
		
		//recortadorTornillo 2
		translate([-anchoPCB-5,(anchoPCB+2)/4+(espesor*1.5),0])
		 cylinder(d=5,h=alturaBase*4,center=true);
		
		//recortadorTornillo 3
		translate([anchoPCB+5,(anchoPCB+2)/4+(espesor*1.5),0])
		 cylinder(d=5,h=alturaBase*4,center=true);
		 
		 //recortadorTornillo 4
		translate([anchoPCB-10,(anchoPCB+2)/4+(espesor*1.5),0])
		 cylinder(d=5,h=alturaBase*4,center=true);
	}
}


module BaseBalero(){

		espesor=2;
	anchoMontura=42;
	altoMontura=16;
	sepTornillos=22;
	anchoPCB=40;
	tol=1;
	sepTornillosFondo=13.25;
	espesorBalero=7;

		translate([0,-anchoBase/2,(anchoMontura/4)+(altoMontura/4)+1.5-0.1])
	rotate([-90,0,0])
	difference(){
		union(){
			cylinder(d=anchoMontura,h=espesorBalero);
			translate([0,altoMontura*0.75,espesorBalero/2])
			cube([anchoMontura,altoMontura*2,espesorBalero],center=true);

			angulo=50+90; //ayuda a crear los agujeros
		
		}

	//agujero Balero
	translate([0,0,espesor*2])
	cylinder(d=22,h=40,center=true);


	}

		alturaBase=100;
	claroBase=110;
	reduccion=0.25;
	anchoBase=15;

		difference(){
			hull(){
				translate([-(anchoPCB+2)/2,-anchoBase/2,-espesor])
				cube([anchoPCB+2,anchoBase,0.1]);
				
				translate([-claroBase/2,-anchoBase/2,-alturaBase])
				cube([claroBase,anchoBase,0.1]);
			}
			
			
		translate([0,0,-alturaBase/2])

		rotate([90,0,0])
				minkowski(){
			   cube([0.1,40,alturaBase],center=true);
			  cylinder(d=22,h=0.1,center=true);
			}
		
			espesorBase=1.5;
			
			
			//recortador apoyo A
			translate([anchoPCB,0,-(alturaBase/2)+espesor*2])
			cube([anchoPCB,anchoBase-(2*espesorBase),alturaBase],center=true);
			
			
			
			//recortador apoyo B
			translate([-anchoPCB,0,-(alturaBase/2)+espesor*2])
			cube([anchoPCB,anchoBase-(2*espesorBase),alturaBase],center=true);
			
			
			
			//recortadorTornillo 1
			translate([-anchoPCB+10,0,0])
			 cylinder(d=5,h=alturaBase*4,center=true);
			
			//recortadorTornillo 2
			translate([-anchoPCB-5,0,0])
			 cylinder(d=5,h=alturaBase*4,center=true);
			
			//recortadorTornillo 3
			translate([anchoPCB+5,0,0])
			 cylinder(d=5,h=alturaBase*4,center=true);
			 
			 //recortadorTornillo 4
			translate([anchoPCB-10,0,0])
			 cylinder(d=5,h=alturaBase*4,center=true);
		

		}
	}

module perfilBarra(alturaBarra=20){

	
		espesorBarra=2.6;

	difference(){
		//2 mm agregados posteriormente, revisar malfuncionamiento
	   cube([18.75,15.25+2,alturaBarra]);
		translate([espesorBarra+2.4,espesorBarra,-alturaBarra])
	   cube([18.75,15.25,alturaBarra*2]);
		translate([3.5,0,0])
		scale([1,2,1])
		rotate(360/16)
		cylinder(d=3,h=alturaBarra,cneter=true,$fn=8);
		translate([0,5,-alturaBarra])
		cube([espesorBarra,15.75,alturaBarra*2]);
	}
	
}

module abrazaderaEje(tol=1,prisionero=true){

difference(){
   cylinder(d=41,h=35);
	translate([40,0,0])
	cube([80,80,80],center=true);
	
	cylinder(d=4,h=35/2);

	translate([0,0,35/2])
	cylinder(d=8,h=35/2);

//cabeza tuerca tornillo balero
    /rotate(360/6)
    translate([0,0,(35/2)-(5.6/2)])
    cylinder(d=14.6+(2*tol),h=5.6+(2*tol),$fn=6);
	
	//tornillo 1
	translate([0,14,35/2])
	rotate([0,90,0])
	cylinder(d=4+tol,h=40,center=true);
	
	//tornillo 2
		translate([0,-14,35/2])
	rotate([0,90,0])
	cylinder(d=4+tol,h=40,center=true);
	
	//agujero tuerca 1
		translate([-5,-14,35/2])
	rotate([0,-90,0])
	rotate(360/12)
	cylinder(d=10,h=40,$fn=6);
	
	//agujero tuerca 2
	translate([-5,14,35/2])
	rotate([0,-90,0])
	rotate(360/12)
	cylinder(d=10,h=40,$fn=6);
	
	
	
	if(prisionero==true){
		
		//recorte tornillo
		translate([0,0,5])
	rotate([0,-90,0])
	cylinder(d=2+tol,h=40);
		
	//cubo tuerca
	translate([-1.6-5,-5/2,0])
	cube([1.6+tol,5+tol,8]);
	
	//recorte para cabeza Tornillo
		translate([-10,0,5])
	rotate([0,-90,0])
	cylinder(d=5,h=40);
	}
	
	
}


}

module cuartoSujetador(){

	tol=1;
	alturaXtor=6;

	difference(){
		translate([0,0,-35/2])
	abrazaderaEje(tol=1,prisionero=false);
		translate([0,25,0])
		
		//recortador para hacer un cuarto de cilindro
	  cube([50,50,100],center=true);
		
			//tornillo 
		translate([-14,0,(35/2)-alturaXtor])
		rotate([0,90,90])
		cylinder(d=4+tol,h=40,center=true);
		
			translate([-14,-5,(35/2)-alturaXtor])
		rotate([0,-90,90])
		rotate(360/12)
		cylinder(d=10,h=40,$fn=6);
		
		
	}

	dimSuj=18;
	translate([-22,0,0])
	rotate([270,0,0])
	rotate([0,0,45+90])
	translate([0,0,-dimSuj/2])
	difference(){
		translate([-5,-5,0])
		cube([20,20,dimSuj/2]);
		perfilBarra(alturaBarra=dimSuj);
		
		translate([5,2.5,0])
		cube([30,30,30]);
		

		}


	}
	
	
module monturaSensor(){

	dimSuj=18;
		espesor=1.5;


	difference(){	
		union(){
			translate([-18.4,0,-espesor/2])
			rotate(45)
			cube([36,36,espesor],center=true);
			
			translate([-18.5,50.91/2,-espesor])
			rotate(180)
			//minimo dimension en X es 26 mm
			cube([76,50.91,espesor]);
		}

	trasladoX=56;
		
		//agujero Tornillo 1
	translate([-trasladoX,35.5/2,0])
	minkowski(){
		cube([65,0.1,espesor*2],center=true);
		 cylinder(d=4,h=2*espesor,center=true);
	}

	//agujero Tornillo 2
	translate([-trasladoX,-35.5/2,0])
	minkowski(){
		cube([65,0.1,espesor*2],center=true);
		 cylinder(d=4,h=2*espesor,center=true);
	}

	//agujero Cables
	translate([-trasladoX,0,0])
	minkowski(){
		cube([60,0.1,espesor*2],center=true);
		 cylinder(d=8,h=2*espesor,center=true);
	}

	//recorte para perfil
	translate([0,0,-dimSuj/2])
	rotate([0,0,45+90])
	perfilBarra(alturaBarra=dimSuj);

	//agujero complemento corte para perfil
	//translate([-7,1.6,0])
	//cube([3.5,6.75,espesor*2],center=true);
	
	//agujero complemento corte para perfil
	translate([0,0,-espesor])
	rotate(45+90)
	//cube([25,25,espesor*2]);
	
	linear_extrude(height=2*espesor)
	difference(){
	    polygon([[2.5,0],[25,0],[2.5,25]]);
	  square([espesor*3.25,espesor*3.25]);
		}
	}
		rotate([0,0,45+90])
		difference(){
			translate([-5,-5,0])
			cube([20,20,dimSuj/2]);
			perfilBarra(alturaBarra=dimSuj);
			
			translate([5,2.5,0])
			cube([30,30,30]);
			

			}
		}
		
		

//####RENDERIZADOS####

$fn=10;
	
	
alturaAbrazadera=35;
//translate([-55,0,70])
//sphere(d=74);

//monturaSensor();

//apoyoMotor();
	
//BaseBalero();

//translate([0,0,-alturaAbrazadera/2])
//rotate(180)
abrazaderaEje(tol=1,prisionero=true);


//original
//cuartoSujetador();
	
	//copia
	//mirror([0,1,0]){
	//	cuartoSujetador();
		
	//}