distX=26.64;
distY=29.73;
diam=3;
tol=1;
altura=1.6;
espesor=1.5;

placa=0.6;
$fn=100;

   module Poste(altura=10,espesor=3,tol=1,diamShaft=2){
	   
	   difference(){
	      cylinder(d=(espesor*2)+diamShaft+tol,h=altura);
	   
		  cylinder(d=diamShaft+tol,h=altura*4,center=true);
	      
	   }
   }


translate([0,0,(placa/2)])
   difference(){
       cube([distX+diam+tol+(espesor*2),distY+diam+tol+(espesor*2),placa],center=true);
       
	   
   //poste 1
   translate([distX/2,distY/2,0])
	   cylinder(d=diam+tol,h=40,center=true);
	   
	   
   //poste 2
   translate([-distX/2,-distY/2,0])
	   cylinder(d=diam+tol,h=40,center=true);
   
	   
   //poste 3
   translate([-distX/2,distY/2,0])
	   cylinder(d=diam+tol,h=40,center=true);
	   
	   
   //poste 4
   translate([distX/2,-distY/2,0])
	   cylinder(d=diam+tol,h=40,center=true);
	   
	   }
	   
   //poste 1
   translate([distX/2,distY/2,0])
   Poste(altura=altura,espesor=espesor,tol=tol,diamShaft=diam);
   
   
   //poste 2
   translate([-distX/2,-distY/2,0])
   Poste(altura=altura,espesor=espesor,tol=tol,diamShaft=diam);
   
   
   //poste 3
   translate([-distX/2,distY/2,0])
   Poste(altura=altura,espesor=espesor,tol=tol,diamShaft=diam);
   
   //poste 4
   translate([distX/2,-distY/2,0])
   Poste(altura=altura,espesor=espesor,tol=tol,diamShaft=diam);