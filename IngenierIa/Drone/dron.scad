distanciaPostes=17;
lonPlano=43;
tol=0.5;
espesor=3;

module anclaje(tol=0.5){
    difference(){
     cylinder(d=8.5,h=5.5);
     cylinder(d=4+tol,h=20,center=true);
        
         translate([0,20,0])
    cube([40,40,40],center=true);
    }
    
}



translate([0,distanciaPostes/2,0])
cylinder(d1=2.8-tol,d2=2.4,h=4);
translate([0,distanciaPostes/2,0])
cylinder(d=5,h=0.6);
translate([0,distanciaPostes/2,0])
rotate(180)
anclaje(tol=0.5);

ajuste=2.3;

translate([ajuste,distanciaPostes/2,0])
cube([lonPlano-ajuste,espesor,espesor]);


translate([0,-distanciaPostes/2,0])
cylinder(d1=2.8-tol,d2=2.4,h=4);
translate([0,-distanciaPostes/2,0])
cylinder(d=5,h=0.6);
translate([0,-distanciaPostes/2,0])
anclaje(tol=0.5);

translate([ajuste,-distanciaPostes/2-espesor,0])
cube([lonPlano-ajuste,espesor,espesor]);


translate([15,-distanciaPostes/2,0])
cube([espesor,distanciaPostes,espesor]);

ajusteLateral=11.5;
    
translate([lonPlano,0,20.36])
scale([1.1,1,1])
difference(){
    rotate([90,0,0])
    cylinder(r=20.36,h=40,center=true);
    
     rotate([90,0,0])
    cylinder(r=20.36-espesor,h=40*2,center=true);

translate([0,0,25,])
cube([50,50,50],center=true);
    
    
translate([-25,0,0,])
cube([50,50,50],center=true);
    
    
    translate([0,25+ajusteLateral,0,])
cube([50,50,50],center=true);
    
    
    translate([0,-25-ajusteLateral,0,])
cube([50,50,50],center=true);
   
   ajuste45=11;
   
     translate([0,-25,0,])
     rotate([45,0,0])
     translate([0,0,ajuste45])
cube([50,50,50],center=true);
   
    translate([0,25,0,])
     rotate([45,0,0])
     translate([0,ajuste45,0])
cube([50,50,50],center=true);
   
  for(i=[0:2]){
  
       translate([0,0,5*i])
  translate([0,0,-20.36])
  rotate([0,90,0])
  rotate(360/16)
  cylinder(d=distanciaPostes+(espesor/2),h=60,center=true,$fn=8);
  
  }
      
  /*  translate([0,0,-20.36+10])
  rotate([0,90,0])
  rotate(360/16)
  cylinder(d=distanciaPostes+(espesor/2),h=60,center=true,$fn=8);
  */
  
    
}


module piezaCubo(){
    ajusteAngulo=1.72;
    tol=0.5;
    translate([(24.17/2),-ajusteAngulo+0.02,-ajusteAngulo])
    difference(){
    rotate([0,8,-8])
    translate([-40/2,0,0])
    cube([80,7.4,4.8]);
    
        translate([30+(24.17/2),0,0])
        cube([60,60,60],center=true);
        
        translate([-30-(24.17/2),0,0])
        cube([60,60,60],center=true);
        }
        
        translate([0,0,4.8/2])
        rotate([0,-90,0])
        for(i=[0:1]){
         translate([0,5.43*i,0])
        cylinder(d=2.5-tol,h=6);
        }
        
        translate([-2,0,2.5])
        rotate([90,0,90])
        linear_extrude(height=2)
        polygon([[0,-2.5],[9,0],[0,2.5]]);
        
        translate([-2,0,2.5])
        rotate([90,0,90])
        linear_extrude(height=2)
        polygon([[0,-2.5],[-1,0],[0,2.5]]);
        
        
}

$fn=100;
!piezaCubo();

