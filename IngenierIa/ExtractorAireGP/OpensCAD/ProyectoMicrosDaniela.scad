
use<modulos.scad>;



//Extractor de gases de propósito general

espe=3;

lonX=200;
lonY=100;
lonZ=80;


!union(){
    difference(){
base(largo=lonX-(2*espe),ancho=lonY-(2*espe),espesor=espe,dientes=10,Up=1,Down=1,Left=0,Right=1);
        
        
         translate([-25,0])
        square([15,25],center=true);
        translate([25,0])
        circle(d=19.39);
    }
    
    
    translate([0,lonY])
    base(largo=lonX-(2*espe),ancho=lonY-(2*espe),espesor=espe,dientes=10,Up=1,Down=1,Left=0,Right=1);
    
    //cara de agujero para recepciOn de gases
    translate([0,lonZ*2.5])
    difference(){
       base(largo=lonX-(2*espe),ancho=lonZ-(2*espe),espesor=espe,dientes=10,Up=1,Down=1,Left=0,Right=1);
        translate([(lonX/2)-20,(lonZ/2)-20])
        circle(d=6);
    }
    translate([0,lonZ*3.5])
    base(largo=lonX-(2*espe),ancho=lonZ-(2*espe),espesor=espe,dientes=10,Up=1,Down=1,Left=0,Right=1);

translate([200,0])
    difference(){
       base(largo=lonZ-(2*espe),ancho=lonY-(2*espe),espesor=espe,dientes=10,Up=1,Down=1,Left=1,Right=1);
        circle(d=60);
        for(i=[0:3]){
           translate([50,0])
          rotate(45*i)  
           circle(d=6);
        }
        
    }
}




cube([lonX,lonY,lonZ],center=true);

//ventilador
color("gray")
translate([lonX/2,0,0])
rotate([0,90,0])
cylinder(d=60,h=20,center=true);

//motor a pasos
color("blue")
translate([-lonX/2,0,lonZ/2])
rotate([0,-90,0])
cylinder(d=30,h=20,center=true);

//sensores de gases y de temperatura
color("green")
translate([0,0,lonZ/2])
cylinder(d=30,h=20,center=true);
