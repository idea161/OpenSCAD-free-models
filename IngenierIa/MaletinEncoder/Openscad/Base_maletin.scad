

distX=350;
distY=252;
dist_offset_min=10;
dist_offset=12.5;
diam_Tor=6;

/*
difference(){
square([420,280],center=true);
    
    translate([((distX/2)+dist_offset),((distY/2)+dist_offset_min)])
    circle(d=diam_Tor);
    
    translate([-((distX/2)+dist_offset),-((distY/2)+dist_offset_min)])
    circle(d=diam_Tor);
    
    translate([-((distX/2)+dist_offset),((distY/2)+dist_offset_min)])
    circle(d=diam_Tor);
    
    translate([((distX/2)+dist_offset),-((distY/2)+dist_offset_min)])
    circle(d=diam_Tor);
}
*/

$fn=60;

diamEncoder=35.7;
medEje=15.5;
espesor=2;


difference(){
   cylinder(d=diamEncoder+(2*espesor),h=30,center=true);
   cylinder(d=diamEncoder,h=60,center=true);
    translate([0,diamEncoder,-medEje+1.5])
   cube([medEje,diamEncoder,medEje],center=true); 
}