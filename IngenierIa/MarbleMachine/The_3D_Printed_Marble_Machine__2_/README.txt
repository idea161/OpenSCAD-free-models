                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:795952
The 3D Printed Marble Machine #2  by Tulio is licensed under the Attribution - Non-Commercial - Share Alike license.
http://creativecommons.org/licenses/by-nc-sa/3.0/

# Summary

The 3D Printed Marble Machine #2 - Designed by Tulio Laanen  
Video: https://www.youtube.com/watch?v=4uJugZ2lUi4  

- Please consider making a donation if you want to support me and my work.
Donate HERE: http://bit.ly/1GovCl0 

- Check out; The 3D Printed Marble Machine #3 !
http://www.thingiverse.com/thing:1385312

- Check out my first 3D Printed Marble Machine as well:  
Thingiverse: http://www.thingiverse.com/thing:615203  
Video: https://www.youtube.com/watch?v=0wrA-zyLiBg  

-  Contact mail: tuliolaanen@gmail.com  
-  My website: http://www.tuliolaanen.com

Made one yourself? Feel free to upload your photo’s!

# Instructions

Its very simple!  

-  I recommend slicing with with Cura: https://ultimaker.com/en/software/list  

- Optimal settings:  
Layer height: 0.1 (0.2 optional for faster print)  
Shell thickness: 0.8  
Bottom/Top thickness: 0.6  
Fill Density: 25%  
Print speed: 40 mm/s  
No Support, brim or raft needed.  

- use 9.5 - 10mm ball-bearings for Marbles  
   
- And print!  

(I Printed on Ultimaker 2)  
.  
.  
Explanation different download files:  


- R_The_3D_Marble_Machine_2_Final_Model.stl  
Original file. Works the best with given instructions above, which means slicing with Cura.  

- repaird version:  
Fixed version, if your not using cura, or if you're experiencing problems while slicing.  
   
- The four parts in separate STL files:  
Cap,  Turning wheel,  Lift,  Track  

- and there is a version of the lift (spiral) without the support cilinder around it if you prefer to print it with a brim or a raft