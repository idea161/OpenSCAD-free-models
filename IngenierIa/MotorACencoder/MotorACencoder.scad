
module Tope(){

diamInt=6.5;
diamExt=9;
espesor=6;
altTope=1.5;

    difference(){
     union(){
         //cuerpo
      cylinder(d=diamExt,h=espesor+altTope);
         //tope
         cylinder(d=diamExt+(2*altTope),h=altTope);
     }
      cylinder(d=diamInt,h=espesor*3,center=true);  
    }

}


module apoyo(){
    
    
diamInt=6.5;
diamExt=9;
espesor=1.5;
//altTope=1.5;


    difference(){

         cylinder(d=diamExt+6,h=espesor);
     
      cylinder(d=diamInt,h=espesor*3,center=true);  
    }

    
}




//####RENDERIZADO####


//dim Apoyo

diamInt=6.5;
diamExt=9;
//espesor=1.5;

//


distX=70;

distY=60;
tol=1;

radioApoyo=sqrt(pow(distX,2)+pow(distY,2))/2;
angulo=atan(distX/distY);
echo(angulo);
espesor=3;

spcMotorToEncoder=20;
dimApMot=12;
minkApMot=2.5;


distMotorEncoder=36.5;

corteCentro=75;

//montura a ENCODER


module recCilindro(diametro=65){
    translate([0,0,diametro])
    difference(){
       cube([diametro*2,diametro*2,diametro*2],center=true);
        cylinder(d=diametro,h=diametro*3,center=true);
    }
}



translate([0,0,distMotorEncoder])
difference(){

union(){
    //cuerpo redondo
       cylinder(d=encoderSTD,h=espesor);
    
 //poste en angulo 1
        rotate(angulo)
        translate([0,radioApoyo/2,espesor/2])
        cube([15,radioApoyo,espesor],center=true);

        //poste en angulo 2
        rotate(-angulo)
         translate([0,radioApoyo/2,espesor/2])
        cube([15,radioApoyo,espesor],center=true);
   
        //poste en angulo 3
        rotate(angulo)
        translate([0,-radioApoyo/2,espesor/2])
        cube([15,radioApoyo,espesor],center=true);

        //poste en angulo 4
        rotate(-angulo)
        translate([0,-radioApoyo/2,espesor/2])
        cube([15,radioApoyo,espesor],center=true);

}



    //corte shaft
   cylinder(d=20+tol,h=espesor*2,center=true);
    
    for(i=[0:2]){
       rotate(120*i)
        //centro de shaft a tornillo
       translate([0,14.87])    
       cylinder(d=espesor+tol,h=2*espesor,center=true);
    }
    
    //corte acopla escpacio centro
    
     //recCilindro(diametro=corteCentro+(2*espesor));
    recCilindro(diametro=corteCentro+(espesor));
}

//translate([0,0,distMotorEncoder])


difference(){
    union(){        
        //cuerpo de abrazadera 
      // cylinder(d=encoderSTD+(2*espesor)+tol,h=encoderSTD+spcMotorToEncoder);

        //poste en angulo 1
        rotate(angulo)
        translate([0,radioApoyo/2,espesor/2])
        cube([15,radioApoyo,espesor],center=true);
         
        //poste en angulo 2
        rotate(-angulo)
        translate([0,radioApoyo/2,espesor/2])
        cube([15,radioApoyo,espesor],center=true);
   

        //poste en angulo 3
        rotate(angulo)
        translate([0,-radioApoyo/2,espesor/2])
        cube([15,radioApoyo,espesor],center=true);


        //poste en angulo 4
        rotate(-angulo)
        translate([0,-radioApoyo/2,espesor/2])
        cube([15,radioApoyo,espesor],center=true);
   

        
        //Cilindros apoyos
            //apoyo (1,1)
    translate([distX/2,distY/2,0])
    cylinder(d=diamExt+6,h=espesor);

            //apoyo (-1,-1)
    translate([-distX/2,-distY/2,0])
    cylinder(d=diamExt+6,h=espesor);

    //apoyo (-1,1)
    translate([-distX/2,distY/2,0])
    cylinder(d=diamExt+6,h=espesor);
    
    //apoyo (1,-1)
    translate([distX/2,-distY/2,0])
    cylinder(d=diamExt+6,h=espesor);

    }
 //espacio para encoder 
//cylinder(d=encoderSTD+tol,h=encoderSTD*10,center=true);
   
  //Cilindros apoyos
            //apoyo (1,1)
    translate([distX/2,distY/2,0])
    cylinder(d=diamInt,h=espesor*3,center=true);  
    
      //apoyo (-1,-1)
    translate([-distX/2,-distY/2,0])
    cylinder(d=diamInt,h=espesor*3,center=true);  

    //apoyo (-1,1)
    translate([-distX/2,distY/2,0])
      cylinder(d=diamInt,h=espesor*3,center=true);  

    //apoyo (1,-1)
    translate([distX/2,-distY/2,0])
      cylinder(d=diamInt,h=espesor*3,center=true);  
    
    //corte espacio centro
     cylinder(d=corteCentro+(2*espesor),h=espesor);

}



module posteCilindro(distMotorEncoder=36.5,corteCentro=75,espesor=3){
    
 //distMotorEncoder=36.5;

  difference(){
    cylinder(d=corteCentro+(2*espesor*1.5),h=distMotorEncoder+espesor);
    cylinder(d=corteCentro,h=(distMotorEncoder+espesor)*2,center=true);
  }
    
}

//!posteCilindro();

intersection(){
  posteCilindro(distMotorEncoder=36.5,corteCentro=75,espesor=espesor);
          //poste en angulo 1
        rotate(angulo)
        translate([0,radioApoyo,(espesor/2)+(distMotorEncoder/2)])
        cube([15,radioApoyo*2,distMotorEncoder+espesor],center=true);

}

intersection(){
  posteCilindro();
      //poste en angulo 2
        rotate(-angulo)
        translate([0,radioApoyo,(espesor/2)+(distMotorEncoder/2)])
        cube([15,radioApoyo*2,distMotorEncoder+espesor],center=true);
}

intersection(){
   posteCilindro();
   //poste en angulo 3
        rotate(angulo)
        translate([0,-radioApoyo,(espesor/2)+(distMotorEncoder/2)])
        cube([15,radioApoyo*2,distMotorEncoder+espesor],center=true);
}

intersection(){
  posteCilindro();
        //poste en angulo 4
        rotate(-angulo)
        translate([0,-radioApoyo,(espesor/2)+(distMotorEncoder/2)])
        cube([15,radioApoyo*2,distMotorEncoder+espesor],center=true);  
}


//apoyo (-1,-1)
//translate([-distX/2,-distY/2,0])
//apoyo();



//apoyo (1,-1)
//translate([distX/2,-distY/2,0])
//apoyo();

encoderSTD=40;

$fn=100;

//COPLE

//copleEncMot();

module copleEncMot(){
difference(){

    cylinder(d=6+(2*espesor),h=(9.77*2)+7.7+9.7);
        union(){
            translate([0,0,9.77])
            difference(){
              cylinder(d=6.18+tol,h=9.77*2,center=true);
                translate([10+(1.17*1.5),0,0])
              cube([20,20,20],center=true);
            }
            cylinder(d=6.18+tol,h=9.77);
            
            translate([0,0,(9.77*2)-tol])
            cylinder(d=6+(tol/2),h=7.7+(2*tol));
            //fin
            translate([0,0,(9.77*2)+7.7])
            cylinder(d=8+(tol/2),h=9.7*2);
            
        }


    }

}

/*
difference(){
    
cylinder(d=encoderSTD+(2*espesor)+tol,h=encoderSTD);

cylinder(d=encoderSTD+tol,h=encoderSTD*2,center=true);
    
}
*/






