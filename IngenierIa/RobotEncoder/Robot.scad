include<./../../Bibliotecas/CASES/CASEgamma/CASEgamma.scad>;
include<./../../Bibliotecas/Utilidades/Utilities.scad>;
include<./../../Bibliotecas/Mecanica/OpenSCAD/EngraneBanda.scad>;
include<./../../Bibliotecas/Mecanica/OpenSCAD/piezas2D.scad>;
include<./../../Bibliotecas/Componentes/motores.scad>;
include<./../../Bibliotecas/Componentes/boards.scad>;

 module eslabon(A=100,diametroExt=10,diametro1=4,diametro2=5){
 difference(){
 union(){
 translate([0,-diametroExt/2])
 square([A,diametroExt]);
 
 circle(d=diametroExt);
 
 
 translate([A,0])
 circle(d=diametroExt);
     
 }
 translate([A,0])
 circle(d=diametro2);
 
 
 
 circle(d=diametro1);
 }
 }
 
  module eslabonEngrane(diametroEng=4,tol=1,diamHerram=7,A=80){
 union(){

 linear_extrude(height=espesor)
 eslabon(A=A,diametroExt=15,diametro1=diametroEng+tol,diametro2=16);
 
 //engrane a eslabon
    translate([0,0,espesor])
 //TOL 1 funciono para estarflojo
//EngraneBanda(alturaEngrane=5.9,espesor=1,flecha=diametroEng,tol=tol,diamDiente=1.3,diamEngrane=9,dientes=14);
 EngraneBanda(alturaEngrane=6.5,espesor=1,flecha=4,tol=0.75,diamDiente=1.3,diamEngrane=9,dientes=14);

     
     }
 
 //frame lingote
 hull(){
    translate([15/2,-15/2,0])
    cube([A-(15),15,0.1]);
        
    translate([15/2,-15/4,8.5-0.1])
    cube([A-(15),15/2,0.1]);
    }

rotate(-90)
translate([-3.5,4,8.25])
cube([7,6,0.9]);

//apoyo motor
 translate([A,0,0])
     difference(){
     poste(diametroPoste=16+(espesor*10),tornillo=16,alturaTornillo=20);
        //TUERCA
        viajeTuerca=9.5;
        translate([viajeTuerca,0,5])
         rotate([0,90,0])
     cylinder(d=10,$fn=6,h=4);
         translate([viajeTuerca,0,10])
         rotate([0,90,0])
     cylinder(d=10,$fn=6,h=4);
        //EJEtornillo 
        translate([0,0,5])
        rotate([0,90,0])
           cylinder(d=5,h=40);
         }
 
 
 }

//!eslabonEngrane(diametroEng=4,tol=1,diamHerram=7);
 
 module eslabonMotor(espesor=1.5,A=80){
 
 union(){
 linear_extrude(height=espesor)
 eslabon(A=A,diametroExt=15,diametro1=5,diametro2=5);
   
    //frame tipo lingote
    hull(){
    translate([15/4,-15/2,0])
    cube([A-(15/2),15,0.1]);
        
    translate([15/4,-15/4,7-0.1])
    cube([A-(15/2),15/2,0.1]);
    }
    
 //   viajeParedes=6;
    
 //   translate([-3.5,espesor/2-viajeParedes*1.5,0])
 //   cube([7,espesor,alturaEng*3]);
   
 //   translate([0,0,alturaEng*3])
 //    cube([7,viajeParedes*2+espesor*3,espesor],center=true);
    
   //apoyo motor
    rotate(90)
      difference(){
          
          union(){
     poste(diametroPoste=5+(espesor*7),tornillo=5,alturaTornillo=14);
          
      //        rotate(-90)
  //             translate([-3.5,espesor/2+viajeParedes,0])
  //  cube([7,espesor,alturaEng*3]);
    
              }
        //TUERCA
        viajeTuerca=3.5;
        translate([viajeTuerca,0,6])
         rotate([0,90,0])
     cylinder(d=6,$fn=6,h=2.5);
         translate([viajeTuerca,0,4])
         rotate([0,90,0])
     cylinder(d=6,$fn=6,h=2.5);
        //EJEtornillo 
        translate([0,0,4])
        rotate([0,90,0])
           cylinder(d=3.5,h=40);
         }
         
         //apoyo brazo
         translate([A,0,0])
          poste(diametroPoste=5+(espesor*7),tornillo=5,alturaTornillo=14);
         //difference(){
             //cylinder();
             //cylinder();
         //}
     }
 }//fin modulo
 
module EngraneSimple(flecha=4,tol=0.75,alturaEng=8.9,flecha=4){
    //alturaEng=7.9+1;
       translate([0,0,(alturaEng)+espesor+4])
//EngraneBanda(alturaEngrane=5.9,espesor=1,flecha=flecha,tol=tol,diamDiente=1.3,diamEngrane=9,dientes=14);
 EngraneBanda(alturaEngrane=6.5,espesor=1,flecha=flecha,tol=0.75,diamDiente=1.3,diamEngrane=9,dientes=14);

    
  //rondana a engrane Simple
   //translate([0,0,0])
 //linear_extrude(height=7.9+4+5)
//rondanas(radioExterno=4,radioInterno=(flecha/2)+(tol/2));  
translate([0,0,6])
poste(diametroPoste=12,tornillo=flecha+tol,alturaTornillo=17);    
    
         rotate([0,0,90])
      difference(){
     poste(diametroPoste=5+16,tornillo=flecha+tol,alturaTornillo=14);
        //TUERCA
        viajeTuerca=6.5;
        translate([viajeTuerca,0,6])
         rotate([0,90,0])
     cylinder(d=6,$fn=6,h=2.5);
         translate([viajeTuerca,0,4])
         rotate([0,90,0])
     cylinder(d=6,$fn=6,h=2.5);
        //EJEtornillo 
        translate([0,0,4])
        rotate([0,90,0])
           cylinder(d=3.5,h=40);
         }

}
 
//!EngraneSimple(flecha=4,tol=0.75,alturaEng=8.9);

module engraneDoble(){
   

 //TOL 1 funciono para estarflojo
//TOL 0.75 funciono para estar apretado
//EngraneBanda(alturaEngrane=5.9,espesor=1,flecha=4,tol=0.75,diamDiente=1.3,diamEngrane=9,dientes=14);
  EngraneBanda(alturaEngrane=6.5,espesor=1,flecha=4,tol=0.75,diamDiente=1.3,diamEngrane=9,dientes=14);

       
    translate([0,0,(alturaEng)-2])
   //TOL 1 funciono para estarflojo
//TOL 0.75 funciono para estar apretado
//EngraneBanda(alturaEngrane=5.9,espesor=1,flecha=4,tol=0.75,diamDiente=1.3,diamEngrane=9,dientes=14);
EngraneBanda(alturaEngrane=6.5,espesor=1,flecha=4,tol=0.75,diamDiente=1.3,diamEngrane=9,dientes=14);


 //rondana a engrane doble
  // translate([0,0,-espesor*2])
 //linear_extrude(height=espesor*2)
//rondanas(radioExterno=4,radioInterno=2+0.5);
 }
 

 

 module upperCase(){
     
      
 flecha=4;
distanciamotores=90;
espesor=1.5; //material a imprimir
diametroapoyomotores=48;
alturamotor=72;
 alturaTornillo=10;
 viajeApoyos=17;
 ajustePoste=1;

//posicion tronillos
 distanciaL298N=37;

 ajusteCentroMega=28;
  ajusteXMega=25;

distanciaYesiX=55;
distanciaYesiZ=49.5;
 
     
 difference(){
    CASEgamma(lonX=diametroapoyomotores+distanciamotores,lonY=diametroapoyomotores,medZ=alturamotor,espesor=espesor,diametroPoste=5,tornillo=2.5,parte=0,minkow=3,soporteA=0,soporteB=0,soporteC=0,soporteD=0);
    translate([-distanciamotores/2,0,alturamotor])

//recortes de motor Namiki
union(){
motorNamiki(modo=1,info=false);
 translate([distanciamotores,0,0])
 motorNamiki(modo=1,info=false);
 }
 
  //RECORTADOR EN EJE "Y"
 rotate([90,0,0])
 translate([0,alturamotor/2,-diametroapoyomotores])
 linear_extrude(height=diametroapoyomotores*2)
 RecortadorEstrellaInverso(lonX=distanciamotores-17,lonY=alturamotor-8,orillas=5,redondeadorPoly=2);
 
 
 
 
 //RECORTADOR EN EJE "X"
 translate([-(diametroapoyomotores+distanciamotores),0,alturamotor/2])
 rotate([0,90,0])
 linear_extrude(height=(diametroapoyomotores+distanciamotores)*2)
 RecortadorEstrellaInverso(lonX=alturamotor-8,lonY=diametroapoyomotores-12,orillas=3,redondeadorPoly=2);
 
 //recortes tornillos

 
 //Back Board
  
   
   //poste (-1,1)
  translate([-(diametroapoyomotores+distanciamotores)/2,distanciaL298N/2,(alturamotor/2)+(distanciaL298N/2)])
   rotate([0,-90,0])
 cylinder(d=2.5+ajustePoste,h=espesor*10,center=true);
  //poste (1,1)
   
   translate([-(diametroapoyomotores+distanciamotores)/2,-distanciaL298N/2,(alturamotor/2)+(distanciaL298N/2)])
   rotate([0,-90,0])
   cylinder(d=2.5+ajustePoste,h=espesor*10,center=true);
  //poste (1,-1)
   
   translate([-(diametroapoyomotores+distanciamotores)/2,-distanciaL298N/2,(alturamotor/2)-(distanciaL298N/2)])
   rotate([0,-90,0])
 cylinder(d=2.5+ajustePoste,h=espesor*10,center=true);
   //poste (-1,-1)
   
   translate([-(diametroapoyomotores+distanciamotores)/2,+distanciaL298N/2,(alturamotor/2)-(distanciaL298N/2)])
   rotate([0,-90,0])
   cylinder(d=2.5+ajustePoste,h=espesor*10,center=true);
 
 
 //RightBoard
 

 //poste (-1,-1)
 translate([15.24-ajusteCentroMega-ajusteXMega,-diametroapoyomotores/2,alturamotor/2+3-ajusteCentroMega])
 rotate([90,0,0])
cylinder(d=2.5+ajustePoste,h=espesor*10,center=true);
 
 //poste (1,-1)
 translate([15.24+81.28-ajusteCentroMega-ajusteXMega,-diametroapoyomotores/2,alturamotor/2+3-ajusteCentroMega])
 rotate([90,0,0])
cylinder(d=2.5+ajustePoste,h=espesor*10,center=true);
 
  //poste (1,1)
 translate([15.24+74.93-ajusteCentroMega-ajusteXMega,-diametroapoyomotores/2,alturamotor/2+(3+4.7+27.9+15.2)-ajusteCentroMega])
 rotate([90,0,0])
  cylinder(d=2.5+ajustePoste,h=espesor*10,center=true);
 
   //poste (-1,1)
 translate([15.24-ajusteCentroMega-ajusteXMega,-diametroapoyomotores/2,alturamotor/2+(3+4.7+27.9+15.2)-ajusteCentroMega])
 rotate([90,0,0])
  cylinder(d=2.5+ajustePoste,h=espesor*10,center=true);
 
  //LeftBoard
 
 
 //distanciaYesiX=55;
//distanciaYesiZ=49.5;

//poste(1,1)
  translate([-(distanciaYesiX/2),diametroapoyomotores/2,alturamotor/2+(distanciaYesiZ/2)])
 rotate([-90,0,0])
  cylinder(d=2.5+ajustePoste,h=espesor*10,center=true);
 
//poste(-1,1)
  translate([(distanciaYesiX/2),diametroapoyomotores/2,alturamotor/2+(distanciaYesiZ/2)])
 rotate([-90,0,0])
  cylinder(d=2.5+ajustePoste,h=espesor*10,center=true);
 
 //poste(-1,-1)
  translate([(distanciaYesiX/2),diametroapoyomotores/2,alturamotor/2-(distanciaYesiZ/2)])
 rotate([-90,0,0])
  cylinder(d=2.5+ajustePoste,h=espesor*10,center=true);
 
  //poste(1,-1)
  translate([-(distanciaYesiX/2),diametroapoyomotores/2,alturamotor/2-(distanciaYesiZ/2)])
 rotate([-90,0,0])
  cylinder(d=2.5+ajustePoste,h=espesor*10,center=true);
 
 
 }//fin difference
 
 //APOYOS PARA MOTOR 1
 
 //poste A
 translate([-distanciamotores/2+viajeApoyos,0,alturamotor-alturaTornillo+(espesor)])
  poste(diametroPoste=5+ajustePoste,tornillo=2.5+ajustePoste,alturaTornillo=alturaTornillo+(espesor*3));
 
 //poste B
  translate([-distanciamotores/2,0,alturamotor-alturaTornillo+(espesor)])
 rotate(140)
 translate([viajeApoyos,0,0])
  poste(diametroPoste=5+ajustePoste,tornillo=2.5+ajustePoste,alturaTornillo=alturaTornillo+(espesor*3));
 
  //APOYOS PARA MOTOR 2
 
 
 //poste A
 translate([distanciamotores/2+viajeApoyos,0,alturamotor-alturaTornillo+(espesor)])
  poste(diametroPoste=5+ajustePoste,tornillo=2.5+ajustePoste,alturaTornillo=alturaTornillo+(espesor*3));
 
 //poste B
  translate([distanciamotores/2,0,alturamotor-alturaTornillo+(espesor)])
 rotate(140)
 translate([viajeApoyos,0,0])
  poste(diametroPoste=5+ajustePoste,tornillo=2.5+ajustePoste,alturaTornillo=alturaTornillo+(espesor*3));
 
    
 alturaAnclajes=espesor+3;
   
   //Back Board
  
   
   //poste (-1,1)
   
   translate([-(diametroapoyomotores+distanciamotores)/2,distanciaL298N/2,(alturamotor/2)+(distanciaL298N/2)])
   rotate([0,-90,0])
   poste(diametroPoste=5+ajustePoste,tornillo=2.5+ajustePoste,alturaTornillo=alturaAnclajes);
 
 //poste (1,1)
   
   translate([-(diametroapoyomotores+distanciamotores)/2,-distanciaL298N/2,(alturamotor/2)+(distanciaL298N/2)])
   rotate([0,-90,0])
   poste(diametroPoste=5+ajustePoste,tornillo=2.5+ajustePoste,alturaTornillo=alturaAnclajes);
 
  //poste (1,-1)
   
   translate([-(diametroapoyomotores+distanciamotores)/2,-distanciaL298N/2,(alturamotor/2)-(distanciaL298N/2)])
   rotate([0,-90,0])
   poste(diametroPoste=5+ajustePoste,tornillo=2.5+ajustePoste,alturaTornillo=alturaAnclajes);
 
   //poste (-1,-1)
   
   translate([-(diametroapoyomotores+distanciamotores)/2,+distanciaL298N/2,(alturamotor/2)-(distanciaL298N/2)])
   rotate([0,-90,0])
   poste(diametroPoste=5+ajustePoste,tornillo=2.5+ajustePoste,alturaTornillo=alturaAnclajes);
 
 
 
 //RightBoard
 

 //poste (-1,-1)
 translate([15.24-ajusteCentroMega-ajusteXMega,-diametroapoyomotores/2,alturamotor/2+3-ajusteCentroMega])
 rotate([90,0,0])
   poste(diametroPoste=5+ajustePoste,tornillo=2.5+ajustePoste,alturaTornillo=alturaAnclajes);
 
 //poste (1,-1)
 translate([15.24+81.28-ajusteCentroMega-ajusteXMega,-diametroapoyomotores/2,alturamotor/2+3-ajusteCentroMega])
 rotate([90,0,0])
   poste(diametroPoste=5+ajustePoste,tornillo=2.5+ajustePoste,alturaTornillo=alturaAnclajes);
 
  //poste (1,1)
 translate([15.24+74.93-ajusteCentroMega-ajusteXMega,-diametroapoyomotores/2,alturamotor/2+(3+4.7+27.9+15.2)-ajusteCentroMega])
 rotate([90,0,0])
   poste(diametroPoste=5+ajustePoste,tornillo=2.5+ajustePoste,alturaTornillo=alturaAnclajes);
 
   //poste (-1,1)
 translate([15.24-ajusteCentroMega-ajusteXMega,-diametroapoyomotores/2,alturamotor/2+(3+4.7+27.9+15.2)-ajusteCentroMega])
 rotate([90,0,0])
   poste(diametroPoste=5+ajustePoste,tornillo=2.5+ajustePoste,alturaTornillo=alturaAnclajes);
 
 //LeftBoard
 
 
 //distanciaYesiX=55;
//distanciaYesiZ=49.5;

//poste(1,1)
  translate([-(distanciaYesiX/2),diametroapoyomotores/2,alturamotor/2+(distanciaYesiZ/2)])
 rotate([-90,0,0])
   poste(diametroPoste=5+ajustePoste,tornillo=2.5+ajustePoste,alturaTornillo=alturaAnclajes);

//poste(-1,1)
  translate([(distanciaYesiX/2),diametroapoyomotores/2,alturamotor/2+(distanciaYesiZ/2)])
 rotate([-90,0,0])
   poste(diametroPoste=5+ajustePoste,tornillo=2.5+ajustePoste,alturaTornillo=alturaAnclajes);
 
 //poste(-1,-1)
  translate([(distanciaYesiX/2),diametroapoyomotores/2,alturamotor/2-(distanciaYesiZ/2)])
 rotate([-90,0,0])
   poste(diametroPoste=5+ajustePoste,tornillo=2.5+ajustePoste,alturaTornillo=alturaAnclajes);
 
  //poste(1,-1)
  translate([-(distanciaYesiX/2),diametroapoyomotores/2,alturamotor/2-(distanciaYesiZ/2)])
 rotate([-90,0,0])
   poste(diametroPoste=5+ajustePoste,tornillo=2.5+ajustePoste,alturaTornillo=alturaAnclajes);
 

 }//fin de case


//Parte trasera CASE

module lowerCase(){
    difference(){
    CASEgamma(lonX=diametroapoyomotores+distanciamotores,lonY=diametroapoyomotores,medZ=5,espesor=espesor,diametroPoste=5,tornillo=2.5,parte=1,minkow=3,soporteA=0,soporteB=0,soporteC=0,soporteD=0);
     //rotate([90,0,0])
     translate([0,0,-diametroapoyomotores])
     linear_extrude(height=diametroapoyomotores*2)
     RecortadorEstrellaInverso(lonX=diametroapoyomotores+distanciamotores,lonY=diametroapoyomotores,orillas=4.25,redondeadorPoly=2);
     

    }

}


 $fn=30;
 
 flecha=4;
distanciamotores=90;
espesor=1.5; //material a imprimir
diametroapoyomotores=48;
alturamotor=72;
 alturaEng=8.5+1;
//A=80;
A=60;
//rotate([90,0,0])
//union(){
color("black")
lowerCase();
 
// color("orange")
//color("green")
color("lime")
 upperCase();

 //engrane solo
 

  //color("pink")
color("lime")
   translate([-distanciamotores/2,0,alturamotor])
 //TOL=1 engrane flojo
//TOL 1 funciono para estarflojo
//TOL 0.75 funciono para estar apretado

EngraneSimple(flecha=4,tol=0.75,alturaEng=alturaEng);
 
 //ENGRANE DOBLE
   //color("pink")
   color("lime")
   translate([distanciamotores/2,0,alturamotor+alturaEng-1])
   
  
   engraneDoble();
 
 
 //eslabon interno
 //color("orange")
color("lime")
 translate([distanciamotores/2, 0, alturamotor])
 
  //!projection()
//rotate([90,0,0])
 eslabonMotor(espesor=1.5,A=A);

 //eslabon mas externo
  //color("pink")
  color("lime")
   translate([distanciamotores/2+A, 0, alturamotor+espesor+6])
 eslabonEngrane(diametroEng=4,tol=1,diamHerram=16,A=A);
//}
  
