//include<EngraneBanda.scad>;

 EngraneBanda(alturaEngrane=6.5,espesor=1,flecha=11.5,tol=0.75,diamDiente=1.3,diamEngrane=14,dientes=20);
 
 
 
 module poste(diametroPoste=5,tornillo=2.5,alturaTornillo=10){
  
    translate([0,0,alturaTornillo/4])
            difference(){

                cylinder(d=diametroPoste,h=alturaTornillo/2,center=true);
                cylinder(d=tornillo,h=alturaTornillo*2,center=true);
               }
           

}//Fin modulo poste (para case gamma)

 
 module EngraneSimple(flecha=4,tol=0.75,alturaEng=8.9,flecha=4){
    //alturaEng=7.9+1;
       translate([0,0,(alturaEng)+espesor+4])
//EngraneBanda(alturaEngrane=5.9,espesor=1,flecha=flecha,tol=tol,diamDiente=1.3,diamEngrane=9,dientes=14);
 //EngraneBanda(alturaEngrane=6.5,espesor=1,flecha=flecha,tol=0.75,diamDiente=1.3,diamEngrane=9,dientes=14);

EngraneBanda(alturaEngrane=6.5,espesor=1,flecha=11.5,tol=0.75,diamDiente=1.3,diamEngrane=14,dientes=20);
 
 

    
  //rondana a engrane Simple
   //translate([0,0,0])
 //linear_extrude(height=7.9+4+5)
//rondanas(radioExterno=4,radioInterno=(flecha/2)+(tol/2));  
translate([0,0,6])
poste(diametroPoste=12,tornillo=flecha+tol,alturaTornillo=17);    
    
         rotate([0,0,90])
      difference(){
     poste(diametroPoste=5+16,tornillo=flecha+tol,alturaTornillo=14);
        //TUERCA
        viajeTuerca=6.5;
        translate([viajeTuerca,0,6])
         rotate([0,90,0])
     cylinder(d=6,$fn=6,h=2.5);
         translate([viajeTuerca,0,4])
         rotate([0,90,0])
     cylinder(d=6,$fn=6,h=2.5);
        //EJEtornillo 
        translate([0,0,4])
        rotate([0,90,0])
           cylinder(d=3.5,h=40);
         }

}


EngraneSimple(flecha=4,tol=0.75,alturaEng=8.9,flecha=4);