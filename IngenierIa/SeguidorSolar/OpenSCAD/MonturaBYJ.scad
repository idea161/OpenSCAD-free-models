module BYJshaft(tol=1){
            altura=3;
            difference(){
            circle(d=4.95+tol);
            translate([-5,(altura+tol)/2])
                square([10,10]);
                
            translate([-5,-10-(altura+tol)/2])
                square([10,10]);
                }
        }
        
        
        
        $fn=100;
        
        
        linear_extrude(height=3.25)
        difference(){
          
            
            resize([30,10])
            circle(d=4);
            
            
            BYJshaft(tol=0.25);
        
            
            translate([10,0])
            circle(d=2.5);
            
               translate([-10,0])
            circle(d=2.5);
            
            }