espesor=2;


lonX=12;
lonY=14;
//lonZ=10;
lonZ=11;
tol=1;

$fn=100;

difference(){
    
    union(){
    translate([0,0,(lonZ+tol+espesor)/2])
    cube([lonX+tol+(2*espesor),lonY,lonZ+tol+(espesor)],center=true);
    
        translate([0,0,espesor])
    cube([lonX*3,lonY,espesor*2],center=true);
 
    }
    
    translate([0,0,(lonZ+tol)/2])
    cube([lonX+tol,lonY+tol,lonZ+tol],center=true);
    
    translate([lonX*1.1,0,0])
    cylinder(d=3,h=20,center=true);
    
    
    translate([-lonX*1.1,0,0])
    cylinder(d=3,h=20,center=true);
}