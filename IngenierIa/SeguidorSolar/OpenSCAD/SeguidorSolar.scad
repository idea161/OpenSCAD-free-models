
//home/pablovc/Documentos/OpenSCAD-free-models/SistemaTiDi
//include<./../../Bibliotecas/CASEgamma/CASEgamma.scad>;
//include<./../../Bibliotecas/Componentes/boards.scad>;
//include<./../../Bibliotecas/Componentes/sensores.scad>;
include<motores.scad>;
//include<./../../Bibliotecas/Utilidades/Utilities.scad>;




     //tope motor reductor
    /* translate([11.5,eje/2,0])
rotate([90,0,0])
cylinder(d=4+tol,h=eje);*/
       
     


   

   module motorMount(espesor=3,eje=40,tol=1){
        dim=23;  
       
       mirror([1,0,0]){
         translate([-(dim*0.5),0,-espesor])
         difference(){
         cube([(dim*0.5)+(espesor*1.5),dim,espesor]);
         
             translate([((dim*0.5)+(espesor*1.5))/2,dim*0.3+espesor,0])
             for(i=[0:1]){
                 translate([0,dim/3*i,0])
                 cylinder(d=2.7,h=20,center=true);
                 }
             
          }
       
       translate([0,espesor/2,dim/2])
difference(){
    cube([dim,espesor,dim],center=true);
    union(){
        rotate([90,0,0])
        cylinder(d=7+tol,h=eje,center=true);
                
               //tornillo1
             translate([-8,0,10])
        rotate([90,0,0])
        cylinder(d=2.5+tol,h=eje,center=true);
                  
               //tornillo1
             translate([-8,0,-10])
        rotate([90,0,0])
        cylinder(d=2.5+tol,h=eje,center=true);
    }
    
    translate([espesor*1.5,-dim,-dim])
    cube([dim*2,dim*2,dim*2]);
   
   }
 }  
}


//#####RENDERIZADOS#####
$fn=100;

/*mirror([1,0,0]){ 
  motorMount();
}*/

linear_extrude(height=7.2)
difference(){
  circle(r=28);
  RedMotShaft();
    for(i=[0:12]){
        rotate((360/12)*i)
          translate([16,0])
        scale([4,1])
    circle(d=4);    
    }
}