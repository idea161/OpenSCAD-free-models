# Termo Modular 

![](./PNG/Termo.png)

## Summary/ Descripción

The following code is for making a modular thermos flask, it is representative, not for daily use.
Parts may vary in size if desired.
The suggested use is for storage, also as a complement to physicochemical projects.

El siguiente código es para realizar un termo modular, es representativo, no de uso diario.
Las partes pueden variar de tamaño si uno lo desea.
La sugerencia de uso es para almacenamiento, también como complementos para proyectos de física-química.

