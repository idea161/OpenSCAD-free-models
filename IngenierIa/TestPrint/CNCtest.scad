

difference(){
    
    circle(r=(1.7*2)+0.5,$fn=32);
    
    union(){
        circle(r=1.7,$fn=16);

        for(i=[0:15]){
            rotate(i*45/2)
            translate([0,1.6])
            polygon([[0.5,0],[0,1.6],[-0.5,0]]);
        }
    }

}