
include<./../../Logos/LogoIdea161.scad>;



module cuboDemo(lon=26,texto1="1 hora",texto2="17576 mm3",tamFuente=3,espesor=1.5,escalaLogo=0.1,logo=1){


echo(lon*lon*lon);

cube([lon,lon,lon],center=true);

translate([0,-lon/2+espesor/2,-lon/2+(tamFuente*1.5)+(espesor/2)])
rotate([90,0,0])
linear_extrude(height=espesor)
 text(size=tamFuente,font = "Simplex",texto1,halign ="center",valign ="bottom");
 
 translate([0,-lon/2+espesor/2,-lon/2+(tamFuente*1.5)-(espesor/2)])
rotate([90,0,0])
linear_extrude(height=espesor)
 text(size=tamFuente,font = "Simplex",texto2,halign ="center",valign ="top");
 
 
    if(logo==1){
translate([0,-lon/2+espesor/2,tamFuente*2])
rotate([90,0,0])
 linear_extrude(height=espesor)
 Idea(escalaLogo);
    }
    
 }
 
 //####RENDERIZADOS####
 
 
 //$fn=8;

//1 hora DEMO 
 //17576 mm3
//cuboDemo(lon=26,texto1="1 hr",texto2="18cc",tamFuente=7,espesor=1.5,escalaLogo=0.1,logo=0);
 
 
 //2 horas DEMO 
//cuboDemo(lon=36,texto1="2 hr",texto2="47 cc",tamFuente=7,espesor=1.5,escalaLogo=0.15,logo=1);
 
 
  //4 horas DEMO 
//cuboDemo(lon=49,texto1="4 horas",texto2="118 cc",tamFuente=7,espesor=1.5,escalaLogo=0.25,logo=1);
 
  
  //8 horas DEMO 
cuboDemo(lon=66,texto1="8 horas",texto2="287 cc",tamFuente=7,espesor=1.5,escalaLogo=0.34,logo=1);
 
 //echo(66+49+36+26+(10*3));