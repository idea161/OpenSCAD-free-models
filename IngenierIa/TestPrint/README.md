# TestPrint/Impresión de Prueba


![](./PNG/TestPrint.png)


## Summary/Descripción

This project is a simple figure that is used to adjust the level of the bed of a 3D printer, in addition to the fact that with this test you can check the arcs and angles of the print horizontally.

Éste proyecto es una figura sencilla que sirve para ajustar la nievelación de la cama de una impresora 3D, además de que con ésta prueba se puede comprobar los arcos y ángulos de la impresión horizontalmente.



