
Radio=30;
pared=10;
grueso=2;


//$fn=8;

module disco(resoluciOn=100,Radio=30,pared=10,grueso=2){
difference(){
    
    
cylinder(r=Radio,center=true,h=grueso,$fn=resoluciOn);
cylinder(r=Radio-pared,center=true,h=grueso*2,$fn=resoluciOn);
       
}

}


//####RENDERIZADOS####

color("cyan")
union(){
    difference(){
        Radio=30;
        pared=10;
        rotate(360/16)
       disco(resoluciOn=8,Radio=Radio+(pared/6),pared=pared,grueso=2);
       translate([-Radio*1.5,0,-Radio*1.5])
        cube([Radio*3,Radio*3,Radio*3]);
    }
        
    
    ajusteEsf=grueso*4;
    ajEspesor=0.5;
    dome=1;
 if(dome==1){
    difference(){
        
        scale([1,1,1.61])
        sphere(r=Radio-(1.5/2)-ajusteEsf+ajEspesor,$fn=80);
        
        scale([1,1,1.61])
        sphere(r=Radio-grueso-ajusteEsf,$fn=160);
        
        translate([0,-Radio*2,0])
        cube([Radio*4,Radio*4,Radio*4],center=true);
        
        translate([0,0,-Radio*2])
        cube([Radio*4,Radio*4,Radio*4],center=true);
    }
}
    rotate(180)
    difference(){
        Radio=30;
       disco(resoluciOn=100,Radio=Radio-0.75,pared=9.25,grueso=2);
       translate([-Radio*1.5,0,-Radio*1.5])
        cube([Radio*3,Radio*3,Radio*3]);
    }
}