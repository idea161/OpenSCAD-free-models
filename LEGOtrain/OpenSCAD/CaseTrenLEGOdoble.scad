
include<Technic.scad>;


module ruedaTren(espesor=2.5,tol=0.5){

    linear_extrude(height=espesor)
    difference(){
        circle(d=15.38);
        //techCruz(tol=tol);
        circle(d=4); 
    }

    for(i=[0:5]){
        rotate(i*(360/6))
        translate([0,5.5,0])
        union(){
            translate([-3.62/2,0,0])
            cube([3.62,2,2.7+espesor]);
            translate([-3.62/2,0,2.7+espesor-1])
            cube([3.62,2.5,1]);
        }
    }

}

   module Poste(altura=10,espesor=3,tol=1,diamShaft=2){
	   
	   difference(){
	      cylinder(d=(espesor*2)+diamShaft+tol,h=altura);
	   
		  cylinder(d=diamShaft+tol,h=altura*4,center=true);
	      
	   }
   }
   
   module agujeroCopTech(tol=0.1,altura=8){


 diamMed=5.4;
 diamExt=6.2;
  
 union(){
       cylinder(d=diamMed+tol,h=altura,center=true);
        
     
        //tope externo A
      
        translate([0,0,-altura/8-3])
        cylinder(d=diamExt+tol,h=altura/4,center=true);
        
        
        //tope externo B
       
        translate([0,0,altura/8+3])
        cylinder(d=diamExt+tol,h=altura/4,center=true);
        
    }


}


//#######RENDERIZADOS######

$fn=100;

//ruedaTren(espesor=2.5,tol=0);


//translate([-8,8,0])
//rotate([90,90,0])
//barrasTech(tol=0.1,tamaNio=2,alturaBarra=8);



unidad=7.7;
difference(){
  
  union(){
    cube([unidad*3.5,unidad*8,unidad],center=true);
   
      rotate([0,0,90])
    translate([unidad*2.25,0,-4])
    rotate([90,0,0])
    cylinder(d=10,h=(unidad*4),center=true);
      
      rotate([0,0,90])
    translate([unidad*2.25,0,-4])
    rotate([90,0,0])
    cylinder(d=8,h=(unidad*4)+12,center=true);
      
        rotate([0,0,90])
    translate([-unidad*2.25,0,-4])
    rotate([90,0,0])
    cylinder(d=10,h=(unidad*4),center=true);
      
      rotate([0,0,90])
    translate([-unidad*2.25,0,-4])
    rotate([90,0,0])
    cylinder(d=8,h=(unidad*4)+12,center=true);
  }
  
  translate([-unidad-0.5,4,-unidad*2])
  cube([(unidad*2)+1,(unidad*3)-4,unidad*4]);
  
  rotate(180)
   translate([-unidad-0.5,4,-unidad*2])
  cube([(unidad*2)+1,(unidad*3)-4,unidad*4]);
   
  
    translate([-unidad,0,0]) 
  for(i=[0:2]){
      translate([unidad*i,0,0])
      agujeroCopTech(tol=0.1,altura=8);
  }
    
  translate([-unidad,0,0]) 
  for(i=[0:2]){
      translate([unidad*i,unidad*3.5,0])
      rotate([90,0,0])
      agujeroCopTech(tol=0.1,altura=8);
  }
  
    translate([-unidad,0,0]) 
  for(i=[0:2]){
      translate([unidad*i,-unidad*3.5,0])
      rotate([90,0,0])
      agujeroCopTech(tol=0.1,altura=8);
  }
    

   rotate([0,0,90])
   translate([unidad*2.25,0,-4])
   rotate([90,0,0])
   cylinder(d=4,h=80,center=true);
  
   rotate([0,0,90])
   translate([-unidad*2.25,0,-4])
   rotate([90,0,0])
   cylinder(d=4,h=80,center=true);
}



/*difference(){
  
  union(){  
    cylinder(d=30,h=1.25);
    translate([0,0,1.25])
    cylinder(d1=30,d2=22.5,h=1.25);
  }
  cylinder(d=22.5,h=40,center=true);
} */