radio=20;
grueso=3;
 radioEspiral=6;
  
  
  //Patas
     anguloPatas=65;
     tol=0.35;
     altoVista=15;

 
  anguloInicioB=38;

$fn=100;

//#####RENDERIZADOS#######

/*
//"cuerpo"
color("cyan")
difference(){
union(){
sphere(r=radio);


rotate([180,0,0])
cylinder(r=radio,h=radio*2.25);



//disco base
translate([0,0,-(radio*2)+10])
minkowski(){
  cylinder(r=radio,h=(radio/6)+1);
  sphere(r=0.75);

}

//disco base
translate([0,0,-radio*2.25])
minkowski(){
  cylinder(r=radio+(radio/6),h=(radio/3));
  sphere(r=1.5);

}




}

//agujeros para piezas


     rotate(anguloPatas)
     translate([radio*0.4,0,-3*radio])
cylinder(r=grueso,h=50,center=true);

  rotate(anguloPatas)
     translate([-radio*0.4,0,-3*radio])
cylinder(r=grueso,h=50,center=true);


}




     //disco
     color("cyan")
translate([radio-(grueso*0.6),0,0])
rotate([90,0,90])
linear_extrude(height=grueso)
union(){
 difference(){

     circle(r=radioEspiral,$fn=100);
     circle(r=radioEspiral-(grueso*1),$fn=100);
 }
 
 //translate([radioEspiral*(0.3),-radioEspiral*(0.4)])
 //rotate(-60)
 //square([grueso,radioEspiral*3]);
 }
 
 
 anguloInicio=10;
 
      
     color("cyan")
 translate([0,0,-4.8])
 rotate(anguloInicio)
 difference(){
     for(i=[0:10]){
         rotate(2.5*i)
         translate([radio-(grueso*0.325),0,0.5*i])
         rotate([30,0,0])
         cube([grueso,grueso/2,grueso]);
         
         
     }
    translate([radio+1.5,0,0])
     rotate([0,90,-anguloInicio])
     cylinder(r=radioEspiral,h=grueso);
  
 } 
 

      
     color("cyan")
 translate([0,0,0.8])
 rotate(anguloInicioB)

     for(i=[0:40]){
         rotate([0,0,1.25*i])
         translate([radio-(grueso*0.325)-(0.19*i),0,0.25*i])
         rotate([30,0,0])
         cube([grueso,grueso/4,grueso]);
         
         
     }
     
   
     */
     
     
     


//pata corta
union(){  
     color("gray")
     rotate(anguloPatas)
     translate([radio*0.4,0,-3*radio])
     cylinder(r=grueso-tol,h=13+altoVista);
     
     color("gray")
rotate(anguloPatas)
translate([radio*0.4,0,-3*radio])
     sphere(r=grueso-tol);
}
   
//base
!union(){
     color("gray")
   rotate(anguloPatas)
     translate([-radio*0.4,0,-3.5*radio])
     cylinder(r=grueso-tol,h=23+altoVista);

   color("gray")
    translate([0,0,-radio*3.5-4])
     cylinder(d=(radio*2.5)+12,h=6);
}

 
 
 
 
 