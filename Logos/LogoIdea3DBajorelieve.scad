use<LogoIdea161.scad>;



  
  dim=32;
  //apo=((sqrt(3))/2);
  apo=cos(30);
  espesor=4;
  
  
      difference(){
          rotate(30)
            //linear_extrude(height=espesor)
          union(){
              translate([0,dim])
              rotate(90)
              linear_extrude(height=espesor)
              circle(d=dim*2,$fn=6);
              //square([dim*2,dim*2],center=true);
              
               translate([apo*(dim),-(dim/2)])
              rotate(90)
              linear_extrude(height=espesor*1.5)
              circle(d=dim*2,$fn=6);
              
               translate([-apo*(dim),-(dim/2)])
              rotate(90)
              linear_extrude(height=espesor*2)
              circle(d=dim*2,$fn=6);
              
               translate([0,-(dim*2)])
              rotate(90)
              linear_extrude(height=espesor*2.5)
              difference(){
                circle(d=dim*2,$fn=6);
                  rotate(240)
                  translate([0,-40])
                square([80,80],center=true);
              }
          }
          
          translate([0,20,2])
          linear_extrude(height=espesor*3)
          union(){
          Idea(escala=0.6,mini=0);

    translate([0,-55,0])
      text(size=8,font = "Simplex:style=Bold","IDEA 1.61",halign ="center",valign="center");
          }
      }
      
      /*translate([0,-dim+espesor,espesor/2])
      minkowski(){
      cube([(dim*2)-20,0.01,0.01],center=true);
          rotate([90,0,0])
          cylinder(d=20,h=4,$fn=6);
      }*/