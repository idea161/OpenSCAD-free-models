

module Imperial(radio=20){

//orillas PROBADAS
difference(){
  circle(r=radio);
  circle(r=radio-(radio/8)); 
}  

//bastones orillas PROBADO
for(i=[0:5]){
  rotate(i*(360/6))
  translate([radio*(4/8),-radio*(1/64)])
  square([radio*(3/8),radio*(1/32)]);
}

//disco centro PROBADO
difference(){
    
    union(){
    circle(r=radio*(5/8));
        
        for(i=[0:5]){
    rotate(i*(360/6))
    //segmento dtalle interno PROBADO
    polygon([[radio*(6/8),radio*(2/16)],[radio*(6/8),-radio*(2/16)],[0,0]]);
       }
        
    }
    circle(r=radio*(1/4));
    
    
    for(i=[0:5]){
    rotate(i*(360/6))
    //segmento dtalle interno PROBADO
    polygon([[radio*(4/8),radio*(1/16)],[radio*(4/8),-radio*(1/16)],[0,0]]);
    }

}
    

}//fin modulo

module PrimeraOrden(radio=20){

cuentas=17;
rotate(90)
difference(){
circle(r=radio,$fn=6);
circle(r=radio-(radio/16),$fn=6);

}

difference(){
circle(r=radio-(radio*(4/16)));
circle(r=radio-(radio*(5/16)));

}

for(i=[0:cuentas-1]){
    rotate(i*(360/cuentas))
    translate([-radio*(3/4)+radio*(1/16),0])
    polygon([[0,radio*(1/32)],[radio*(1/4),0],[0,-radio*(1/32)]]);
}
}//fin modulo


module Republica(radio=20){


//orillas PROBADAS
difference(){
  circle(r=radio);
  circle(r=radio-(radio/16)); 
}  

difference(){
  circle(r=radio-(radio*(1/8)));
  circle(r=radio-(radio*(2/8)));
   
    for(i=[0:7])
    {
        rotate(i*(360/8))
   translate([radio-(radio*(3/16)),0])
    square([radio/4,radio/4],center=true);
    }
}  

 circle(r=radio*(1/4));

for(i=[0:3]){
 rotate(i*(90/2))   
square([radio*1.25,radio*(1/8)],center=true);
}

}
//####RENDERIZADOS####
//$fn=20;
//linear_extrude(height=1.5)
//Imperial(radio=20);
//PrimeraOrden(radio=30);
//Republica(radio=30);




